﻿/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DotNetSources.Web.UI.Buttons.CssModel;
using System.Web.UI;

[assembly: WebResource("DotNetSources.Web.UI.Buttons.Popup.PopupBackground.js", "text/javascript", PerformSubstitution = true)]
[assembly: WebResource("DotNetSources.Web.UI.Buttons.Popup.PopupBackground.css", "text/css")]

namespace DotNetSources.Web.UI.Buttons {

    /// <summary>
    /// Just background to cover whole page and disable to users click to something. 
    /// Is used when message box or confirm box is displayed.
    /// </summary>
    [Css("DotNetSources.Web.UI.Buttons.Popup.PopupBackground.css")]
    public class PopupBackground : ScriptWebControl {

        public PopupBackground()
            : base(HtmlTextWriterTag.Div) {
        }

        #region IScriptControl Members

        public override IEnumerable<ScriptDescriptor> GetScriptDescriptors() {
            ScriptControlDescriptor descriptor = new ScriptControlDescriptor("DotNetSources.Background", this.ClientID);
            return new ScriptDescriptor[] { descriptor };
        }

        public override IEnumerable<ScriptReference> GetScriptReferences() {
            ScriptReference jsReference = new ScriptReference();
            jsReference.Assembly = "DotNetSources.Web.UI.Buttons";
            jsReference.Name = "DotNetSources.Web.UI.Buttons.Popup.PopupBackground.js";
            return new ScriptReference[] { jsReference };
        }

        #endregion
    }

}

