﻿Type.registerNamespace('DotNetSources');

var dns_global_confirm_box;

DotNetSources.ConfirmBox = function(element) {
    DotNetSources.ConfirmBox.initializeBase(this, [element]);

    // properties
    this._buttonId = null;

    // internal
    this._buttonToFireId = null;

    dns_global_confirm_box = this;
}

DotNetSources.ConfirmBox.get_instance = function() {
    return dns_global_confirm_box;
}

DotNetSources.ConfirmBox.prototype = {

    //
    // Override initialize
    //
    initialize: function() {
        DotNetSources.BoxBase.callBaseMethod(this, 'initialize');
        this._yesClickHandler = Function.createDelegate(this, this._performPostback);
        $addHandler($get(this._buttonId), "click", this._yesClickHandler);
    },

    //
    // Override Show
    //
    Show: function(message, buttonToFireId) {
        this._buttonToFireId = buttonToFireId;
        // call base to display box with question icon
        DotNetSources.ConfirmBox.callBaseMethod(this, 'Show', [message, DotNetSources.Configuration.get_instance().get_questionIcon()]);
    },

    _performPostback: function() {
        this.Hide();
        __doPostBack(this._buttonToFireId, '');
    },

    _clearHandler: function() {
        $removeHandler($get(this._buttonId), "click", this._yesClickHandler);
    },

    dispose: function() {
        DotNetSources.BoxBase.callBaseMethod(this, 'dispose');
        this._clearHandler();
    },

    //
    // this._buttonId
    //
    set_buttonId: function(value) {
        this._buttonId = value;
    },

    get_buttonId: function() {
        return this._buttonId;
    },
    
    set_buttonToFireId: function(value) {
        this._buttonToFireId = value;
    },

    get_buttonToFireId: function() {
        return this._buttonToFireId;
    }

}

DotNetSources.ConfirmBox.registerClass('DotNetSources.ConfirmBox', DotNetSources.BoxBase);