﻿/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DotNetSources.Web.UI.Buttons.CssModel;
using System.Web.UI;

[assembly: WebResource("DotNetSources.Web.UI.Buttons.Popup.ConfirmBox.ConfirmBox.js", "text/javascript", PerformSubstitution = true)]
[assembly: WebResource("DotNetSources.Web.UI.Buttons.Popup.ConfirmBox.ConfirmBox.css", "text/css")]

namespace DotNetSources.Web.UI.Buttons {

    /// <summary>
    /// The confirm box.
    /// </summary>
    [Css("DotNetSources.Web.UI.Buttons.Popup.ConfirmBox.ConfirmBox.css")]
    public class ConfirmBox : BoxBase {

        /// <summary>
        /// Close button.
        /// </summary>
        protected TextButton noButton;

        /// <summary>
        /// Ok button.
        /// </summary>
        protected TextButton yesButton;

        /// <summary>
        /// Client ID of OK buton.
        /// </summary>
        public string OkButtonId {
            get {
                return Helper.GetJSId(yesButton);
            }
        }

        private string buttonToFireId = string.Empty;

        public string ButtonToFireId {
            get {
                return buttonToFireId;
            }
        }

        protected override System.Web.UI.WebControls.Panel GetPanelWithButtons() {
            
            Panel pnlButtons = new Panel();
            pnlButtons.CssClass = "dns_cb_main";

            Panel pnlClose = new Panel();
            pnlClose.CssClass = "dns_cb_close";
            Panel pnlCloseInner = new Panel();
            pnlCloseInner.CssClass = "dns_cb_close_inner";
            pnlClose.Controls.Add(pnlCloseInner);
            pnlButtons.Controls.Add(pnlClose);

            Panel pnlOk = new Panel();
            pnlOk.CssClass = "dns_cb_ok";
            pnlButtons.Controls.Add(pnlOk);

            // JS for close button is set in base class
            noButton = new DotNetSources.Web.UI.Buttons.TextButton();
            noButton.OnClientClick = GetCloseButtonClickScript();
            noButton.ID = "closeButton";
            noButton.PredefinedStyle = TextButtonStyle.Red;
            noButton.Text = "No";
            pnlCloseInner.Controls.Add(noButton);

            yesButton = new DotNetSources.Web.UI.Buttons.TextButton();
            yesButton.PredefinedStyle = TextButtonStyle.Green;
            yesButton.OnClientClick = "return false;";
            yesButton.ID = "okButton";
            yesButton.Text = "Yes";
            pnlOk.Controls.Add(yesButton);

            // set same width
            noButton.ButtonWidth = yesButton.ButtonWidth = "52px";

            pnlButtons.Controls.Add(new ClearDiv());

            return pnlButtons;
        }

        public override IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors() {

            ScriptControlDescriptor[] descriptors = base.GetScriptDescriptors() as ScriptControlDescriptor[];
            descriptors[0].Type = "DotNetSources.ConfirmBox";
            descriptors[0].AddProperty("buttonId", OkButtonId);
            descriptors[0].AddProperty("buttonToFireId", ButtonToFireId);
            return descriptors;
        }

        public override IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences() {

            ScriptReference jsReference = new ScriptReference();
            jsReference.Assembly = "DotNetSources.Web.UI.Buttons";
            jsReference.Name = "DotNetSources.Web.UI.Buttons.Popup.ConfirmBox.ConfirmBox.js";
            List<ScriptReference> ret = new List<ScriptReference>(base.GetScriptReferences());
            ret.Add(jsReference);
            return ret;
        }

        public override void Localize(params string[] args) {
            yesButton.Text = args[0];
            noButton.Text = args[1];
        }
    }
}