﻿/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetSources.Web.UI.Buttons.Popup {

    /// <summary>
    /// The Icon enum. Must consist with it's 
    /// JavaScript mirror defined in BoxBase.js.
    /// </summary>
    public enum Icon : int {
        Error = 1,
        Question = 2,
        Info = 3,
        Warning = 4,
        Undefined = 5
    }

}

