﻿Type.registerNamespace('DotNetSources');

DotNetSources.Icons = function() {
    // nothing, just enumerator
}

//
// Icons enumerator.
// 
DotNetSources.Icons.prototype = {
    Error: 1,
    Question: 2,
    Info: 3,
    Warning: 4,
    Undefined: 5
}

DotNetSources.Icons.registerEnum('DotNetSources.Icons');

// ctor.
DotNetSources.BoxBase = function(element) {
    DotNetSources.BoxBase.initializeBase(this, [element]);
    this._iconId = null;
    this._messageId = null;
    this._backgroundId = null;
}

// prototype
DotNetSources.BoxBase.prototype = {

    initialize: function() {
        DotNetSources.BoxBase.callBaseMethod(this, 'initialize');
    },

    Show: function(message, icon) {

        var imgIcon = $get(this._iconId);

        if (icon === DotNetSources.Icons.Error) {
            imgIcon.setAttribute('src', DotNetSources.Configuration.get_instance().get_errorIcon());
        } else if (icon === DotNetSources.Icons.Question) {
            imgIcon.setAttribute('src', DotNetSources.Configuration.get_instance().get_questionIcon());
        } else if (icon === DotNetSources.Icons.Info) {
            imgIcon.setAttribute('src', DotNetSources.Configuration.get_instance().get_infoIcon());
        } else if (icon === DotNetSources.Icons.Warning) {
            imgIcon.setAttribute('src', DotNetSources.Configuration.get_instance().get_warningIcon());
        } else {
            imgIcon.setAttribute('src', DotNetSources.Configuration.get_instance().get_questionIcon());
        }
        $get(this._messageId).innerHTML = message;
        this.get_element().style.visibility = 'visible';
        $find(this._backgroundId).Show();
    },

    Hide: function() {
        this.get_element().style.visibility = 'hidden';
        $find(this._backgroundId).Hide();
    },

    //
    // Icon ID
    //
    get_iconId: function() {
        return this._iconId;
    },

    set_iconId: function(value) {
        this._iconId = value;
    },

    //
    // Message ID
    //
    get_messageId: function() {
        return this._messageId;
    },

    set_messageId: function(value) {
        this._messageId = value;
    },

    //
    // Background ID
    //
    get_backgroundId: function() {
        return this._backgroundId;
    },

    set_backgroundId: function(value) {
        this._backgroundId = value;
    }

}

DotNetSources.BoxBase.registerClass('DotNetSources.BoxBase', Sys.UI.Control);