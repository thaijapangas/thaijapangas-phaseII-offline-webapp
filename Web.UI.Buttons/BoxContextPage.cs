﻿/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using DotNetSources.Web.UI.Buttons.CssModel;
using System.Web.UI.WebControls;
using DotNetSources.Web.UI.Buttons.Interfaces;

namespace DotNetSources.Web.UI.Buttons {

    /// <summary>
    /// Page contains MessageBox and ConfirmBox.
    /// </summary>
    public class BoxContextPage : Page {

        /// <summary>
        /// The dictionary of CSS registered by controls.
        /// </summary>
        private Dictionary<Type, object> registredCss = new Dictionary<Type, object>();

        private ScriptManager sm = null;

        /// <summary>
        /// ScriptManager for this page.
        /// </summary>
        public ScriptManager Sm {
            get {
                if (sm == null) {
                    sm = ScriptManager.GetCurrent(this);
                    if (sm == null)
                        throw new Exception("ScriptManager is missing.");
                }
                return sm;
            }
        }

        #region Localization buttons texts

        private string messageBoxButtonText = "OK";

        /// <summary>
        /// The OK message box button text.
        /// To have affect, it must be set before OnPreRender phase.
        /// </summary>
        public string MessageBoxButtonText {
            get {
                return messageBoxButtonText;
            }
            set {
                messageBoxButtonText = value;
            }
        }

        private string positiveButtonText = "Yes";

        /// <summary>
        /// The yes button for confirm box.
        /// </summary>
        public string PositiveButtonText {
            get {
                return positiveButtonText;
            }
            set {
                positiveButtonText = value;
            }
        }

        private string negativeButtonText = "No";

        /// <summary>
        /// The no button for confirm box.
        /// </summary>
        public string NegativeButtonText {
            get { 
                return negativeButtonText; 
            }
            set { 
                negativeButtonText = value; 
            }
        }

        #endregion

        #region Controls

        private IBox confirmBox;

        /// <summary>
        /// Get ConfirmBox instance.
        /// </summary>
        protected IBox ConfirmBox {
            get { return confirmBox; }
            set { confirmBox = value; }
        }

        /// <summary>
        /// Client ID of confirm box.
        /// </summary>
        public virtual string ConfirmBoxId {
            get {
                return Helper.GetJSId(confirmBox as Control);
            }
        }

        protected Control background;

        /// <summary>
        /// Client ID of background DIV.
        /// </summary>
        public virtual string BackgroundId {
            get {
                return Helper.GetJSId(background);
            }
        }

        private IBox messageBox;

        /// <summary>
        /// Get MessageBox instance to call Show method.
        /// </summary>
        public IBox MessageBox {
            get { return messageBox; }
        }

        /// <summary>
        /// Client ID of messageBox.
        /// </summary>
        public virtual string MessageBoxId {
            get {
                return Helper.GetJSId(messageBox as Control);
            }
        }

        #endregion

        #region Prepared for customization

        /// <summary>
        /// Customization for MessageBox.
        /// </summary>
        /// <returns></returns>
        protected virtual IBox CreateMessageBox() {
            MessageBox mBox = new MessageBox();
            mBox.ID = "dns__globalMessageBox";
            return mBox;
        }

        /// <summary>
        /// Customization for ConfirmBox.
        /// </summary>
        /// <returns></returns>
        protected virtual IBox CreateConfirmBox() {
            ConfirmBox cBox = new ConfirmBox();
            cBox.ID = "dns__globalConfirmBox";
            return cBox;
        }

        /// <summary>
        /// Customization of boxes background.
        /// </summary>
        /// <returns></returns>
        protected virtual Control CreateBackground() {
            PopupBackground bck = new PopupBackground();
            bck.CssClass = "dns_pb_background";
            return bck;
        }

        /// <summary>
        /// Customization for configuration.
        /// </summary>
        /// <returns></returns>
        protected virtual Configuration CreateConfiguration() {
            return new Configuration();
        }

        #endregion

        protected override void OnInit(EventArgs e) {

            // add configuration
            Configuration cnf = CreateConfiguration();
            Form.Controls.Add(cnf);

            // add global background for boxes
            background = CreateBackground();
            Form.Controls.Add(background);

            // add global confirm box
            confirmBox = CreateConfirmBox();
            Form.Controls.Add(confirmBox as Control);

            // add global message box
            messageBox = CreateMessageBox();
            Form.Controls.Add(messageBox as Control);

            // call base
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e) {

            if (!IsPostBack) {
                // localize buttons in boxes
                MessageBox.Localize(MessageBoxButtonText);
                ConfirmBox.Localize(PositiveButtonText, NegativeButtonText);
            }

            base.OnPreRender(e);
        }


        /// <summary>
        /// Register CSS file to page.
        /// </summary>
        /// <param name="t"></param>
        public void RegisterCss(Type t) {

            // only one link for every same controls
            if (registredCss.ContainsKey(t))
                return;

            do {
                CssAttribute cssAtr = CssModel.CssModel.GetCss(t);
                if (cssAtr == null || registredCss.ContainsKey(t)) {
                    t = t.BaseType;
                    continue;
                }
                // add css to collection
                registredCss.Add(t, cssAtr);

                string rawLink = ClientScript.GetWebResourceUrl(t, cssAtr.Path);

                string csslink = string.Format("<link href='{0}' rel='stylesheet' type='text/css' />",
                    rawLink);

                HtmlLink link = new HtmlLink();
                link.Href = rawLink;
                link.Attributes.Add("type", "text/css");
                link.Attributes.Add("rel", "stylesheet");
                Header.Controls.Add(link);

                t = t.BaseType;
            } while (t != null);

        }
    }

}

