﻿/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace DotNetSources.Web.UI.Buttons {

    /// <summary>
    /// Collection of static methods used widely in project. 
    /// </summary>
    public static class Helper {

        /// <summary>
        /// Provide ButtonSupportPage. The Exception can be thrown if page is not derived from ButtonSupportPage.
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static BoxContextPage RetypeToBoxContextPage(Page page) {
            BoxContextPage ret = page as BoxContextPage;
            if (ret == null)
                throw new Exception("The page must be derived from BoxContextPage.");
            return ret;
        }

        /// <summary>
        /// This method exists as a back door when some of the control has a special way to generate ClientID.
        /// Could be for example AccordionExtender from ACT.
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static string GetJSId(Control c) {
            return c.ClientID;
        }

    }

}
