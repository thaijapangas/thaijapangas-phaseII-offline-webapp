﻿/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace DotNetSources.Web.UI.Buttons {

    /// <summary>
    /// Just the HTML design stuff.
    /// </summary>
    public class ClearDiv : WebControl {

        public ClearDiv()
            : base(HtmlTextWriterTag.Div) {
        }

        protected override void OnPreRender(EventArgs e) {
            Style.Add("clear", "both");
            base.OnPreRender(e);
        }

    }

}

