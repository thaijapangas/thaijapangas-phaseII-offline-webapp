﻿/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using DotNetSources.Web.UI.Buttons.CssModel;

[assembly: WebResource("DotNetSources.Web.UI.Buttons.TextButton.TextButton.css", "text/css")]

namespace DotNetSources.Web.UI.Buttons {

    /// <summary>
    /// Simple button contains only text. Has two predefined styles but is much easier to define your own styles.
    /// </summary>
    [Css("DotNetSources.Web.UI.Buttons.TextButton.TextButton.css")]
    public class TextButton : ButtonBase<Literal> {

        /// <summary>
        /// Predefined style. Currently Red and Green.
        /// </summary>
        public TextButtonStyle PredefinedStyle {
            get {
                object o = this.ViewState["PredefinedStyle"];
                if (o != null) {
                    return (TextButtonStyle) o;
                }
                return TextButtonStyle.Undefined;
            }
            set {
                this.ViewState["PredefinedStyle"] = value;
            }
        }

        /// <summary>
        /// Set button width.
        /// </summary>
        public string ButtonWidth {
            get {
                string str = (string)this.ViewState["ButtonWidth"];
                if (str != null) {
                    return str;
                }
                return string.Empty;
            }
            set {
                this.ViewState["ButtonWidth"] = value;
            }
        }

        protected override Literal CreateContentControl() {
            return new Literal();
        }

        protected override void OnPreRender(EventArgs e) {
            // set button text
            ContentControl.Text = Text;

            Div.CssClass = "dns_pre_btn_base";

            // set styles
            if (PredefinedStyle == TextButtonStyle.Red) {
                Div.CssClass += " dns_pre_btn_red";
            } else if (PredefinedStyle == TextButtonStyle.Green) {
                Div.CssClass += " dns_pre_btn_green";
            }

            // set width
            if (ButtonWidth != string.Empty)
                Div.Style.Add(HtmlTextWriterStyle.Width, ButtonWidth);

            base.OnPreRender(e);
        }

    }


}

