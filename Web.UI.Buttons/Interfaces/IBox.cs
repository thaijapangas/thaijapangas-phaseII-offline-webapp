﻿/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotNetSources.Web.UI.Buttons.Popup;

namespace DotNetSources.Web.UI.Buttons.Interfaces {

    /// <summary>
    /// IBox interface for customization of boxes.
    /// </summary>
    public interface IBox {

        /// <summary>
        /// Server side Show.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="icon"></param>
        void Show(string message, Icon icon);

        /// <summary>
        /// Localize buttons. This is called be BasePage where the localization is set.
        /// </summary>
        /// <param name="args"></param>
        void Localize(params string[] args);

    }

}
