﻿Type.registerNamespace('DotNetSources');

var global_cnf;

// ctor.
DotNetSources.Configuration = function(element) {
    DotNetSources.Configuration.initializeBase(this, [element]);

    this._questionIcon = null;
    this._infoIcon = null;
    this._warningIcon = null;
    this._errorIcon = null;
    global_cnf = this;
}

DotNetSources.Configuration.get_instance = function() {
    return global_cnf;
}

DotNetSources.Configuration.prototype = {

    get_questionIcon: function() {
        return this._questionIcon;
    },
    
    set_questionIcon: function(value) {
        this._questionIcon = value;
    },

    get_infoIcon: function() {
        return this._infoIcon;
    },
    
    set_infoIcon: function(value) {
        this._infoIcon = value;
    },
    
    get_warningIcon: function() {
        return this._warningIcon;
    },
    
    set_warningIcon: function(value) {
        this._warningIcon = value;
    },
    
    get_errorIcon: function() {
        return this._errorIcon;
    },
    
    set_errorIcon: function(value) {
        this._errorIcon = value;
    }
}


DotNetSources.Configuration.registerClass('DotNetSources.Configuration', Sys.UI.Control);