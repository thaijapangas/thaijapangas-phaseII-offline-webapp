﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web;

[assembly: WebResource("DotNetSources.Web.UI.Buttons.Configuration.Configuration.js", "text/javascript", PerformSubstitution = true)] // "text/css")]

namespace DotNetSources.Web.UI.Buttons {

    /// <summary>
    /// Configuration class is just for keeping configuration in client side in JavaScript.
    /// </summary>
    public class Configuration : ScriptWebControl {


        public virtual string QuestionIcon {
            get {
                return Page.ClientScript.GetWebResourceUrl(this.GetType(), "DotNetSources.Web.UI.Buttons.Icons.question.png");
            }
        }

        public virtual string InfoIcon {
            get {
                return Page.ClientScript.GetWebResourceUrl(this.GetType(), "DotNetSources.Web.UI.Buttons.Icons.info.png");
            }
        }

        public virtual string WarningIcon {
            get {
                return Page.ClientScript.GetWebResourceUrl(this.GetType(), "DotNetSources.Web.UI.Buttons.Icons.warning.png");
            }
        }

        public virtual string ErrorIcon {
            get {
                return Page.ClientScript.GetWebResourceUrl(this.GetType(), "DotNetSources.Web.UI.Buttons.Icons.error.png");
            }
        }

        protected override void RegisterCss() {
            //base.RegisterCss();
        }

        public override IEnumerable<ScriptDescriptor> GetScriptDescriptors() {
            ScriptControlDescriptor descriptor = new ScriptControlDescriptor("DotNetSources.Configuration", this.ClientID);
            descriptor.AddProperty("questionIcon", QuestionIcon);
            descriptor.AddProperty("infoIcon", InfoIcon);
            descriptor.AddProperty("warningIcon", WarningIcon);
            descriptor.AddProperty("errorIcon", ErrorIcon);
            return new ScriptDescriptor[] { descriptor };
        }

        public override IEnumerable<ScriptReference> GetScriptReferences() {
            ScriptReference jsReference = new ScriptReference();
            jsReference.Assembly = "DotNetSources.Web.UI.Buttons";
            jsReference.Name = "DotNetSources.Web.UI.Buttons.Configuration.Configuration.js";
            return new ScriptReference[] { jsReference };
        }
    }

}

