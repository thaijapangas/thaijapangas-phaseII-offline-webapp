﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entitys.Customers
{
    public class entCustomer
    {
        public string guid { get; set; }
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string is_active { get; set; }
        public DateTime create_date { get; set; }
        public string update_by { get; set; }
        public DateTime update_date { get; set; }
    }
}