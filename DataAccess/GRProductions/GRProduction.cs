﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.GRProductions
{
    public class GRProduction : IDisposable
    {
        #region Instance

        private static GRProduction _Instance = new GRProduction();
        public static GRProduction Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new GRProduction();
                }
                return _Instance;
            }
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
