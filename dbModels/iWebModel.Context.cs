﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dbModels
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Objects;
    using System.Data.Objects.DataClasses;
    using System.Linq;
    
    public partial class TJG01Entities : DbContext
    {
        public TJG01Entities()
            : base("name=TJG01Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<tbm_customer> tbm_customer { get; set; }
        public DbSet<tbm_item> tbm_item { get; set; }
    
        public virtual int sp_tran_tbm_customer(string guid, string action, string customer_code, string customer_name, string is_active, string create_by, ObjectParameter out_vchCode, ObjectParameter out_vchMessage)
        {
            var guidParameter = guid != null ?
                new ObjectParameter("guid", guid) :
                new ObjectParameter("guid", typeof(string));
    
            var actionParameter = action != null ?
                new ObjectParameter("action", action) :
                new ObjectParameter("action", typeof(string));
    
            var customer_codeParameter = customer_code != null ?
                new ObjectParameter("customer_code", customer_code) :
                new ObjectParameter("customer_code", typeof(string));
    
            var customer_nameParameter = customer_name != null ?
                new ObjectParameter("customer_name", customer_name) :
                new ObjectParameter("customer_name", typeof(string));
    
            var is_activeParameter = is_active != null ?
                new ObjectParameter("is_active", is_active) :
                new ObjectParameter("is_active", typeof(string));
    
            var create_byParameter = create_by != null ?
                new ObjectParameter("create_by", create_by) :
                new ObjectParameter("create_by", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_tran_tbm_customer", guidParameter, actionParameter, customer_codeParameter, customer_nameParameter, is_activeParameter, create_byParameter, out_vchCode, out_vchMessage);
        }
    
        public virtual int sp_tran_tbm_item(string guid, string action, string temp_item_code, string full_item_code, string item_name, string is_active, string create_by, ObjectParameter out_vchCode, ObjectParameter out_vchMessage)
        {
            var guidParameter = guid != null ?
                new ObjectParameter("guid", guid) :
                new ObjectParameter("guid", typeof(string));
    
            var actionParameter = action != null ?
                new ObjectParameter("action", action) :
                new ObjectParameter("action", typeof(string));
    
            var temp_item_codeParameter = temp_item_code != null ?
                new ObjectParameter("temp_item_code", temp_item_code) :
                new ObjectParameter("temp_item_code", typeof(string));
    
            var full_item_codeParameter = full_item_code != null ?
                new ObjectParameter("full_item_code", full_item_code) :
                new ObjectParameter("full_item_code", typeof(string));
    
            var item_nameParameter = item_name != null ?
                new ObjectParameter("item_name", item_name) :
                new ObjectParameter("item_name", typeof(string));
    
            var is_activeParameter = is_active != null ?
                new ObjectParameter("is_active", is_active) :
                new ObjectParameter("is_active", typeof(string));
    
            var create_byParameter = create_by != null ?
                new ObjectParameter("create_by", create_by) :
                new ObjectParameter("create_by", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_tran_tbm_item", guidParameter, actionParameter, temp_item_codeParameter, full_item_codeParameter, item_nameParameter, is_activeParameter, create_byParameter, out_vchCode, out_vchMessage);
        }
    }
}
