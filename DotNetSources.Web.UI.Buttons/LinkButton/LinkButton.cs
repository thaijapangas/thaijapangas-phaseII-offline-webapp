﻿/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetSources.Web.UI.Buttons {

    /// <summary>
    /// The LinkButton.
    /// </summary>
    public class LinkButton : ButtonBase<System.Web.UI.WebControls.LinkButton> {        

        protected override System.Web.UI.WebControls.LinkButton CreateContentControl() {
            return new System.Web.UI.WebControls.LinkButton();
        }

        protected override void OnPreRender(EventArgs e) {

            // set text
            ContentControl.Text = Text;

            // set tooltip
            ContentControl.ToolTip = ToolTip;

            // disable internal linkButton function
            ContentControl.OnClientClick = "return false;";

            base.OnPreRender(e);
        }

    }

}

