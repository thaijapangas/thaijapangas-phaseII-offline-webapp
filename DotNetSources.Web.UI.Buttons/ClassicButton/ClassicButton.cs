﻿/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace DotNetSources.Web.UI.Buttons {

    /// <summary>
    /// The ClassicButton.
    /// </summary>
    public class ClassicButton : ButtonBase<Button> {

        protected override Button CreateContentControl() {
            return new Button();
        }

        protected override void OnPreRender(EventArgs e) {
            ContentControl.Text = Text;

            // disable button
            ContentControl.OnClientClick = "return false;";

            base.OnPreRender(e);
        }

    }

}

