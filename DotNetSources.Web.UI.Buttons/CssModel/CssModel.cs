﻿/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetSources.Web.UI.Buttons.CssModel {

    /// <summary>
    /// CssModel is used to get information about CSS from resource according to control type.
    /// </summary>
    public class CssModel {

        /// <summary>
        /// Sync object.
        /// </summary>
        private static object sync = new object();

        /// <summary>
        /// Static collection.
        /// </summary>
        private static Dictionary<Type, CssAttribute> data = new Dictionary<Type, CssAttribute>();

        /// <summary>
        /// Use to retrieve CssAttribute from control.
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static CssAttribute GetCss(Type type) {

            lock (sync) {
                if (!data.ContainsKey(type)) {
                    CssAttribute[] atts = type.GetCustomAttributes(typeof(CssAttribute), true) as CssAttribute[];
                    if (atts == null || atts.Length == 0) {
                        return null;
                    }
                    data.Add(type, atts[0]);
                }
                return data[type];
            }
        }

    }

}

