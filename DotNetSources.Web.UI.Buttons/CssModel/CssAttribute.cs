﻿/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetSources.Web.UI.Buttons.CssModel {

    /// <summary>
    /// CssAttribute for specify relation between control and CSS file.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class CssAttribute : Attribute {

        private string path;

        public string Path {
            get { return path; }
            set { path = value; }
        }

        /// <summary>
        /// CssAttribute keeping information about CSS resource.
        /// </summary>
        /// <param name="path">The path to embedded resource.</param>
        public CssAttribute(string path) {
            this.path = path;
        }

    }

}

