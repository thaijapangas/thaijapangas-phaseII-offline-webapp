﻿Type.registerNamespace('DotNetSources');

// ctor.
DotNetSources.Background = function(element) {
    DotNetSources.Background.initializeBase(this, [element]);
}

// prototype

DotNetSources.Background.prototype = {

    Show: function() {
        this.get_element().style.visibility = 'visible';
    },

    Hide: function() {
        this.get_element().style.visibility = 'hidden';
    }
}

DotNetSources.Background.registerClass('DotNetSources.Background', Sys.UI.Control);
