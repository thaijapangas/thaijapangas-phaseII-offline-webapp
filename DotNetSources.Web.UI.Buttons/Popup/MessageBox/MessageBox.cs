﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System.Web.UI;
using DotNetSources.Web.UI.Buttons.CssModel;

[assembly: WebResource("DotNetSources.Web.UI.Buttons.Popup.MessageBox.MessageBox.js", "text/javascript", PerformSubstitution = true)]
[assembly: WebResource("DotNetSources.Web.UI.Buttons.Popup.MessageBox.MessageBox.css", "text/css")]

namespace DotNetSources.Web.UI.Buttons {

    /// <summary>
    /// The message box.
    /// </summary>
    [Css("DotNetSources.Web.UI.Buttons.Popup.MessageBox.MessageBox.css")]
    public class MessageBox : BoxBase {

        TextButton okButton;

        protected override System.Web.UI.WebControls.Panel GetPanelWithButtons() {

            Panel panel = new Panel();
            panel.CssClass = "dns_mb_main";
            okButton = new TextButton();
            okButton.Text = "OK";
            okButton.ButtonWidth = "70px";
            okButton.OnClientClick = GetCloseButtonClickScript();
            okButton.PredefinedStyle = TextButtonStyle.Green;
            panel.Controls.Add(okButton);

            return panel;
        }

        public override IEnumerable<System.Web.UI.ScriptDescriptor> GetScriptDescriptors() {

            ScriptControlDescriptor[] descriptors = base.GetScriptDescriptors() as ScriptControlDescriptor[];
            descriptors[0].Type = "DotNetSources.MessageBox";
            return descriptors;
        }

        public override IEnumerable<System.Web.UI.ScriptReference> GetScriptReferences() {

            ScriptReference jsReference = new ScriptReference();
            jsReference.Assembly = "DotNetSources.Web.UI.Buttons";
            jsReference.Name = "DotNetSources.Web.UI.Buttons.Popup.MessageBox.MessageBox.js";
            List<ScriptReference> ret = new List<ScriptReference>(base.GetScriptReferences());
            ret.Add(jsReference);
            return ret;
        }


        public override void Localize(params string[] args) {
            okButton.Text = args[0];    
        }

    }

}

