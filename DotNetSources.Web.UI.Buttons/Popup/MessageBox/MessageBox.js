﻿Type.registerNamespace('DotNetSources');

var dns_global_message_box;

DotNetSources.MessageBox = function(element) {
    DotNetSources.MessageBox.initializeBase(this, [element]);
    dns_global_message_box = this;
}

DotNetSources.MessageBox.get_instance = function() {
    return dns_global_message_box;
}

DotNetSources.MessageBox.prototype = {

    //
    // Override initialize
    //
    initialize: function() {
        DotNetSources.BoxBase.callBaseMethod(this, 'initialize');
    },

    //
    // Override Show
    //
    Show: function(message, icon) {
        DotNetSources.MessageBox.callBaseMethod(this, 'Show', [message, icon]);
    },

    dispose: function() {
        DotNetSources.BoxBase.callBaseMethod(this, 'dispose');
    }

}

DotNetSources.MessageBox.registerClass('DotNetSources.MessageBox', DotNetSources.BoxBase);