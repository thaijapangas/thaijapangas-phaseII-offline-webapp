﻿/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace DotNetSources.Web.UI.Buttons {

    /// <summary>
    /// The ImageButton
    /// </summary>
    public class ImageButton : ButtonBase<Image> {

        public string ImageUrl {
            get {
                string str = (string)this.ViewState["ImageUrl"];
                if (str != null) {
                    return str;
                }
                return string.Empty;
            }
            set {
                this.ViewState["ImageUrl"] = value;
            }
        }

        protected override Image CreateContentControl() {
            return new Image();
        }       

        protected override void OnPreRender(EventArgs e) {

            ContentControl.ImageUrl = ImageUrl;
            ContentControl.AlternateText = Text;

            base.OnPreRender(e);
        }

    }

}

