﻿/// Author: Petr Pechovic
/// Contact: http://www.pechovic.eu
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetSources.Web.UI.Buttons {

    /// <summary>
    /// Predefined styles from build in CSS.
    /// </summary>
    public enum TextButtonStyle {
        Red,
        Green,
        Undefined
    }

}
