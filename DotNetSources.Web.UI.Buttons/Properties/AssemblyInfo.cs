﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DotNetSources.Web.UI.Buttons")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Petr Pechovic")]
[assembly: AssemblyProduct("DotNetSources.Web.UI.Buttons")]
[assembly: AssemblyCopyright("Copyright © Petr Pechovic 2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("93dda357-8082-4f24-98bc-96ae822759ac")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

//
// Register icons
//
[assembly: WebResource("DotNetSources.Web.UI.Buttons.Icons.close.jpg", "image/jpeg")]
[assembly: WebResource("DotNetSources.Web.UI.Buttons.Icons.error.png", "image/png")]
[assembly: WebResource("DotNetSources.Web.UI.Buttons.Icons.info.png", "image/png")]
[assembly: WebResource("DotNetSources.Web.UI.Buttons.Icons.question.png", "image/png")]
[assembly: WebResource("DotNetSources.Web.UI.Buttons.Icons.warning.png", "image/png")]
