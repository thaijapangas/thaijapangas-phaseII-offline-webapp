﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Globalization;
using System.Linq;
using System.Web;
using Utilitys.Events;
using System.Configuration;
using System.Text;

namespace UI.Admin.Execute
{
    public class Execution : IDisposable
    {
        #region Member

        private SqlCeEngine sqlCEEngine;
        private SqlCeConnection sqlCEConnection;
        string connectionString = string.IsNullOrEmpty(ConfigurationManager.AppSettings["conn"].ToString()) ? @"Data Source=C:\Data Base SDF\TJG.sdf;Default Lock Timeout = 30000;Max Database Size = 2560;Password=pass;Persist Security Info=True" : ConfigurationManager.AppSettings["conn"].ToString().Trim();
        string connectionStringLogging = string.IsNullOrEmpty(ConfigurationManager.AppSettings["connLogging"].ToString()) ? @"Data Source=C:\Data Base SDF\Logging.sdf;Default Lock Timeout = 30000;Max Database Size = 2560;Password=pass;Persist Security Info=True" : ConfigurationManager.AppSettings["connLogging"].ToString().Trim();

        #endregion

        #region Constructor

        public Execution()
        {
            /*
            sqlCEEngine = new SqlCeEngine(connectionStringLogging);
            sqlCEEngine.Upgrade();

            sqlCEConnection = new SqlCeConnection(sqlCEEngine.LocalConnectionString);
            */
        }

        #endregion

        #region 0. getDateTime

        public DateTime GetDateServer()
        {
            try
            {
                DateTime dtpDateTime = DateTime.Now;
                System.Data.DataTable dataTable = new System.Data.DataTable();
                SqlCeConnection conn = new SqlCeConnection(connectionString);
                sqlCEEngine = new SqlCeEngine(connectionString);
                sqlCEEngine.Upgrade();
                conn.Open();

                SqlCeCommand command = conn.CreateCommand();
                command.CommandText = "SELECT GETDATE() AS getDateTime";
                command.CommandType = System.Data.CommandType.Text;
                SqlCeDataReader dr = command.ExecuteReader();
                dataTable.Load(dr);
                DataTable dtDateTime = dataTable;

                return (DateTime)dtDateTime.Rows[0]["getDateTime"];
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region 1. Search Logging By Criteria 

        public System.Data.DataTable getLoggingBycriteria(string startDate, string endDate, string errorCode)
        {
            System.Data.DataTable result;
            try
            {
                string _startDate = string.Empty;
                string _endDate = string.Empty;
                /* Upgrade database ce 3.5 to 4.0
                sqlCEEngine = new SqlCeEngine(connectionStringLogging);
                sqlCEEngine.Upgrade();
                */
                SqlCeConnection conn = new SqlCeConnection(connectionStringLogging);
                conn.Open();
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = string.Empty;
                if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate) && !string.IsNullOrEmpty(errorCode))
                {
                    //_startDate = startDate.Value.ToString("dd-MM-yy");
                    //_endDate = endDate.Value.ToString("dd-MM-yy");
                    //text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,5) >= @START_DATE AND CONVERT(nvarchar(11),create_date,5) <= @END_DATE) AND error_code LIKE '%"+ errorCode +"%' ";
                    text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,103) >= @START_DATE AND CONVERT(nvarchar(11),create_date,103) <= @END_DATE) AND error_code LIKE '%" + errorCode + "%' ";
                }
                else if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
                {
                    //_startDate = startDate.Value.ToString("dd-MM-yy");
                    //_endDate = endDate.Value.ToString("dd-MM-yy");
                    //text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,5) >= @START_DATE AND CONVERT(nvarchar(11),create_date,5) <= @END_DATE) ";
                    text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,103) >= @START_DATE AND CONVERT(nvarchar(11),create_date,103) <= @END_DATE) ";
                }
                else if(!string.IsNullOrEmpty(errorCode))
                {
                    text = "SELECT * FROM Action_Log WHERE error_code LIKE '%" + errorCode + "%' ";
                }
                else
                {
                    text = "SELECT * FROM Action_Log ";
                }
                
                
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.Connection = conn;
                sqlCeCommand.CommandType = System.Data.CommandType.Text;
                
                /*
                if(!string.IsNullOrEmpty(_startDate))
                    sqlCeCommand.Parameters.Add("@START_DATE", SqlDbType.NVarChar).Value = _startDate;
                if(!string.IsNullOrEmpty(_endDate))
                    sqlCeCommand.Parameters.Add("@END_DATE", SqlDbType.NVarChar).Value = _endDate;
                */

                if (!string.IsNullOrEmpty(startDate))
                    sqlCeCommand.Parameters.Add("@START_DATE", SqlDbType.NVarChar).Value = startDate;
                if (!string.IsNullOrEmpty(endDate))
                    sqlCeCommand.Parameters.Add("@END_DATE", SqlDbType.NVarChar).Value = endDate;

                sqlCeCommand.CommandText = text.ToString();
                SqlCeDataReader dr = sqlCeCommand.ExecuteReader();
                dataTable.Load(dr);
                conn.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        public System.Data.DataTable getPDALoggingBycriteria(string startDate, string endDate, string errorCode)
        {
            System.Data.DataTable result;
            try
            {
                string _startDate = string.Empty;
                string _endDate = string.Empty;
                string connectionString_temp = @"Data Source=C:\Data Base SDF\TJG.sdf;Default Lock Timeout = 30000;Max Database Size = 2560;Password=pass;Persist Security Info=True";
                string connectionString = string.IsNullOrEmpty(ConfigurationManager.AppSettings["conn"].ToString()) ? connectionString_temp : ConfigurationManager.AppSettings["conn"].ToString().Trim();
                SqlCeConnection conn = new SqlCeConnection(connectionString);
                conn.Open();
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = string.Empty;
                if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate) && !string.IsNullOrEmpty(errorCode))
                {
                    //_startDate = startDate.Value.ToString("dd-MM-yy");
                    //_endDate = endDate.Value.ToString("dd-MM-yy");
                    //text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,5) >= @START_DATE AND CONVERT(nvarchar(11),create_date,5) <= @END_DATE) AND error_code LIKE '%"+ errorCode +"%' ";
                    text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,103) >= @START_DATE AND CONVERT(nvarchar(11),create_date,103) <= @END_DATE) AND error_code LIKE '%" + errorCode + "%' ";
                }
                else if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
                {
                    //_startDate = startDate.Value.ToString("dd-MM-yy");
                    //_endDate = endDate.Value.ToString("dd-MM-yy");
                    //text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,5) >= @START_DATE AND CONVERT(nvarchar(11),create_date,5) <= @END_DATE) ";
                    text = "SELECT * FROM Action_Log where (CONVERT(nvarchar(11),create_date,103) >= @START_DATE AND CONVERT(nvarchar(11),create_date,103) <= @END_DATE) ";
                }
                else if (!string.IsNullOrEmpty(errorCode))
                {
                    text = "SELECT * FROM Action_Log WHERE error_code LIKE '%" + errorCode + "%' ";
                }
                else
                {
                    text = "SELECT * FROM Action_Log ";
                }


                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.Connection = conn;
                sqlCeCommand.CommandType = System.Data.CommandType.Text;

                /*
                if(!string.IsNullOrEmpty(_startDate))
                    sqlCeCommand.Parameters.Add("@START_DATE", SqlDbType.NVarChar).Value = _startDate;
                if(!string.IsNullOrEmpty(_endDate))
                    sqlCeCommand.Parameters.Add("@END_DATE", SqlDbType.NVarChar).Value = _endDate;
                */

                if (!string.IsNullOrEmpty(startDate))
                    sqlCeCommand.Parameters.Add("@START_DATE", SqlDbType.NVarChar).Value = startDate;
                if (!string.IsNullOrEmpty(endDate))
                    sqlCeCommand.Parameters.Add("@END_DATE", SqlDbType.NVarChar).Value = endDate;

                sqlCeCommand.CommandText = text.ToString();
                SqlCeDataReader dr = sqlCeCommand.ExecuteReader();
                dataTable.Load(dr);
                conn.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        #endregion

        #region 5. Insert Log

        public bool Insert_Log(string error_code, string error_message, string page_form, string method_name)
        {
            bool _result = false;
            SqlCeConnection conn = new SqlCeConnection(connectionStringLogging);
            conn.Open();
            try
            {
                
                StringBuilder sql = new StringBuilder();
                sql.Append("INSERT INTO Action_Log (id, error_code, error_message, page_form, method_name, create_date)  ");
                sql.Append(string.Format("VALUES ('{0}','{1}','{2}', '{3}', '{4}', getdate()) ", Guid.NewGuid().ToString().ToUpper().Trim(), error_code, error_message, page_form, method_name));

                SqlCeCommand command = new SqlCeCommand();
                command.Connection = conn;
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();

                if (command.ExecuteNonQuery() > 0)
                    _result = true;
                conn.Close();
            }
            catch (SqlCeException)
            {
                _result = false;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
            return _result;
        }

        #endregion

        #region 6. Activate Licence

        public string CheckExpireSoftware()
        {
            string connti = "FALSE";
            try
            {
                DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;

                System.Data.DataTable dt_licence = new System.Data.DataTable();
                sqlCEEngine = new SqlCeEngine(connectionString);
                sqlCEEngine.Upgrade();

                SqlCeConnection conn = new SqlCeConnection(connectionString);
                conn.Open();

                SqlCeCommand command = conn.CreateCommand();
                command.CommandText = "SELECT TOP(1) expire_date FROM t_setup";
                command.CommandType = System.Data.CommandType.Text;
                SqlCeDataReader dr = command.ExecuteReader();
                dt_licence.Load(dr);

                if (!dt_licence.IsNullOrNoRows())
                {
                    #region Has data

                    string _expire_date = dt_licence.Rows[0][0].ToString().Trim();
                    if (string.IsNullOrEmpty(_expire_date))
                    {
                        connti = "FALSE";
                    }
                    else if (!string.IsNullOrEmpty(_expire_date))
                    {
                        string _decry_expire_date = Convert.ToString(Convert.ToInt64(_expire_date) - 39335472);
                        if (_decry_expire_date.Trim() == "30000101")
                        {
                            connti = "LIFETIME";
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_decry_expire_date))
                            {
                                string _crrent_date = Convert.ToDateTime(GetDateServer()).ToString("yyyyMMdd", usDtfi);

                                if (Convert.ToInt64(_decry_expire_date) < Convert.ToInt64(_crrent_date))
                                    connti = "FALSE";
                                else if (Convert.ToInt64(_decry_expire_date) > Convert.ToInt64(_crrent_date))
                                    connti = "TRUE";
                                else
                                    connti = "FALSE";
                            }
                            else
                            {
                                connti = "FALSE";
                            }
                        }
                    }

                    #endregion
                }
            }
            catch (Exception)
            {
                connti = "FALSE";
            }

            return connti;
        }
        public bool ActivateLicence(string _licence_key)
        {
            bool result = false;
            try
            {
                System.Data.DataTable dataTable = new System.Data.DataTable();
                sqlCEEngine = new SqlCeEngine(connectionString);
                sqlCEEngine.Upgrade();

                SqlCeConnection conn = new SqlCeConnection(connectionString);
                conn.Open();

                string text = "UPDATE t_setup SET expire_date = '" + _licence_key.Trim() + "', update_date = GETDATE() ";
                SqlCeCommand command = conn.CreateCommand();
                command.CommandText = text;
                command.CommandType = System.Data.CommandType.Text;
                if (command.ExecuteNonQuery() > 0)
                    result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }
        public string ValidationLicence(string _licence_keys)
        {
            string Result = string.Empty;
            try
            {
                DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;
                string _licence_key = Convert.ToString((Convert.ToInt64(_licence_keys) - 39335472));
                string _crrent_date = Convert.ToDateTime(GetDateServer()).ToString("yyyyMMdd", usDtfi);
                if (!string.IsNullOrEmpty(_licence_key))
                {
                    if (_licence_key.Trim() == "30000101")
                    {
                        if (ActivateLicence(_licence_keys))
                        {
                            Result = "TRUE";
                        }
                    }
                    else if (_licence_key.IndexOf("2016") >= 0)
                    {
                        if (ActivateLicence(_licence_keys))
                        {
                            DateTime dTime = DateTime.ParseExact(_licence_key, "yyyyMMdd", null);
                            string display_date = dTime.ToLongDateString();
                            Result = "TRUE_EXPRIE|" + display_date;
                        }
                    }
                    else if (Convert.ToInt32(_licence_key) < Convert.ToInt32(_crrent_date))
                    {
                        Result = "TRUE_EXPIRED";
                    }
                    else
                    {
                        Result = "FALSE";
                    }
                }
            }
            catch (Exception)
            {
                Result = "FALSE";
            }

            return Result;
        }

        #endregion

        #region 7. View Logging

        /*
        public System.Data.DataTable getLogging()
        {
            System.Data.DataTable result;
            try
            {
                SqlCeConnection conn = new SqlCeConnection(connectionStringLogging);
                conn.Open();
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = "SELECT * FROM Action_Log";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.Connection = conn;
                sqlCeCommand.CommandType = System.Data.CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                SqlCeDataReader dr = sqlCeCommand.ExecuteReader();
                dataTable.Load(dr);
                conn.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        */

        /*
        public System.Data.DataTable getPDALogging()
        {
            System.Data.DataTable result;
            try
            {
                string connectionString_temp = @"Data Source=C:\Data Base SDF\TJG.sdf;Default Lock Timeout = 30000;Max Database Size = 2560;Password=pass;Persist Security Info=True";
                string connectionString = string.IsNullOrEmpty(ConfigurationManager.AppSettings["conn"].ToString()) ? connectionString_temp : ConfigurationManager.AppSettings["conn"].ToString().Trim();
                SqlCeConnection conn = new SqlCeConnection(connectionString);
                conn.Open();
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = "SELECT * FROM Action_Log";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.Connection = conn;
                sqlCeCommand.CommandType = System.Data.CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                SqlCeDataReader dr = sqlCeCommand.ExecuteReader();
                dataTable.Load(dr);
                conn.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        */

        #endregion

        #region Dispose
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}