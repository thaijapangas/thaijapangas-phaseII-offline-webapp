﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ActivateLicense.aspx.cs" Inherits="UI.Admin.ActivateLicense" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Thai Japan Gas</title>
    <script src="_js/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="_css/btnbootstrap.css" />
    <link rel="stylesheet" href="_css/textbox.css" />
    <link rel="stylesheet" href="_css/element-view.css" />

    <link rel="stylesheet" href="_css/popup.css" />
    <link rel="stylesheet" href="_css/button.css" />
    <link rel="stylesheet" type="text/css" href="../_css/main.css" />
    <link rel="stylesheet" type="text/css" href="../_css/styles.css" />
    <script src="../_js/script.js" type="text/javascript"></script>
    <script src="_js/jquery-latest.min.js" type="text/javascript"></script>
    <script src="_js/jquery1.7.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="_js/jquery.min.js"></script>
    <style type="text/css">
        .TestTop{
            vertical-align: top;
        }
    </style>
</head>
<body style="font-family: 'Open Sans', sans-serif;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager" runat="server" />
        <asp:UpdatePanel ID="upnmain" runat="server">
            <ContentTemplate>
                <div class="header">
                    <div class="fullpage">
                        <div id='cssmenu'>
                            <table>
                                <tr>
                                    <td style="height: 20px;"></td>
                                </tr>
                            </table>
                            <div class="login" id="bt_login">
                                <%--<a href="Login.aspx" style="text-decoration: none; color: black;">--%>
                                <a href="#" style="text-decoration: none; color: black;">
                                    <asp:Label ID="lblLogout" runat="server" Text="Log out" ForeColor="White" Font-Bold="true" Width="55px"></asp:Label>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="divHead">
                    <table style="width: 95%; margin-left: auto; margin-right: auto;" cellpadding="3" cellspacing="3">
                        <tr>
                            <td style="height: 50px;"></td>
                        </tr>
                        <tr>
                            <td align="center" style="font-weight: bold;">
                                <table style="width: 40%; background-color: #f2f1f1; border-radius: 8px 8px 8px 8px;" cellpadding="2"
                                    cellspacing="2">
                                    <tr>
                                        <td style="background-color: #006699; width: 100%; height: 45px; border-radius: 6px 6px 6px 6px; text-align: center;">
                                            <asp:Label ID="lblTitle" runat="server" Text="Activate License" ForeColor="White" Font-Size="30px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 15px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 10px; text-align: center;">
                                            <asp:Label ID="lblExpire" runat="server" Text="Licences key สามารถใช้งานได้ กรุณาลงทะเบียน Licences key ที่โปรแกรมบน PDA -> เมนู 'Activate Licences'" ForeColor="Black" Font-Size="28px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="width: 550px; height: 47px; display:none;">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblName" runat="server" Text="Activating Key :" Visible="false" CssClass="TestTop"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:TextBox ID="txtActivateKeys" runat="server" CssClass="txt_login" Visible="false" TextMode="Password" placeholder="License Keys"></asp:TextBox><br />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 10px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" Visible="false" CssClass="btn btn-success" OnClick="btnSubmit_Click" />
                                            &nbsp;&nbsp;
                                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-danger" OnClick="btnBack_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 10px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 10px; text-align: left;">
                                            <asp:Label ID="lblInformation" runat="server" Text="Activating key has been for unlock software provide in the activate / Reactivate form"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnBack" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
