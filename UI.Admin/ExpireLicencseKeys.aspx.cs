﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin
{
    public partial class ExpireLicencseKeys : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["secLogin"] == null)
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["activate"]))
            {
                if (Request.QueryString["activate"].ToString().Trim() == "EXPIRE")
                {
                    Response.Redirect("~/Mamgements.aspx");
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }
        }
    }
}