﻿using DataAccess.Customers;
using Entitys.Customers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;
using UI.Admin.Execute;

namespace UI.Admin
{
    public partial class Mamgements : System.Web.UI.Page
    {
        #region Member

        // Page Customer
        int Cus_pageSize = 15;
        //int totalUsers = 0;
        int Cus_totalPages = 0;
        int Cus_currentPage = 1;

        // Page Loading
        int pageSize = 15;
        //int totalUsers = 0;
        int totalPages = 0;
        int currentPage = 1;

        // Page Items
        int Items_pageSize = 15;
        int Items_totalPages = 0;
        int Items_currentPage = 1;

        //DataTable
        DataTable dtReload;
        DataTable dtReloadItem;
        DataTable dtRead = new DataTable();

        #endregion

        #region Focus TextBox

        void PageSetFocus(Control ctrl)
        {
            ScriptManager.GetCurrent(this.Page).SetFocus(ctrl);
        }

        #endregion

        #region Property

        DataTable dtReceive = new DataTable();
        /// <New object RAPI.>
        /// 
        /// </summary>
        private static OpenNETCF.Desktop.Communication.RAPI rapi;

        System.Data.DataTable gIDelivery;
        System.Data.DataTable distributionRet;
        System.Data.DataTable gRFromProduction;
        System.Data.DataTable gIRefIOEmpty;

        DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;

        #endregion

        #region Method

        // Connect Device
        private void ConnectDevice()
        {
            rapi = new OpenNETCF.Desktop.Communication.RAPI();
            try
            {
                if ((rapi.DevicePresent))
                {
                    //"Connect HandHeld Success";
                    lblResultSync.Text = "Sync HandHeld";
                    lblResultSync.ForeColor = Color.Green;
                }
                else
                {
                    //"Connect HandHeld Not Success";
                    lblResultSync.Text = "Not Sync HandHeld";
                    lblResultSync.ForeColor = Color.Red;
                }
            }
            catch (Exception)
            {
                rapi.Dispose();
            }
        }
        // Paging Items.
        private void ReloadItems()
        {
            dtReloadItem = DataAccess.Items.Items.Instance.getItems();
            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("guid");
            dtSubReload.Columns.Add("temp_item_code");
            dtSubReload.Columns.Add("full_item_code");
            dtSubReload.Columns.Add("item_name");
            dtSubReload.Columns.Add("is_active");
            dtSubReload.Columns.Add("create_date");

            if (dtReloadItem.Rows.Count > 0)
            {
                dgvItems.DataSource = dtReloadItem;
                dgvItems.DataBind();

                //Calculate split page.
                int counter = 0;
                int pageIndex = Items_currentPage - 1;
                int startIndex = Items_pageSize * pageIndex;
                int endIndex = startIndex + Items_pageSize - 1;

                if (dtReloadItem.Rows.Count > 0 || dtReloadItem != null)
                {
                    foreach (DataRow itemReload in dtReloadItem.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["guid"].ToString().Trim();
                            drReload[1] = itemReload["temp_item_code"].ToString().Trim();
                            drReload[2] = itemReload["full_item_code"].ToString().Trim();
                            drReload[3] = itemReload["item_name"].ToString().Trim();
                            drReload[4] = itemReload["is_active"].ToString().Trim();
                            drReload[5] = itemReload["create_date"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblItemsSearchingCount.Visible = false;
                }
                else if (dtReloadItem.Rows.Count == 0 || dtReloadItem == null)
                {
                    //Not found data.
                    this.lblItemsSearchingCount.Visible = true;
                    this.lblItemsSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvItems.DataSource = dtReloadItem;
                    dgvItems.DataBind();

                    ItemsNextButton.Visible = false;
                    ItemsEndpage.Visible = false;
                    ItemsPreviousButton.Visible = false;
                    ItemsFirstpage.Visible = false;

                    this.lblItemspageIndex.Visible = false;
                    this.txtItemspageIndex.Visible = false;
                    this.btnItemsGO.Visible = false;

                    ItemsCurrentPageLabel.Text = "0";
                    ItemsTotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                ItemsNextButton.Visible = true;
                ItemsEndpage.Visible = true;
                ItemsPreviousButton.Visible = true;
                ItemsFirstpage.Visible = true;

                dgvItems.DataSource = dtSubReload;

                Items_totalPages = ((dtReloadItem.Rows.Count - 1) / Items_pageSize) + 1;

                if (Items_currentPage > Items_totalPages)
                {
                    Items_currentPage = Items_totalPages;
                    ReloadItems();
                    return;
                }

                dgvItems.DataBind();
                ItemsCurrentPageLabel.Text = Items_currentPage.ToString();
                ItemsTotalPagesLabel.Text = Items_totalPages.ToString();

                if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblItemspageIndex.Visible = false;
                    this.txtItemspageIndex.Visible = false;
                    this.btnItemsGO.Visible = false;
                }
                else if (this.ItemsTotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblItemspageIndex.Visible = true;
                    this.txtItemspageIndex.Visible = true;
                    this.btnItemsGO.Visible = true;
                }

                if (Items_currentPage == Items_totalPages)
                {
                    ItemsNextButton.Visible = false;
                    ItemsEndpage.Visible = false;
                }
                else
                {
                    ItemsNextButton.Visible = true;
                    ItemsEndpage.Visible = true;
                }

                if (Items_currentPage == 1)
                {
                    ItemsFirstpage.Visible = false;
                    ItemsPreviousButton.Visible = false;
                }
                else
                {
                    ItemsFirstpage.Visible = true;
                    ItemsPreviousButton.Visible = true;
                }

                if (dtReloadItem.Rows.Count <= 0)
                    NavigationPanelItems.Visible = false;
                else
                    NavigationPanelItems.Visible = true;
            }
        }
        void startPagingItems(int startPage)
        {
            this.txtItemspageIndex.Text = string.Empty;
            dtReloadItem = DataAccess.Items.Items.Instance.getItems();

            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("guid");
            dtSubReload.Columns.Add("temp_item_code");
            dtSubReload.Columns.Add("full_item_code");
            dtSubReload.Columns.Add("item_name");
            dtSubReload.Columns.Add("is_active");
            dtSubReload.Columns.Add("create_date");

            //Calculate split page.
            int counter = 0;
            int pageIndex = startPage - 1;
            int startIndex = Items_pageSize * pageIndex;
            int endIndex = startIndex + Items_pageSize - 1;

            if (dtReloadItem.Rows.Count > 0 || dtReloadItem != null)
            {
                foreach (DataRow itemReload in dtReloadItem.Rows)
                {
                    if (counter >= startIndex)
                    {
                        DataRow drReload = dtSubReload.NewRow();
                        drReload[0] = itemReload["guid"].ToString().Trim();
                        drReload[1] = itemReload["temp_item_code"].ToString().Trim();
                        drReload[2] = itemReload["full_item_code"].ToString().Trim();
                        drReload[3] = itemReload["item_name"].ToString().Trim();
                        drReload[4] = itemReload["is_active"].ToString().Trim();
                        drReload[5] = itemReload["create_date"].ToString().Trim();

                        dtSubReload.Rows.Add(drReload);
                    }

                    if (counter >= endIndex) { break; }
                    counter++;
                }

                this.lblItemsSearchingCount.Visible = false;
            }
            else if (dtReload == null || dtReload.Rows.Count == 0)
            {
                //Not found data.
                this.lblItemsSearchingCount.Visible = true;
                this.lblItemsSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                dgvItems.DataSource = dtReloadItem;
                dgvItems.DataBind();

                ItemsNextButton.Visible = false;
                ItemsEndpage.Visible = false;
                ItemsPreviousButton.Visible = false;
                ItemsFirstpage.Visible = false;

                this.lblItemspageIndex.Visible = false;
                this.txtItemspageIndex.Visible = false;
                this.btnItemsGO.Visible = false;

                ItemsCurrentPageLabel.Text = "0";
                ItemsTotalPagesLabel.Text = "0";
                return;
            }

            //Control page.
            ItemsNextButton.Visible = true;
            ItemsEndpage.Visible = true;
            ItemsPreviousButton.Visible = true;
            ItemsFirstpage.Visible = true;

            dgvItems.DataSource = dtSubReload;

            Items_totalPages = ((dtReloadItem.Rows.Count - 1) / Items_pageSize) + 1;

            if (startPage > Items_totalPages)
            {
                startPage = Items_totalPages;
                startPagingItems(startPage);
                return;
            }

            dgvItems.DataBind();
            ItemsCurrentPageLabel.Text = startPage.ToString();
            ItemsTotalPagesLabel.Text = Items_totalPages.ToString();

            if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
            {
                this.lblItemspageIndex.Visible = false;
                this.txtItemspageIndex.Visible = false;
                this.btnItemsGO.Visible = false;
            }
            else if (this.ItemsTotalPagesLabel.Text.Trim() != "1")
            {
                this.lblItemspageIndex.Visible = true;
                this.txtItemspageIndex.Visible = true;
                this.btnItemsGO.Visible = true;
            }

            if (startPage == Items_totalPages)
            {
                ItemsNextButton.Visible = false;
                ItemsEndpage.Visible = false;
            }
            else
            {
                ItemsNextButton.Visible = true;
                ItemsEndpage.Visible = true;
            }

            if (startPage == 1)
            {
                ItemsFirstpage.Visible = false;
                ItemsPreviousButton.Visible = false;
            }
            else
            {
                ItemsFirstpage.Visible = true;
                ItemsPreviousButton.Visible = true;
            }

            if (dtReloadItem.Rows.Count <= 0)
                NavigationPanelItems.Visible = false;
            else
                NavigationPanelItems.Visible = true;
        }
        void endPagingItems(int lastPage)
        {
            this.txtItemspageIndex.Text = string.Empty;
            dtReloadItem = DataAccess.Items.Items.Instance.getItems();
            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("guid");
            dtSubReload.Columns.Add("temp_item_code");
            dtSubReload.Columns.Add("full_item_code");
            dtSubReload.Columns.Add("item_name");
            dtSubReload.Columns.Add("is_active");
            dtSubReload.Columns.Add("create_date");

            //Calculate split page.
            int counter = 0;
            int pageIndex = lastPage - 1;
            int startIndex = Items_pageSize * pageIndex;
            int endIndex = startIndex + Items_pageSize - 1;

            if (dtReloadItem.Rows.Count > 0 || dtReloadItem != null)
            {
                foreach (DataRow itemReload in dtReload.Rows)
                {
                    if (counter >= startIndex)
                    {
                        DataRow drReload = dtSubReload.NewRow();
                        drReload[0] = itemReload["guid"].ToString().Trim();
                        drReload[1] = itemReload["temp_item_code"].ToString().Trim();
                        drReload[2] = itemReload["full_item_code"].ToString().Trim();
                        drReload[3] = itemReload["item_name"].ToString().Trim();
                        drReload[4] = itemReload["is_active"].ToString().Trim();
                        drReload[5] = itemReload["create_date"].ToString().Trim();

                        dtSubReload.Rows.Add(drReload);
                    }

                    if (counter >= endIndex) { break; }
                    counter++;
                }

                this.lblItemsSearchingCount.Visible = false;
            }
            else if (dtReloadItem == null || dtReloadItem.Rows.Count == 0)
            {
                //Not found data.
                this.lblItemsSearchingCount.Visible = true;
                this.lblItemsSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                dgvItems.DataSource = dtReloadItem;
                dgvItems.DataBind();

                ItemsNextButton.Visible = false;
                ItemsEndpage.Visible = false;
                ItemsPreviousButton.Visible = false;
                ItemsFirstpage.Visible = false;

                this.lblItemspageIndex.Visible = false;
                this.txtItemspageIndex.Visible = false;
                this.btnItemsGO.Visible = false;

                ItemsCurrentPageLabel.Text = "0";
                ItemsTotalPagesLabel.Text = "0";
                return;
            }

            //Control page.
            ItemsNextButton.Visible = true;
            ItemsEndpage.Visible = true;
            ItemsPreviousButton.Visible = true;
            ItemsFirstpage.Visible = true;

            dgvItems.DataSource = dtSubReload;

            Items_totalPages = ((dtReloadItem.Rows.Count - 1) / Items_pageSize) + 1;

            if (lastPage > Items_totalPages)
            {
                lastPage = Items_totalPages;
                endPagingItems(lastPage);
                return;
            }

            dgvItems.DataBind();
            ItemsCurrentPageLabel.Text = lastPage.ToString();
            ItemsTotalPagesLabel.Text = Items_totalPages.ToString();

            if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
            {
                this.lblItemspageIndex.Visible = false;
                this.txtItemspageIndex.Visible = false;
                this.btnItemsGO.Visible = false;
            }
            else if (this.ItemsTotalPagesLabel.Text.Trim() != "1")
            {
                this.lblItemspageIndex.Visible = true;
                this.txtItemspageIndex.Visible = true;
                this.btnCusGO.Visible = true;
            }

            if (lastPage == Items_totalPages)
            {
                ItemsNextButton.Visible = false;
                ItemsEndpage.Visible = false;
            }
            else
            {
                ItemsNextButton.Visible = true;
                ItemsEndpage.Visible = true;
            }

            if (lastPage == 1)
            {
                ItemsFirstpage.Visible = false;
                ItemsPreviousButton.Visible = false;
            }
            else
            {
                ItemsFirstpage.Visible = true;
                ItemsPreviousButton.Visible = true;
            }

            if (dtReloadItem.Rows.Count <= 0)
                NavigationPanelItems.Visible = false;
            else
                NavigationPanelItems.Visible = true;
        }
        void gotoPageItems(int index)
        {
            if (index <= 0)
            {
                if (this.ItemsTotalPagesLabel.Text.Trim() == "0")
                {
                    //Alert.
                    Page.ScriptJqueryMessage("Not found data of page", JMessageType.Warning);
                    this.txtItemspageIndex.Text = string.Empty;
                    return;
                }
                else
                {
                    //Alert.
                    Page.ScriptJqueryMessage("Input page correct format", JMessageType.Warning);
                    this.txtItemspageIndex.Text = string.Empty;
                    return;
                }
            }
            else
            {
                if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
                {
                    this.txtItemspageIndex.Attributes.Add("onfocus", "selectText();");
                    PageSetFocus(this.txtItemspageIndex);
                    return;
                }
                else if (index > Convert.ToInt32(this.ItemsTotalPagesLabel.Text.Trim()))
                {
                    this.txtCuspageIndex.Text = string.Empty;
                    PageSetFocus(this.txtItemspageIndex);
                    return;
                }
                else
                {
                    #region Go to Page

                    dtReload = Customer.Instance.getCustomer();
                    DataTable goPage = new DataTable();

                    //Add colunms
                    goPage.Columns.Add("guid");
                    goPage.Columns.Add("temp_item_code");
                    goPage.Columns.Add("full_item_code");
                    goPage.Columns.Add("item_name");
                    goPage.Columns.Add("is_active");
                    goPage.Columns.Add("create_date");

                    int counter = 0;
                    int pageIndex = index - 1;
                    int startIndex = Cus_pageSize * pageIndex;
                    int endIndex = startIndex + Cus_pageSize - 1;

                    if (dtReloadItem.Rows.Count > 0 || dtReloadItem != null)
                    {
                        foreach (DataRow itemReload in dtReloadItem.Rows)
                        {
                            if (counter >= startIndex)
                            {
                                DataRow drgoPage = goPage.NewRow();
                                drgoPage[0] = itemReload["guid"].ToString().Trim();
                                drgoPage[1] = itemReload["temp_item_code"].ToString().Trim();
                                drgoPage[2] = itemReload["full_item_code"].ToString().Trim();
                                drgoPage[3] = itemReload["item_name"].ToString().Trim();
                                drgoPage[4] = itemReload["is_active"].ToString().Trim();
                                drgoPage[5] = itemReload["create_date"].ToString().Trim();
                                goPage.Rows.Add(drgoPage);
                            }

                            if (counter >= endIndex) { break; }
                            counter++;
                        }

                        this.lblItemsSearchingCount.Visible = false;
                    }
                    else if (dtReloadItem == null || dtReloadItem.Rows.Count == 0)
                    {
                        //Not found data.
                        this.lblItemsSearchingCount.Visible = true;
                        this.lblItemsSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                        dgvItems.DataSource = goPage;
                        dgvItems.DataBind();

                        ItemsNextButton.Visible = false;
                        ItemsEndpage.Visible = false;
                        ItemsPreviousButton.Visible = false;
                        ItemsFirstpage.Visible = false;

                        this.lblItemspageIndex.Visible = false;
                        this.txtItemspageIndex.Visible = false;
                        this.btnItemsGO.Visible = false;

                        ItemsCurrentPageLabel.Text = "0";
                        ItemsTotalPagesLabel.Text = "0";
                        return;
                    }

                    //Control page.
                    ItemsNextButton.Visible = true;
                    ItemsEndpage.Visible = true;
                    ItemsPreviousButton.Visible = true;
                    ItemsFirstpage.Visible = true;

                    dgvItems.DataSource = goPage;

                    Items_totalPages = ((dtReloadItem.Rows.Count - 1) / Items_pageSize) + 1;

                    if (index > Items_totalPages)
                    {
                        index = Items_totalPages;
                        gotoPageItems(index);
                        return;
                    }

                    dgvItems.DataBind();
                    ItemsCurrentPageLabel.Text = index.ToString();
                    ItemsTotalPagesLabel.Text = Items_totalPages.ToString();

                    if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
                    {
                        this.lblItemspageIndex.Visible = false;
                        this.txtItemspageIndex.Visible = false;
                        this.btnItemsGO.Visible = false;
                    }
                    else if (this.ItemsTotalPagesLabel.Text.Trim() != "1")
                    {
                        this.lblItemspageIndex.Visible = true;
                        this.txtItemspageIndex.Visible = true;
                        this.btnItemsGO.Visible = true;
                    }

                    if (index == Items_totalPages)
                    {
                        ItemsNextButton.Visible = false;
                        ItemsEndpage.Visible = false;
                    }
                    else
                    {
                        ItemsNextButton.Visible = true;
                        ItemsEndpage.Visible = true;
                    }

                    if (index == 1)
                    {
                        ItemsFirstpage.Visible = false;
                        ItemsPreviousButton.Visible = false;
                    }
                    else
                    {
                        ItemsFirstpage.Visible = true;
                        ItemsPreviousButton.Visible = true;
                    }

                    if (dtReloadItem.Rows.Count <= 0)
                        NavigationPanelItems.Visible = false;
                    else
                        NavigationPanelItems.Visible = true;

                    #endregion
                }
            }
        }

        //Paging Customer.
        private void Reload()
        {
            dtReload = Customer.Instance.getCustomer();
            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("guid");
            dtSubReload.Columns.Add("customer_code");
            dtSubReload.Columns.Add("customer_name");
            dtSubReload.Columns.Add("is_active");
            dtSubReload.Columns.Add("create_date");

            if (dtReload.Rows.Count > 0)
            {
                dgvCustomer.DataSource = dtReload;
                dgvCustomer.DataBind();

                //Calculate split page.
                int counter = 0;
                int pageIndex = Cus_currentPage - 1;
                int startIndex = Cus_pageSize * pageIndex;
                int endIndex = startIndex + Cus_pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["guid"].ToString().Trim();
                            drReload[1] = itemReload["customer_code"].ToString().Trim();
                            drReload[2] = itemReload["customer_name"].ToString().Trim();
                            drReload[3] = itemReload["is_active"].ToString().Trim();
                            drReload[4] = itemReload["create_date"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblCusSearchingCount.Visible = false;
                }
                else if (dtReload.Rows.Count == 0 || dtReload == null)
                {
                    //Not found data.
                    this.lblCusSearchingCount.Visible = true;
                    this.lblCusSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvCustomer.DataSource = dtReload;
                    dgvCustomer.DataBind();

                    CusNextButton.Visible = false;
                    CusEndpage.Visible = false;
                    CusPreviousButton.Visible = false;
                    CusFirstpage.Visible = false;

                    this.lblCuspageIndex.Visible = false;
                    this.txtCuspageIndex.Visible = false;
                    this.btnCusGO.Visible = false;

                    CusCurrentPageLabel.Text = "0";
                    CusTotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                CusNextButton.Visible = true;
                CusEndpage.Visible = true;
                CusPreviousButton.Visible = true;
                CusFirstpage.Visible = true;

                dgvCustomer.DataSource = dtSubReload;

                Cus_totalPages = ((dtReload.Rows.Count - 1) / Cus_pageSize) + 1;

                if (Cus_currentPage > Cus_totalPages)
                {
                    Cus_currentPage = Cus_totalPages;
                    Reload();
                    return;
                }

                dgvCustomer.DataBind();
                CusCurrentPageLabel.Text = Cus_currentPage.ToString();
                CusTotalPagesLabel.Text = Cus_totalPages.ToString();

                if (this.CusTotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblCuspageIndex.Visible = false;
                    this.txtCuspageIndex.Visible = false;
                    this.btnCusGO.Visible = false;
                }
                else if (this.CusTotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblCuspageIndex.Visible = true;
                    this.txtCuspageIndex.Visible = true;
                    this.btnCusGO.Visible = true;
                }

                if (Cus_currentPage == Cus_totalPages)
                {
                    CusNextButton.Visible = false;
                    CusEndpage.Visible = false;
                }
                else
                {
                    CusNextButton.Visible = true;
                    CusEndpage.Visible = true;
                }

                if (Cus_currentPage == 1)
                {
                    CusFirstpage.Visible = false;
                    CusPreviousButton.Visible = false;
                }
                else
                {
                    CusFirstpage.Visible = true;
                    CusPreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanelCustomer.Visible = false;
                else
                    NavigationPanelCustomer.Visible = true;
            }
        }
        void startPaging(int startPage)
        {
            this.txtCuspageIndex.Text = string.Empty;
            dtReload = Customer.Instance.getCustomer();

            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("guid");
            dtSubReload.Columns.Add("customer_code");
            dtSubReload.Columns.Add("customer_name");
            dtSubReload.Columns.Add("is_active");
            dtSubReload.Columns.Add("create_date");

            //Calculate split page.
            int counter = 0;
            int pageIndex = startPage - 1;
            int startIndex = Cus_pageSize * pageIndex;
            int endIndex = startIndex + Cus_pageSize - 1;

            if (dtReload.Rows.Count > 0 || dtReload != null)
            {
                foreach (DataRow itemReload in dtReload.Rows)
                {
                    if (counter >= startIndex)
                    {
                        DataRow drReload = dtSubReload.NewRow();
                        drReload[0] = itemReload["guid"].ToString().Trim();
                        drReload[1] = itemReload["customer_code"].ToString().Trim();
                        drReload[2] = itemReload["customer_name"].ToString().Trim();
                        drReload[3] = itemReload["is_active"].ToString().Trim();
                        drReload[4] = itemReload["create_date"].ToString().Trim();

                        dtSubReload.Rows.Add(drReload);
                    }

                    if (counter >= endIndex) { break; }
                    counter++;
                }

                this.lblCusSearchingCount.Visible = false;
            }
            else if (dtReload == null || dtReload.Rows.Count == 0)
            {
                //Not found data.
                this.lblCusSearchingCount.Visible = true;
                this.lblCusSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                dgvCustomer.DataSource = dtReload;
                dgvCustomer.DataBind();

                CusNextButton.Visible = false;
                CusEndpage.Visible = false;
                CusPreviousButton.Visible = false;
                CusFirstpage.Visible = false;

                this.lblCuspageIndex.Visible = false;
                this.txtCuspageIndex.Visible = false;
                this.btnCusGO.Visible = false;

                CusCurrentPageLabel.Text = "0";
                CusTotalPagesLabel.Text = "0";
                return;
            }

            //Control page.
            CusNextButton.Visible = true;
            CusEndpage.Visible = true;
            CusPreviousButton.Visible = true;
            CusFirstpage.Visible = true;

            dgvCustomer.DataSource = dtSubReload;

            Cus_totalPages = ((dtReload.Rows.Count - 1) / Cus_pageSize) + 1;

            if (startPage > Cus_totalPages)
            {
                startPage = Cus_totalPages;
                startPaging(startPage);
                return;
            }

            dgvCustomer.DataBind();
            CusCurrentPageLabel.Text = startPage.ToString();
            CusTotalPagesLabel.Text = Cus_totalPages.ToString();

            if (this.CusTotalPagesLabel.Text.Trim() == "1")
            {
                this.lblCuspageIndex.Visible = false;
                this.txtCuspageIndex.Visible = false;
                this.btnCusGO.Visible = false;
            }
            else if (this.CusTotalPagesLabel.Text.Trim() != "1")
            {
                this.lblCuspageIndex.Visible = true;
                this.txtCuspageIndex.Visible = true;
                this.btnCusGO.Visible = true;
            }

            if (startPage == Cus_totalPages)
            {
                CusNextButton.Visible = false;
                CusEndpage.Visible = false;
            }
            else
            {
                CusNextButton.Visible = true;
                CusEndpage.Visible = true;
            }

            if (startPage == 1)
            {
                CusFirstpage.Visible = false;
                CusPreviousButton.Visible = false;
            }
            else
            {
                CusFirstpage.Visible = true;
                CusPreviousButton.Visible = true;
            }

            if (dtReload.Rows.Count <= 0)
                NavigationPanelCustomer.Visible = false;
            else
                NavigationPanelCustomer.Visible = true;
        }
        void endPaging(int lastPage)
        {
            this.txtpageIndex.Text = string.Empty;
            dtReload = Customer.Instance.getCustomer();
            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("guid");
            dtSubReload.Columns.Add("customer_code");
            dtSubReload.Columns.Add("customer_name");
            dtSubReload.Columns.Add("is_active");
            dtSubReload.Columns.Add("create_date");

            //Calculate split page.
            int counter = 0;
            int pageIndex = lastPage - 1;
            int startIndex = Cus_pageSize * pageIndex;
            int endIndex = startIndex + Cus_pageSize - 1;

            if (dtReload.Rows.Count > 0 || dtReload != null)
            {
                foreach (DataRow itemReload in dtReload.Rows)
                {
                    if (counter >= startIndex)
                    {
                        DataRow drReload = dtSubReload.NewRow();
                        drReload[0] = itemReload["guid"].ToString().Trim();
                        drReload[1] = itemReload["customer_code"].ToString().Trim();
                        drReload[2] = itemReload["customer_name"].ToString().Trim();
                        drReload[3] = itemReload["is_active"].ToString().Trim();
                        drReload[4] = itemReload["create_date"].ToString().Trim();

                        dtSubReload.Rows.Add(drReload);
                    }

                    if (counter >= endIndex) { break; }
                    counter++;
                }

                this.lblCusSearchingCount.Visible = false;
            }
            else if (dtReload == null || dtReload.Rows.Count == 0)
            {
                //Not found data.
                this.lblCusSearchingCount.Visible = true;
                this.lblCusSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                dgvCustomer.DataSource = dtReload;
                dgvCustomer.DataBind();

                CusNextButton.Visible = false;
                CusEndpage.Visible = false;
                CusPreviousButton.Visible = false;
                CusFirstpage.Visible = false;

                this.lblCuspageIndex.Visible = false;
                this.txtCuspageIndex.Visible = false;
                this.btnCusGO.Visible = false;

                CusCurrentPageLabel.Text = "0";
                CusTotalPagesLabel.Text = "0";
                return;
            }

            //Control page.
            CusNextButton.Visible = true;
            CusEndpage.Visible = true;
            CusPreviousButton.Visible = true;
            CusFirstpage.Visible = true;

            dgvCustomer.DataSource = dtSubReload;

            Cus_totalPages = ((dtReload.Rows.Count - 1) / Cus_pageSize) + 1;

            if (lastPage > Cus_totalPages)
            {
                lastPage = Cus_totalPages;
                endPaging(lastPage);
                return;
            }

            dgvCustomer.DataBind();
            CusCurrentPageLabel.Text = lastPage.ToString();
            CusTotalPagesLabel.Text = Cus_totalPages.ToString();

            if (this.CusTotalPagesLabel.Text.Trim() == "1")
            {
                this.lblCuspageIndex.Visible = false;
                this.txtCuspageIndex.Visible = false;
                this.btnCusGO.Visible = false;
            }
            else if (this.CusTotalPagesLabel.Text.Trim() != "1")
            {
                this.lblCuspageIndex.Visible = true;
                this.txtCuspageIndex.Visible = true;
                this.btnCusGO.Visible = true;
            }

            if (lastPage == Cus_totalPages)
            {
                CusNextButton.Visible = false;
                CusEndpage.Visible = false;
            }
            else
            {
                CusNextButton.Visible = true;
                CusEndpage.Visible = true;
            }

            if (lastPage == 1)
            {
                CusFirstpage.Visible = false;
                CusPreviousButton.Visible = false;
            }
            else
            {
                Firstpage.Visible = true;
                PreviousButton.Visible = true;
            }

            if (dtReload.Rows.Count <= 0)
                NavigationPanelCustomer.Visible = false;
            else
                NavigationPanelCustomer.Visible = true;
        }
        void gotoPage(int index)
        {
            if (index <= 0)
            {
                if (this.CusTotalPagesLabel.Text.Trim() == "0")
                {
                    //Alert.
                    Page.ScriptJqueryMessage("Not found data of page", JMessageType.Warning);
                    this.txtCuspageIndex.Text = string.Empty;
                    return;
                }
                else
                {
                    //Alert.
                    Page.ScriptJqueryMessage("Input page correct format", JMessageType.Warning);
                    this.txtCuspageIndex.Text = string.Empty;
                    return;
                }
            }
            else
            {
                if (this.CusTotalPagesLabel.Text.Trim() == "1")
                {
                    this.txtCuspageIndex.Attributes.Add("onfocus", "selectText();");
                    PageSetFocus(this.txtCuspageIndex);
                    return;
                }
                else if (index > Convert.ToInt32(this.CusTotalPagesLabel.Text.Trim()))
                {
                    this.txtCuspageIndex.Text = string.Empty;
                    PageSetFocus(this.txtCuspageIndex);
                    return;
                }
                else
                {
                    #region Go to Page

                    dtReload = Customer.Instance.getCustomer();
                    DataTable goPage = new DataTable();

                    //Add colunms
                    goPage.Columns.Add("guid");
                    goPage.Columns.Add("customer_code");
                    goPage.Columns.Add("customer_name");
                    goPage.Columns.Add("is_active");
                    goPage.Columns.Add("create_date");

                    int counter = 0;
                    int pageIndex = index - 1;
                    int startIndex = Cus_pageSize * pageIndex;
                    int endIndex = startIndex + Cus_pageSize - 1;

                    if (dtReload.Rows.Count > 0 || dtReload != null)
                    {
                        foreach (DataRow itemReload in dtReload.Rows)
                        {
                            if (counter >= startIndex)
                            {
                                DataRow drgoPage = goPage.NewRow();
                                drgoPage[0] = itemReload["guid"].ToString().Trim();
                                drgoPage[1] = itemReload["customer_code"].ToString().Trim();
                                drgoPage[2] = itemReload["customer_name"].ToString().Trim();
                                drgoPage[3] = itemReload["is_active"].ToString().Trim();
                                drgoPage[4] = itemReload["create_date"].ToString().Trim();
                                goPage.Rows.Add(drgoPage);
                            }

                            if (counter >= endIndex) { break; }
                            counter++;
                        }

                        this.lblCusSearchingCount.Visible = false;
                    }
                    else if (dtReload == null || dtReload.Rows.Count == 0)
                    {
                        //Not found data.
                        this.lblCusSearchingCount.Visible = true;
                        this.lblCusSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                        dgvCustomer.DataSource = goPage;
                        dgvCustomer.DataBind();

                        CusNextButton.Visible = false;
                        CusEndpage.Visible = false;
                        CusPreviousButton.Visible = false;
                        CusFirstpage.Visible = false;

                        this.lblCuspageIndex.Visible = false;
                        this.txtCuspageIndex.Visible = false;
                        this.btnCusGO.Visible = false;

                        CusCurrentPageLabel.Text = "0";
                        CusTotalPagesLabel.Text = "0";
                        return;
                    }

                    //Control page.
                    CusNextButton.Visible = true;
                    CusEndpage.Visible = true;
                    CusPreviousButton.Visible = true;
                    CusFirstpage.Visible = true;

                    dgvCustomer.DataSource = goPage;

                    Cus_totalPages = ((dtReload.Rows.Count - 1) / Cus_pageSize) + 1;

                    if (index > Cus_totalPages)
                    {
                        index = Cus_totalPages;
                        gotoPage(index);
                        return;
                    }

                    dgvCustomer.DataBind();
                    CusCurrentPageLabel.Text = index.ToString();
                    CusTotalPagesLabel.Text = Cus_totalPages.ToString();

                    if (this.CusTotalPagesLabel.Text.Trim() == "1")
                    {
                        this.lblCuspageIndex.Visible = false;
                        this.txtCuspageIndex.Visible = false;
                        this.btnCusGO.Visible = false;
                    }
                    else if (this.CusTotalPagesLabel.Text.Trim() != "1")
                    {
                        this.lblCuspageIndex.Visible = true;
                        this.txtCuspageIndex.Visible = true;
                        this.btnCusGO.Visible = true;
                    }

                    if (index == Cus_totalPages)
                    {
                        CusNextButton.Visible = false;
                        CusEndpage.Visible = false;
                    }
                    else
                    {
                        CusNextButton.Visible = true;
                        CusEndpage.Visible = true;
                    }

                    if (index == 1)
                    {
                        CusFirstpage.Visible = false;
                        CusPreviousButton.Visible = false;
                    }
                    else
                    {
                        CusFirstpage.Visible = true;
                        CusPreviousButton.Visible = true;
                    }

                    if (dtReload.Rows.Count <= 0)
                        NavigationPanelCustomer.Visible = false;
                    else
                        NavigationPanelCustomer.Visible = true;

                    #endregion
                }
            }
        }

        // Paging Loading data.
        private void DoLoading(string _tab, DataTable dtReload)
        {
            if (_tab.Trim() == "1")
            {
                #region Tab 1.1.2

                //dtReload = Executing.Instance.getGIDelivery();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("do_no");
                dtSubReload.Columns.Add("serial_number");

                if (dtReload.Rows.Count > 0)
                {
                    dgvGIRefDelivery.DataSource = dtReload;
                    dgvGIRefDelivery.DataBind();

                    //Calculate split page.
                    int counter = 0;
                    int pageIndex = currentPage - 1;
                    int startIndex = pageSize * pageIndex;
                    int endIndex = startIndex + pageSize - 1;

                    if (dtReload.Rows.Count > 0 || dtReload != null)
                    {
                        foreach (DataRow itemReload in dtReload.Rows)
                        {
                            if (counter >= startIndex)
                            {
                                DataRow drReload = dtSubReload.NewRow();
                                drReload[0] = itemReload["create_date"].ToString().Trim();
                                drReload[1] = itemReload["do_no"].ToString().Trim();
                                drReload[2] = itemReload["serial_number"].ToString().Trim();

                                dtSubReload.Rows.Add(drReload);
                            }

                            if (counter >= endIndex) { break; }
                            counter++;
                        }

                        this.lblSearchingCount.Visible = false;
                    }
                    else if (dtReload.Rows.Count == 0 || dtReload == null)
                    {
                        //Not found data.
                        this.lblSearchingCount.Visible = true;
                        this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                        dgvGIRefDelivery.DataSource = dtReload;
                        dgvGIRefDelivery.DataBind();

                        NextButton.Visible = false;
                        Endpage.Visible = false;
                        PreviousButton.Visible = false;
                        Firstpage.Visible = false;

                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;

                        CurrentPageLabel.Text = "0";
                        TotalPagesLabel.Text = "0";
                        return;
                    }

                    //Control page.
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                    PreviousButton.Visible = true;
                    Firstpage.Visible = true;

                    dgvGIRefDelivery.DataSource = dtSubReload;

                    totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                    if (currentPage > totalPages)
                    {
                        currentPage = totalPages;
                        DoLoading("1", dtReload);
                        return;
                    }

                    dgvGIRefDelivery.DataBind();
                    CurrentPageLabel.Text = currentPage.ToString();
                    TotalPagesLabel.Text = totalPages.ToString();

                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;
                    }
                    else if (this.TotalPagesLabel.Text.Trim() != "1")
                    {
                        this.lblpageIndex.Visible = true;
                        this.txtpageIndex.Visible = true;
                        this.btnGO.Visible = true;
                    }

                    if (currentPage == totalPages)
                    {
                        NextButton.Visible = false;
                        Endpage.Visible = false;
                    }
                    else
                    {
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                    }

                    if (currentPage == 1)
                    {
                        Firstpage.Visible = false;
                        PreviousButton.Visible = false;
                    }
                    else
                    {
                        Firstpage.Visible = true;
                        PreviousButton.Visible = true;
                    }

                    if (dtReload.Rows.Count <= 0)
                        NavigationPanel.Visible = false;
                    else
                        NavigationPanel.Visible = true;
                }

                #endregion
            }
            else if (_tab.Trim() == "2")
            {
                #region Tab 1.2

                //dtReload = Executing.Instance.getDistributionRet();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("empty_or_full");
                dtSubReload.Columns.Add("cust_id");
                dtSubReload.Columns.Add("doc_no");
                dtSubReload.Columns.Add("serial_number");
                dtSubReload.Columns.Add("vehicle");

                if (dtReload.Rows.Count > 0)
                {
                    dgvDistributionRet.DataSource = dtReload;
                    dgvDistributionRet.DataBind();

                    //Calculate split page.
                    int counter = 0;
                    int pageIndex = currentPage - 1;
                    int startIndex = pageSize * pageIndex;
                    int endIndex = startIndex + pageSize - 1;

                    if (dtReload.Rows.Count > 0 || dtReload != null)
                    {
                        foreach (DataRow itemReload in dtReload.Rows)
                        {
                            if (counter >= startIndex)
                            {
                                DataRow drReload = dtSubReload.NewRow();
                                drReload[0] = itemReload["create_date"].ToString().Trim();
                                drReload[1] = itemReload["empty_or_full"].ToString().Trim();
                                drReload[2] = itemReload["cust_id"].ToString().Trim();
                                drReload[3] = itemReload["doc_no"].ToString().Trim();
                                drReload[4] = itemReload["serial_number"].ToString().Trim();
                                drReload[5] = itemReload["vehicle"].ToString().Trim();

                                dtSubReload.Rows.Add(drReload);
                            }

                            if (counter >= endIndex) { break; }
                            counter++;
                        }

                        this.lblSearchingCount.Visible = false;
                    }
                    else if (dtReload.Rows.Count == 0 || dtReload == null)
                    {
                        //Not found data.
                        this.lblSearchingCount.Visible = true;
                        this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                        dgvDistributionRet.DataSource = dtReload;
                        dgvDistributionRet.DataBind();

                        NextButton.Visible = false;
                        Endpage.Visible = false;
                        PreviousButton.Visible = false;
                        Firstpage.Visible = false;

                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;

                        CurrentPageLabel.Text = "0";
                        TotalPagesLabel.Text = "0";
                        return;
                    }

                    //Control page.
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                    PreviousButton.Visible = true;
                    Firstpage.Visible = true;

                    dgvDistributionRet.DataSource = dtSubReload;

                    totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                    if (currentPage > totalPages)
                    {
                        currentPage = totalPages;
                        DoLoading("2", dtReload);
                        return;
                    }

                    dgvDistributionRet.DataBind();
                    CurrentPageLabel.Text = currentPage.ToString();
                    TotalPagesLabel.Text = totalPages.ToString();

                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;
                    }
                    else if (this.TotalPagesLabel.Text.Trim() != "1")
                    {
                        this.lblpageIndex.Visible = true;
                        this.txtpageIndex.Visible = true;
                        this.btnGO.Visible = true;
                    }

                    if (currentPage == totalPages)
                    {
                        NextButton.Visible = false;
                        Endpage.Visible = false;
                    }
                    else
                    {
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                    }

                    if (currentPage == 1)
                    {
                        Firstpage.Visible = false;
                        PreviousButton.Visible = false;
                    }
                    else
                    {
                        Firstpage.Visible = true;
                        PreviousButton.Visible = true;
                    }

                    if (dtReload.Rows.Count <= 0)
                        NavigationPanel.Visible = false;
                    else
                        NavigationPanel.Visible = true;
                }

                #endregion
            }
            else if (_tab.Trim() == "3")
            {
                #region Tab 1.4.1

                //dtReload = Executing.Instance.getGRFromProduction();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("pre_order");
                dtSubReload.Columns.Add("batch");
                dtSubReload.Columns.Add("serial_number");

                if (dtReload.Rows.Count > 0)
                {
                    dgvGRFromProduction.DataSource = dtReload;
                    dgvGRFromProduction.DataBind();

                    //Calculate split page.
                    int counter = 0;
                    int pageIndex = currentPage - 1;
                    int startIndex = pageSize * pageIndex;
                    int endIndex = startIndex + pageSize - 1;

                    if (dtReload.Rows.Count > 0 || dtReload != null)
                    {
                        foreach (DataRow itemReload in dtReload.Rows)
                        {
                            if (counter >= startIndex)
                            {
                                DataRow drReload = dtSubReload.NewRow();
                                drReload[0] = itemReload["create_date"].ToString().Trim();
                                drReload[1] = itemReload["pre_order"].ToString().Trim();
                                drReload[2] = itemReload["batch"].ToString().Trim();
                                drReload[3] = itemReload["serial_number"].ToString().Trim();

                                dtSubReload.Rows.Add(drReload);
                            }

                            if (counter >= endIndex) { break; }
                            counter++;
                        }

                        this.lblSearchingCount.Visible = false;
                    }
                    else if (dtReload.Rows.Count == 0 || dtReload == null)
                    {
                        //Not found data.
                        this.lblSearchingCount.Visible = true;
                        this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                        dgvGRFromProduction.DataSource = dtReload;
                        dgvGRFromProduction.DataBind();

                        NextButton.Visible = false;
                        Endpage.Visible = false;
                        PreviousButton.Visible = false;
                        Firstpage.Visible = false;

                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;

                        CurrentPageLabel.Text = "0";
                        TotalPagesLabel.Text = "0";
                        return;
                    }

                    //Control page.
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                    PreviousButton.Visible = true;
                    Firstpage.Visible = true;

                    dgvDistributionRet.DataSource = dtSubReload;

                    totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                    if (currentPage > totalPages)
                    {
                        currentPage = totalPages;
                        DoLoading("3", dtReload);
                        return;
                    }

                    dgvGRFromProduction.DataBind();
                    CurrentPageLabel.Text = currentPage.ToString();
                    TotalPagesLabel.Text = totalPages.ToString();

                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;
                    }
                    else if (this.TotalPagesLabel.Text.Trim() != "1")
                    {
                        this.lblpageIndex.Visible = true;
                        this.txtpageIndex.Visible = true;
                        this.btnGO.Visible = true;
                    }

                    if (currentPage == totalPages)
                    {
                        NextButton.Visible = false;
                        Endpage.Visible = false;
                    }
                    else
                    {
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                    }

                    if (currentPage == 1)
                    {
                        Firstpage.Visible = false;
                        PreviousButton.Visible = false;
                    }
                    else
                    {
                        Firstpage.Visible = true;
                        PreviousButton.Visible = true;
                    }

                    if (dtReload.Rows.Count <= 0)
                        NavigationPanel.Visible = false;
                    else
                        NavigationPanel.Visible = true;
                }

                #endregion
            }
            else if (_tab.Trim() == "4")
            {
                #region Tab 1.5.3

                //dtReload = Executing.Instance.getGIRefIOEmpty();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("do_no");
                dtSubReload.Columns.Add("serial_number");

                if (dtReload.Rows.Count > 0)
                {
                    dgvGIRefIOEmpty.DataSource = dtReload;
                    dgvGIRefIOEmpty.DataBind();

                    //Calculate split page.
                    int counter = 0;
                    int pageIndex = currentPage - 1;
                    int startIndex = pageSize * pageIndex;
                    int endIndex = startIndex + pageSize - 1;

                    if (dtReload.Rows.Count > 0 || dtReload != null)
                    {
                        foreach (DataRow itemReload in dtReload.Rows)
                        {
                            if (counter >= startIndex)
                            {
                                DataRow drReload = dtSubReload.NewRow();
                                drReload[0] = itemReload["create_date"].ToString().Trim();
                                drReload[1] = itemReload["do_no"].ToString().Trim();
                                drReload[2] = itemReload["serial_number"].ToString().Trim();

                                dtSubReload.Rows.Add(drReload);
                            }

                            if (counter >= endIndex) { break; }
                            counter++;
                        }

                        this.lblSearchingCount.Visible = false;
                    }
                    else if (dtReload.Rows.Count == 0 || dtReload == null)
                    {
                        //Not found data.
                        this.lblSearchingCount.Visible = true;
                        this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                        dgvGIRefIOEmpty.DataSource = dtReload;
                        dgvGIRefIOEmpty.DataBind();

                        NextButton.Visible = false;
                        Endpage.Visible = false;
                        PreviousButton.Visible = false;
                        Firstpage.Visible = false;

                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;

                        CurrentPageLabel.Text = "0";
                        TotalPagesLabel.Text = "0";
                        return;
                    }

                    //Control page.
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                    PreviousButton.Visible = true;
                    Firstpage.Visible = true;

                    dgvGIRefIOEmpty.DataSource = dtSubReload;

                    totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                    if (currentPage > totalPages)
                    {
                        currentPage = totalPages;
                        DoLoading("4", dtReload);
                        return;
                    }

                    dgvGIRefIOEmpty.DataBind();
                    CurrentPageLabel.Text = currentPage.ToString();
                    TotalPagesLabel.Text = totalPages.ToString();

                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;
                    }
                    else if (this.TotalPagesLabel.Text.Trim() != "1")
                    {
                        this.lblpageIndex.Visible = true;
                        this.txtpageIndex.Visible = true;
                        this.btnGO.Visible = true;
                    }

                    if (currentPage == totalPages)
                    {
                        NextButton.Visible = false;
                        Endpage.Visible = false;
                    }
                    else
                    {
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                    }

                    if (currentPage == 1)
                    {
                        Firstpage.Visible = false;
                        PreviousButton.Visible = false;
                    }
                    else
                    {
                        Firstpage.Visible = true;
                        PreviousButton.Visible = true;
                    }

                    if (dtReload.Rows.Count <= 0)
                        NavigationPanel.Visible = false;
                    else
                        NavigationPanel.Visible = true;
                }

                #endregion
            }
        }
        void DostartPaging(int startPage, string _tab)
        {
            if (_tab.Trim() == "1")
            {
                #region Tab 1.1.2

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getGIDelivery();

                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("do_no");
                dtSubReload.Columns.Add("serial_number");

                //Calculate split page.
                int counter = 0;
                int pageIndex = startPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["do_no"].ToString().Trim();
                            drReload[2] = itemReload["serial_number"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvGIRefDelivery.DataSource = dtReload;
                    dgvGIRefDelivery.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvGIRefDelivery.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (startPage > totalPages)
                {
                    startPage = totalPages;
                    DostartPaging(startPage, "1");
                    return;
                }

                dgvGIRefDelivery.DataBind();
                CurrentPageLabel.Text = startPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (startPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (startPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
            else if (_tab.Trim() == "2")
            {
                #region Tab 1.2

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getDistributionRet();

                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("empty_or_full");
                dtSubReload.Columns.Add("cust_id");
                dtSubReload.Columns.Add("doc_no");
                dtSubReload.Columns.Add("serial_number");
                dtSubReload.Columns.Add("vehicle");

                //Calculate split page.
                int counter = 0;
                int pageIndex = startPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["empty_or_full"].ToString().Trim();
                            drReload[2] = itemReload["cust_id"].ToString().Trim();
                            drReload[3] = itemReload["doc_no"].ToString().Trim();
                            drReload[4] = itemReload["serial_number"].ToString().Trim();
                            drReload[5] = itemReload["vehicle"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvDistributionRet.DataSource = dtReload;
                    dgvDistributionRet.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvDistributionRet.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (startPage > totalPages)
                {
                    startPage = totalPages;
                    DostartPaging(startPage, "2");
                    return;
                }

                dgvDistributionRet.DataBind();
                CurrentPageLabel.Text = startPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (startPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (startPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
            else if (_tab.Trim() == "3")
            {
                #region Tab 1.4.1

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getGRFromProduction();

                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("pre_order");
                dtSubReload.Columns.Add("batch");
                dtSubReload.Columns.Add("serial_number");

                //Calculate split page.
                int counter = 0;
                int pageIndex = startPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["pre_order"].ToString().Trim();
                            drReload[2] = itemReload["batch"].ToString().Trim();
                            drReload[3] = itemReload["serial_number"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvGRFromProduction.DataSource = dtReload;
                    dgvGRFromProduction.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvGRFromProduction.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (startPage > totalPages)
                {
                    startPage = totalPages;
                    DostartPaging(startPage, "3");
                    return;
                }

                dgvGRFromProduction.DataBind();
                CurrentPageLabel.Text = startPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (startPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (startPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
            else if (_tab.Trim() == "4")
            {
                #region Tab 1.5.3

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getGIRefIOEmpty();

                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("do_no");
                dtSubReload.Columns.Add("serial_number");

                //Calculate split page.
                int counter = 0;
                int pageIndex = startPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["do_no"].ToString().Trim();
                            drReload[2] = itemReload["serial_number"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvGIRefIOEmpty.DataSource = dtReload;
                    dgvGIRefIOEmpty.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvGIRefIOEmpty.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (startPage > totalPages)
                {
                    startPage = totalPages;
                    DostartPaging(startPage, "4");
                    return;
                }

                dgvGIRefIOEmpty.DataBind();
                CurrentPageLabel.Text = startPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (startPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (startPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
        }
        void DoendPaging(int lastPage, string _tab)
        {
            if (_tab.Trim() == "1")
            {
                #region Tab 1.1.2

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getGIDelivery();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("do_no");
                dtSubReload.Columns.Add("serial_number");

                //Calculate split page.
                int counter = 0;
                int pageIndex = lastPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["do_no"].ToString().Trim();
                            drReload[2] = itemReload["serial_number"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvGIRefDelivery.DataSource = dtReload;
                    dgvGIRefDelivery.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvGIRefDelivery.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (lastPage > totalPages)
                {
                    lastPage = totalPages;
                    DoendPaging(lastPage, "1");
                    return;
                }

                dgvGIRefDelivery.DataBind();
                CurrentPageLabel.Text = lastPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (lastPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (lastPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
            else if (_tab.Trim() == "2")
            {
                #region Tab 1.2

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getGIDelivery();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("empty_or_full");
                dtSubReload.Columns.Add("cust_id");
                dtSubReload.Columns.Add("doc_no");
                dtSubReload.Columns.Add("serial_number");
                dtSubReload.Columns.Add("vehicle");

                //Calculate split page.
                int counter = 0;
                int pageIndex = lastPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["empty_or_full"].ToString().Trim();
                            drReload[2] = itemReload["cust_id"].ToString().Trim();
                            drReload[3] = itemReload["doc_no"].ToString().Trim();
                            drReload[4] = itemReload["serial_number"].ToString().Trim();
                            drReload[5] = itemReload["vehicle"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvDistributionRet.DataSource = dtReload;
                    dgvDistributionRet.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvDistributionRet.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (lastPage > totalPages)
                {
                    lastPage = totalPages;
                    DoendPaging(lastPage, "2");
                    return;
                }

                dgvDistributionRet.DataBind();
                CurrentPageLabel.Text = lastPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (lastPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (lastPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
            else if (_tab.Trim() == "3")
            {
                #region Tab 1.4.1

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getGRFromProduction();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("pre_order");
                dtSubReload.Columns.Add("batch");
                dtSubReload.Columns.Add("serial_number");

                //Calculate split page.
                int counter = 0;
                int pageIndex = lastPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["pre_order"].ToString().Trim();
                            drReload[2] = itemReload["batch"].ToString().Trim();
                            drReload[3] = itemReload["serial_number"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvGRFromProduction.DataSource = dtReload;
                    dgvGRFromProduction.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvGRFromProduction.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (lastPage > totalPages)
                {
                    lastPage = totalPages;
                    DoendPaging(lastPage, "3");
                    return;
                }

                dgvGRFromProduction.DataBind();
                CurrentPageLabel.Text = lastPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (lastPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (lastPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
            else if (_tab.Trim() == "4")
            {
                #region Tab 1.5.3

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getGIRefIOEmpty();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("do_no");
                dtSubReload.Columns.Add("serial_number");

                //Calculate split page.
                int counter = 0;
                int pageIndex = lastPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["do_no"].ToString().Trim();
                            drReload[2] = itemReload["serial_number"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvGIRefIOEmpty.DataSource = dtReload;
                    dgvGIRefIOEmpty.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvGIRefIOEmpty.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (lastPage > totalPages)
                {
                    lastPage = totalPages;
                    DoendPaging(lastPage, "4");
                    return;
                }

                dgvGIRefIOEmpty.DataBind();
                CurrentPageLabel.Text = lastPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (lastPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (lastPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
        }
        void DogotoPage(int index, string _tab)
        {
            if (_tab.Trim() == "1")
            {
                #region Tab 1.1.2

                if (index <= 0)
                {
                    if (this.TotalPagesLabel.Text.Trim() == "0")
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Not found data of page", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                    else
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Input page correct format", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(TotalPagesLabel.Text))
                    {
                        if (this.TotalPagesLabel.Text.Trim() == "1")
                        {
                            this.txtpageIndex.Attributes.Add("onfocus", "selectText();");
                            PageSetFocus(this.txtpageIndex);
                            return;
                        }
                        else if (index > Convert.ToInt32(this.TotalPagesLabel.Text.Trim()))
                        {
                            this.txtpageIndex.Text = string.Empty;
                            PageSetFocus(this.txtpageIndex);
                            return;
                        }
                        else
                        {
                            #region Go to Page

                            dtReload = Executing.Instance.getGIDelivery();
                            DataTable goPage = new DataTable();

                            //Add colunms
                            goPage.Columns.Add("create_date");
                            goPage.Columns.Add("do_no");
                            goPage.Columns.Add("serial_number");

                            int counter = 0;
                            int pageIndex = index - 1;
                            int startIndex = pageSize * pageIndex;
                            int endIndex = startIndex + pageSize - 1;

                            if (dtReload.Rows.Count > 0 || dtReload != null)
                            {
                                foreach (DataRow itemReload in dtReload.Rows)
                                {
                                    if (counter >= startIndex)
                                    {
                                        DataRow drgoPage = goPage.NewRow();
                                        drgoPage[0] = itemReload["create_date"].ToString().Trim();
                                        drgoPage[1] = itemReload["do_no"].ToString().Trim();
                                        drgoPage[2] = itemReload["serial_number"].ToString().Trim();

                                        goPage.Rows.Add(drgoPage);
                                    }

                                    if (counter >= endIndex) { break; }
                                    counter++;
                                }

                                this.lblSearchingCount.Visible = false;
                            }
                            else if (dtReload == null || dtReload.Rows.Count == 0)
                            {
                                //Not found data.
                                this.lblSearchingCount.Visible = true;
                                this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                                dgvGIRefDelivery.DataSource = goPage;
                                dgvGIRefDelivery.DataBind();

                                NextButton.Visible = false;
                                Endpage.Visible = false;
                                PreviousButton.Visible = false;
                                Firstpage.Visible = false;

                                this.lblpageIndex.Visible = false;
                                this.txtpageIndex.Visible = false;
                                this.btnGO.Visible = false;

                                CurrentPageLabel.Text = "0";
                                TotalPagesLabel.Text = "0";
                                return;
                            }

                            //Control page.
                            NextButton.Visible = true;
                            Endpage.Visible = true;
                            PreviousButton.Visible = true;
                            Firstpage.Visible = true;

                            dgvGIRefDelivery.DataSource = goPage;

                            totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                            if (index > totalPages)
                            {
                                index = totalPages;
                                DogotoPage(index, "1");
                                return;
                            }

                            dgvGIRefDelivery.DataBind();
                            CurrentPageLabel.Text = index.ToString();
                            TotalPagesLabel.Text = totalPages.ToString();

                            if (this.TotalPagesLabel.Text.Trim() == "1")
                            {
                                this.lblpageIndex.Visible = false;
                                this.txtpageIndex.Visible = false;
                                this.btnGO.Visible = false;
                            }
                            else if (this.TotalPagesLabel.Text.Trim() != "1")
                            {
                                this.lblpageIndex.Visible = true;
                                this.txtpageIndex.Visible = true;
                                this.btnGO.Visible = true;
                            }

                            if (index == totalPages)
                            {
                                NextButton.Visible = false;
                                Endpage.Visible = false;
                            }
                            else
                            {
                                NextButton.Visible = true;
                                Endpage.Visible = true;
                            }

                            if (index == 1)
                            {
                                Firstpage.Visible = false;
                                PreviousButton.Visible = false;
                            }
                            else
                            {
                                Firstpage.Visible = true;
                                PreviousButton.Visible = true;
                            }

                            if (dtReload.Rows.Count <= 0)
                                NavigationPanel.Visible = false;
                            else
                                NavigationPanel.Visible = true;

                            #endregion
                        }
                    }
                }

                #endregion
            }
            else if (_tab.Trim() == "2")
            {
                #region Tab 1.2

                if (index <= 0)
                {
                    if (this.TotalPagesLabel.Text.Trim() == "0")
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Not found data of page", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                    else
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Input page correct format", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                }
                else
                {
                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.txtpageIndex.Attributes.Add("onfocus", "selectText();");
                        PageSetFocus(this.txtpageIndex);
                        return;
                    }
                    else if (index > Convert.ToInt32(this.TotalPagesLabel.Text.Trim()))
                    {
                        this.txtpageIndex.Text = string.Empty;
                        PageSetFocus(this.txtpageIndex);
                        return;
                    }
                    else
                    {
                        #region Go to Page

                        dtReload = Executing.Instance.getDistributionRet();
                        DataTable goPage = new DataTable();

                        //Add colunms
                        goPage.Columns.Add("create_date");
                        goPage.Columns.Add("empty_or_full");
                        goPage.Columns.Add("cust_id");
                        goPage.Columns.Add("doc_no");
                        goPage.Columns.Add("serial_number");
                        goPage.Columns.Add("vehicle");

                        int counter = 0;
                        int pageIndex = index - 1;
                        int startIndex = pageSize * pageIndex;
                        int endIndex = startIndex + pageSize - 1;

                        if (dtReload.Rows.Count > 0 || dtReload != null)
                        {
                            foreach (DataRow itemReload in dtReload.Rows)
                            {
                                if (counter >= startIndex)
                                {
                                    DataRow drgoPage = goPage.NewRow();
                                    drgoPage[0] = itemReload["create_date"].ToString().Trim();
                                    drgoPage[1] = itemReload["empty_or_full"].ToString().Trim();
                                    drgoPage[2] = itemReload["cust_id"].ToString().Trim();
                                    drgoPage[3] = itemReload["doc_no"].ToString().Trim();
                                    drgoPage[4] = itemReload["serial_number"].ToString().Trim();
                                    drgoPage[5] = itemReload["vehicle"].ToString().Trim();

                                    goPage.Rows.Add(drgoPage);
                                }

                                if (counter >= endIndex) { break; }
                                counter++;
                            }

                            this.lblSearchingCount.Visible = false;
                        }
                        else if (dtReload == null || dtReload.Rows.Count == 0)
                        {
                            //Not found data.
                            this.lblSearchingCount.Visible = true;
                            this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                            dgvDistributionRet.DataSource = goPage;
                            dgvDistributionRet.DataBind();

                            NextButton.Visible = false;
                            Endpage.Visible = false;
                            PreviousButton.Visible = false;
                            Firstpage.Visible = false;

                            this.lblpageIndex.Visible = false;
                            this.txtpageIndex.Visible = false;
                            this.btnGO.Visible = false;

                            CurrentPageLabel.Text = "0";
                            TotalPagesLabel.Text = "0";
                            return;
                        }

                        //Control page.
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                        PreviousButton.Visible = true;
                        Firstpage.Visible = true;

                        dgvDistributionRet.DataSource = goPage;

                        totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                        if (index > totalPages)
                        {
                            index = totalPages;
                            DogotoPage(index, "2");
                            return;
                        }

                        dgvGIRefDelivery.DataBind();
                        CurrentPageLabel.Text = index.ToString();
                        TotalPagesLabel.Text = totalPages.ToString();

                        if (this.TotalPagesLabel.Text.Trim() == "1")
                        {
                            this.lblpageIndex.Visible = false;
                            this.txtpageIndex.Visible = false;
                            this.btnGO.Visible = false;
                        }
                        else if (this.TotalPagesLabel.Text.Trim() != "1")
                        {
                            this.lblpageIndex.Visible = true;
                            this.txtpageIndex.Visible = true;
                            this.btnGO.Visible = true;
                        }

                        if (index == totalPages)
                        {
                            NextButton.Visible = false;
                            Endpage.Visible = false;
                        }
                        else
                        {
                            NextButton.Visible = true;
                            Endpage.Visible = true;
                        }

                        if (index == 1)
                        {
                            Firstpage.Visible = false;
                            PreviousButton.Visible = false;
                        }
                        else
                        {
                            Firstpage.Visible = true;
                            PreviousButton.Visible = true;
                        }

                        if (dtReload.Rows.Count <= 0)
                            NavigationPanel.Visible = false;
                        else
                            NavigationPanel.Visible = true;

                        #endregion
                    }
                }

                #endregion
            }
            else if (_tab.Trim() == "3")
            {
                #region Tab 1.4.1

                if (index <= 0)
                {
                    if (this.TotalPagesLabel.Text.Trim() == "0")
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Not found data of page", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                    else
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Input page correct format", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                }
                else
                {
                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.txtpageIndex.Attributes.Add("onfocus", "selectText();");
                        PageSetFocus(this.txtpageIndex);
                        return;
                    }
                    else if (index > Convert.ToInt32(this.TotalPagesLabel.Text.Trim()))
                    {
                        this.txtpageIndex.Text = string.Empty;
                        PageSetFocus(this.txtpageIndex);
                        return;
                    }
                    else
                    {
                        #region Go to Page

                        dtReload = Executing.Instance.getGRFromProduction();
                        DataTable goPage = new DataTable();

                        //Add colunms
                        goPage.Columns.Add("create_date");
                        goPage.Columns.Add("pre_order");
                        goPage.Columns.Add("batch");
                        goPage.Columns.Add("serial_number");

                        int counter = 0;
                        int pageIndex = index - 1;
                        int startIndex = pageSize * pageIndex;
                        int endIndex = startIndex + pageSize - 1;

                        if (dtReload.Rows.Count > 0 || dtReload != null)
                        {
                            foreach (DataRow itemReload in dtReload.Rows)
                            {
                                if (counter >= startIndex)
                                {
                                    DataRow drgoPage = goPage.NewRow();
                                    drgoPage[0] = itemReload["create_date"].ToString().Trim();
                                    drgoPage[1] = itemReload["pre_order"].ToString().Trim();
                                    drgoPage[2] = itemReload["batch"].ToString().Trim();
                                    drgoPage[3] = itemReload["serial_number"].ToString().Trim();

                                    goPage.Rows.Add(drgoPage);
                                }

                                if (counter >= endIndex) { break; }
                                counter++;
                            }

                            this.lblSearchingCount.Visible = false;
                        }
                        else if (dtReload == null || dtReload.Rows.Count == 0)
                        {
                            //Not found data.
                            this.lblSearchingCount.Visible = true;
                            this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                            dgvGRFromProduction.DataSource = goPage;
                            dgvGRFromProduction.DataBind();

                            NextButton.Visible = false;
                            Endpage.Visible = false;
                            PreviousButton.Visible = false;
                            Firstpage.Visible = false;

                            this.lblpageIndex.Visible = false;
                            this.txtpageIndex.Visible = false;
                            this.btnGO.Visible = false;

                            CurrentPageLabel.Text = "0";
                            TotalPagesLabel.Text = "0";
                            return;
                        }

                        //Control page.
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                        PreviousButton.Visible = true;
                        Firstpage.Visible = true;

                        dgvGRFromProduction.DataSource = goPage;

                        totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                        if (index > totalPages)
                        {
                            index = totalPages;
                            DogotoPage(index, "3");
                            return;
                        }

                        dgvGRFromProduction.DataBind();
                        CurrentPageLabel.Text = index.ToString();
                        TotalPagesLabel.Text = totalPages.ToString();

                        if (this.TotalPagesLabel.Text.Trim() == "1")
                        {
                            this.lblpageIndex.Visible = false;
                            this.txtpageIndex.Visible = false;
                            this.btnGO.Visible = false;
                        }
                        else if (this.TotalPagesLabel.Text.Trim() != "1")
                        {
                            this.lblpageIndex.Visible = true;
                            this.txtpageIndex.Visible = true;
                            this.btnGO.Visible = true;
                        }

                        if (index == totalPages)
                        {
                            NextButton.Visible = false;
                            Endpage.Visible = false;
                        }
                        else
                        {
                            NextButton.Visible = true;
                            Endpage.Visible = true;
                        }

                        if (index == 1)
                        {
                            Firstpage.Visible = false;
                            PreviousButton.Visible = false;
                        }
                        else
                        {
                            Firstpage.Visible = true;
                            PreviousButton.Visible = true;
                        }

                        if (dtReload.Rows.Count <= 0)
                            NavigationPanel.Visible = false;
                        else
                            NavigationPanel.Visible = true;

                        #endregion
                    }
                }

                #endregion
            }
            else if (_tab.Trim() == "4")
            {
                #region Tab 1.5.3

                if (index <= 0)
                {
                    if (this.TotalPagesLabel.Text.Trim() == "0")
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Not found data of page", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                    else
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Input page correct format", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                }
                else
                {
                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.txtpageIndex.Attributes.Add("onfocus", "selectText();");
                        PageSetFocus(this.txtpageIndex);
                        return;
                    }
                    else if (index > Convert.ToInt32(this.TotalPagesLabel.Text.Trim()))
                    {
                        this.txtpageIndex.Text = string.Empty;
                        PageSetFocus(this.txtpageIndex);
                        return;
                    }
                    else
                    {
                        #region Go to Page

                        dtReload = Executing.Instance.getGIRefIOEmpty();
                        DataTable goPage = new DataTable();

                        //Add colunms
                        goPage.Columns.Add("create_date");
                        goPage.Columns.Add("do_no");
                        goPage.Columns.Add("serial_number");

                        int counter = 0;
                        int pageIndex = index - 1;
                        int startIndex = pageSize * pageIndex;
                        int endIndex = startIndex + pageSize - 1;

                        if (dtReload.Rows.Count > 0 || dtReload != null)
                        {
                            foreach (DataRow itemReload in dtReload.Rows)
                            {
                                if (counter >= startIndex)
                                {
                                    DataRow drgoPage = goPage.NewRow();
                                    drgoPage[0] = itemReload["create_date"].ToString().Trim();
                                    drgoPage[1] = itemReload["do_no"].ToString().Trim();
                                    drgoPage[2] = itemReload["serial_number"].ToString().Trim();

                                    goPage.Rows.Add(drgoPage);
                                }

                                if (counter >= endIndex) { break; }
                                counter++;
                            }

                            this.lblSearchingCount.Visible = false;
                        }
                        else if (dtReload == null || dtReload.Rows.Count == 0)
                        {
                            //Not found data.
                            this.lblSearchingCount.Visible = true;
                            this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                            dgvGIRefIOEmpty.DataSource = goPage;
                            dgvGIRefIOEmpty.DataBind();

                            NextButton.Visible = false;
                            Endpage.Visible = false;
                            PreviousButton.Visible = false;
                            Firstpage.Visible = false;

                            this.lblpageIndex.Visible = false;
                            this.txtpageIndex.Visible = false;
                            this.btnGO.Visible = false;

                            CurrentPageLabel.Text = "0";
                            TotalPagesLabel.Text = "0";
                            return;
                        }

                        //Control page.
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                        PreviousButton.Visible = true;
                        Firstpage.Visible = true;

                        dgvGIRefIOEmpty.DataSource = goPage;

                        totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                        if (index > totalPages)
                        {
                            index = totalPages;
                            DogotoPage(index, "4");
                            return;
                        }

                        dgvGIRefIOEmpty.DataBind();
                        CurrentPageLabel.Text = index.ToString();
                        TotalPagesLabel.Text = totalPages.ToString();

                        if (this.TotalPagesLabel.Text.Trim() == "1")
                        {
                            this.lblpageIndex.Visible = false;
                            this.txtpageIndex.Visible = false;
                            this.btnGO.Visible = false;
                        }
                        else if (this.TotalPagesLabel.Text.Trim() != "1")
                        {
                            this.lblpageIndex.Visible = true;
                            this.txtpageIndex.Visible = true;
                            this.btnGO.Visible = true;
                        }

                        if (index == totalPages)
                        {
                            NextButton.Visible = false;
                            Endpage.Visible = false;
                        }
                        else
                        {
                            NextButton.Visible = true;
                            Endpage.Visible = true;
                        }

                        if (index == 1)
                        {
                            Firstpage.Visible = false;
                            PreviousButton.Visible = false;
                        }
                        else
                        {
                            Firstpage.Visible = true;
                            PreviousButton.Visible = true;
                        }

                        if (dtReload.Rows.Count <= 0)
                            NavigationPanel.Visible = false;
                        else
                            NavigationPanel.Visible = true;

                        #endregion
                    }
                }

                #endregion
            }
        }

        private void cusSearching(string _customer_code, string _customer_name)
        {
            DataTable dtSearCustomer = Customer.Instance.getCustomerByCondition(_customer_code.Trim(), _customer_name.Trim());
            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("guid");
            dtSubReload.Columns.Add("customer_code");
            dtSubReload.Columns.Add("customer_name");
            dtSubReload.Columns.Add("is_active");
            dtSubReload.Columns.Add("create_date");

            if (dtSearCustomer.Rows.Count > 0)
            {
                dgvCustomer.DataSource = dtSearCustomer;
                dgvCustomer.DataBind();

                //Calculate split page.
                int counter = 0;
                int pageIndex = Cus_currentPage - 1;
                int startIndex = Cus_pageSize * pageIndex;
                int endIndex = startIndex + Cus_pageSize - 1;

                if (dtSearCustomer.Rows.Count > 0 || dtSearCustomer != null)
                {
                    foreach (DataRow itemReload in dtSearCustomer.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["guid"].ToString().Trim();
                            drReload[1] = itemReload["customer_code"].ToString().Trim();
                            drReload[2] = itemReload["customer_name"].ToString().Trim();
                            drReload[3] = itemReload["is_active"].ToString().Trim();
                            drReload[4] = itemReload["create_date"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblCusSearchingCount.Visible = false;
                }
                else if (dtSearCustomer.Rows.Count == 0 || dtSearCustomer == null)
                {
                    //Not found data.
                    this.lblCusSearchingCount.Visible = true;
                    this.lblCusSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvCustomer.DataSource = dtSearCustomer;
                    dgvCustomer.DataBind();

                    CusNextButton.Visible = false;
                    CusEndpage.Visible = false;
                    CusPreviousButton.Visible = false;
                    CusFirstpage.Visible = false;

                    this.lblCuspageIndex.Visible = false;
                    this.txtCuspageIndex.Visible = false;
                    this.btnCusGO.Visible = false;

                    CusCurrentPageLabel.Text = "0";
                    CusTotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                CusNextButton.Visible = true;
                CusEndpage.Visible = true;
                CusPreviousButton.Visible = true;
                CusFirstpage.Visible = true;

                dgvCustomer.DataSource = dtSubReload;

                Cus_totalPages = ((dtSearCustomer.Rows.Count - 1) / Cus_pageSize) + 1;

                if (Cus_currentPage > Cus_totalPages)
                {
                    Cus_currentPage = Cus_totalPages;
                    cusSearching(_customer_code, _customer_name);
                    return;
                }

                dgvCustomer.DataBind();
                CusCurrentPageLabel.Text = Cus_currentPage.ToString();
                CusTotalPagesLabel.Text = Cus_totalPages.ToString();

                if (this.CusTotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblCuspageIndex.Visible = false;
                    this.txtCuspageIndex.Visible = false;
                    this.btnCusGO.Visible = false;
                }
                else if (this.CusTotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblCuspageIndex.Visible = true;
                    this.txtCuspageIndex.Visible = true;
                    this.btnCusGO.Visible = true;
                }

                if (Cus_currentPage == Cus_totalPages)
                {
                    CusNextButton.Visible = false;
                    CusEndpage.Visible = false;
                }
                else
                {
                    CusNextButton.Visible = true;
                    CusEndpage.Visible = true;
                }

                if (Cus_currentPage == 1)
                {
                    CusFirstpage.Visible = false;
                    CusPreviousButton.Visible = false;
                }
                else
                {
                    CusFirstpage.Visible = true;
                    CusPreviousButton.Visible = true;
                }

                if (dtSearCustomer.Rows.Count <= 0)
                    NavigationPanelCustomer.Visible = false;
                else
                    NavigationPanelCustomer.Visible = true;
            }
            else
            {
                Page.ScriptJqueryMessage("Data not found", JMessageType.Warning);
                dtSearCustomer.Columns.Add("guid");
                dtSearCustomer.Columns.Add("customer_code");
                dtSearCustomer.Columns.Add("customer_name");
                dtSearCustomer.Columns.Add("is_active");
                dtSearCustomer.Columns.Add("create_date");
                this.dgvCustomer.DataSource = dtSearCustomer;
                this.dgvCustomer.DataBind();
            }
        }

        private void DefaultPaging()
        {
            // Paging default.
            CurrentPageLabel.Text = string.Empty;
            TotalPagesLabel.Text = string.Empty;
            Firstpage.Visible = true;
            Firstpage.Text = "<< Start";
            PreviousButton.Visible = true;
            PreviousButton.Text = "< Prev";
            NextButton.Visible = true;
            NextButton.Text = "Next >";
            Endpage.Visible = true;
            Endpage.Text = "End >>";
            lblpageIndex.Visible = true;
            lblpageIndex.Text = "Page";
            txtpageIndex.Visible = true;
            txtpageIndex.Text = string.Empty;
            btnGO.Visible = true;
        }
        private void ExportDataToExcel(GridView gvDetails, string _tabName)
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            switch (_tabName)
            {
                case "1":
                    string filenameTab1 = String.Format("GIRefDelivery_{0}.xls",
                                DateTime.Now.ToString("ddMMyyyyHHmmss", usDtfi));
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", filenameTab1));
                    Response.ContentType = "application/ms-excel";
                    gvDetails.AllowPaging = false;
                    //Change the Header Row back to white color
                    gvDetails.HeaderRow.Style.Add("background-color", "#FFFFFF");
                    //Applying stlye to gridview header cells
                    for (int i = 0; i < gvDetails.HeaderRow.Cells.Count; i++)
                    {
                        gvDetails.HeaderRow.Cells[i].Style.Add("background-color", "#507CD1");
                    }
                    int j = 1;
                    //Set alternate row color
                    foreach (GridViewRow gvrow in gvDetails.Rows)
                    {
                        gvrow.BackColor = System.Drawing.Color.White;
                        if (j <= gvDetails.Rows.Count)
                        {
                            if (j % 2 != 0)
                            {
                                for (int k = 0; k < gvrow.Cells.Count; k++)
                                {
                                    gvrow.Cells[k].Style.Add("background-color", "#EFF3FB");
                                }
                            }
                        }
                        j++;
                    }
                    gvDetails.RenderControl(htw);

                    Response.Write(@"<html><body>" + sw.ToString() + "</body></html>");
                    Response.End();
                    break;
                case "2":
                    string filenameTab2 = String.Format("DistributionRet_{0}.xls",
                                DateTime.Now.ToString("ddMMyyyyHHmmss", usDtfi));
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", filenameTab2));
                    Response.ContentType = "application/ms-excel";
                    gvDetails.AllowPaging = false;
                    //Change the Header Row back to white color
                    gvDetails.HeaderRow.Style.Add("background-color", "#FFFFFF");
                    //Applying stlye to gridview header cells
                    for (int i = 0; i < gvDetails.HeaderRow.Cells.Count; i++)
                    {
                        gvDetails.HeaderRow.Cells[i].Style.Add("background-color", "#507CD1");
                    }
                    int h = 1;
                    //Set alternate row color
                    foreach (GridViewRow gvrow in gvDetails.Rows)
                    {
                        gvrow.BackColor = System.Drawing.Color.White;
                        if (h <= gvDetails.Rows.Count)
                        {
                            if (h % 2 != 0)
                            {
                                for (int k = 0; k < gvrow.Cells.Count; k++)
                                {
                                    gvrow.Cells[k].Style.Add("background-color", "#EFF3FB");
                                }
                            }
                        }
                        h++;
                    }
                    gvDetails.RenderControl(htw);

                    Response.Write(@"<html><body>" + sw.ToString() + "</body></html>");
                    Response.End();
                    break;
                case "3":
                    string filenameTab3 = String.Format("GRProduction_{0}.xls",
                                DateTime.Now.ToString("ddMMyyyyHHmmss", usDtfi));
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", filenameTab3));
                    Response.ContentType = "application/ms-excel";
                    gvDetails.AllowPaging = false;
                    //Change the Header Row back to white color
                    gvDetails.HeaderRow.Style.Add("background-color", "#FFFFFF");
                    //Applying stlye to gridview header cells
                    for (int i = 0; i < gvDetails.HeaderRow.Cells.Count; i++)
                    {
                        gvDetails.HeaderRow.Cells[i].Style.Add("background-color", "#507CD1");
                    }
                    int l = 1;
                    //Set alternate row color
                    foreach (GridViewRow gvrow in gvDetails.Rows)
                    {
                        gvrow.BackColor = System.Drawing.Color.White;
                        if (l <= gvDetails.Rows.Count)
                        {
                            if (l % 2 != 0)
                            {
                                for (int k = 0; k < gvrow.Cells.Count; k++)
                                {
                                    gvrow.Cells[k].Style.Add("background-color", "#EFF3FB");
                                }
                            }
                        }
                        l++;
                    }
                    gvDetails.RenderControl(htw);

                    Response.Write(@"<html><body>" + sw.ToString() + "</body></html>");
                    Response.End();
                    break;
                case "4":
                    string filenameTab4 = String.Format("GIRefEmpty_{0}.xls",
                                DateTime.Now.ToString("ddMMyyyyHHmmss", usDtfi));
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", filenameTab4));
                    Response.ContentType = "application/ms-excel";
                    gvDetails.AllowPaging = false;
                    //Change the Header Row back to white color
                    gvDetails.HeaderRow.Style.Add("background-color", "#FFFFFF");
                    //Applying stlye to gridview header cells
                    for (int i = 0; i < gvDetails.HeaderRow.Cells.Count; i++)
                    {
                        gvDetails.HeaderRow.Cells[i].Style.Add("background-color", "#507CD1");
                    }
                    int p = 1;
                    //Set alternate row color
                    foreach (GridViewRow gvrow in gvDetails.Rows)
                    {
                        gvrow.BackColor = System.Drawing.Color.White;
                        if (p <= gvDetails.Rows.Count)
                        {
                            if (p % 2 != 0)
                            {
                                for (int k = 0; k < gvrow.Cells.Count; k++)
                                {
                                    gvrow.Cells[k].Style.Add("background-color", "#EFF3FB");
                                }
                            }
                        }
                        p++;
                    }
                    gvDetails.RenderControl(htw);

                    Response.Write(@"<html><body>" + sw.ToString() + "</body></html>");
                    Response.End();
                    break;
            }
        }

        #endregion

        #region Event

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["secLogin"] == null)
                {
                    Response.Redirect("~/Login.aspx");
                }

                Tab1.CssClass = "Clicked";
                MainView.ActiveViewIndex = 0;

                ConnectDevice();
            }

            this.lblVersion.Text = " Version : " + Assembly.GetExecutingAssembly().GetName().Version.ToString();
            this.lblTitle.Text = string.IsNullOrEmpty(ConfigurationManager.AppSettings["TitleName"].ToString()) ? "[Thai Japan Gas] Download Data System" : ConfigurationManager.AppSettings["TitleName"].ToString().Trim();
        }
        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Initial";
            Tab4.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;

            // Tab 2
            this.dgvDistributionRet.DataSource = null;
            this.dgvDistributionRet.DataBind();
            // Tab 3
            this.dgvGRFromProduction.DataSource = null;
            this.dgvGRFromProduction.DataBind();
            // Tab 4
            this.dgvGIRefIOEmpty.DataSource = null;
            this.dgvGIRefIOEmpty.DataBind();

            DefaultPaging();
        }
        protected void Tab2_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Clicked";
            Tab3.CssClass = "Initial";
            Tab4.CssClass = "Initial";
            MainView.ActiveViewIndex = 1;

            // Tab 1
            this.dgvGIRefDelivery.DataSource = null;
            this.dgvGIRefDelivery.DataBind();
            // Tab 3
            this.dgvGRFromProduction.DataSource = null;
            this.dgvGRFromProduction.DataBind();
            // Tab 4
            this.dgvGIRefIOEmpty.DataSource = null;
            this.dgvGIRefIOEmpty.DataBind();

            // Paging default.
            DefaultPaging();
        }
        protected void Tab3_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Clicked";
            Tab4.CssClass = "Initial";
            MainView.ActiveViewIndex = 2;

            // Tab 1
            this.dgvGIRefDelivery.DataSource = null;
            this.dgvGIRefDelivery.DataBind();
            // Tab 2
            this.dgvDistributionRet.DataSource = null;
            this.dgvDistributionRet.DataBind();
            // Tab 4
            this.dgvGIRefIOEmpty.DataSource = null;
            this.dgvGIRefIOEmpty.DataBind();

            // Paging default.
            DefaultPaging();
        }
        protected void Tab4_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Initial";
            Tab4.CssClass = "Clicked";
            MainView.ActiveViewIndex = 3;

            // Tab 1
            this.dgvGIRefDelivery.DataSource = null;
            this.dgvGIRefDelivery.DataBind();
            // Tab 2
            this.dgvDistributionRet.DataSource = null;
            this.dgvDistributionRet.DataBind();
            // Tab 3
            this.dgvGRFromProduction.DataSource = null;
            this.dgvGRFromProduction.DataBind();

            // Paging default.
            DefaultPaging();
        }
        protected void TabCustomer_Click(object sender, EventArgs e)
        {
            TabCustomer.CssClass = "Clicked";
            TabImportCustomer.CssClass = "Initial";
            TabItems.CssClass = "Initial";
            MultiViewMaster.ActiveViewIndex = 0;

            // Binding data customer.
            DataTable dtTabCustomer = Customer.Instance.getCustomer();
            if (dtTabCustomer.Rows.Count > 0)
            {
                Reload();
            }
            else
            {
                this.dgvCustomer.DataSource = dtTabCustomer;
                this.dgvCustomer.DataBind();
                NavigationPanelCustomer.Visible = false;
            }

            // Import Customer Controls.
            if (!this.dtRead.Columns.Contains("cust_id"))
                this.dtRead.Columns.Add("cust_id");
            else if (!this.dtRead.Columns.Contains("cust_name"))
                this.dtRead.Columns.Add("cust_name");

            // Clear customer control
            this.txtCustomerCode.Text = string.Empty;
            this.txtCustomerName.Text = string.Empty;

            PageSetFocus(txtCustomerCode);
        }
        protected void TabImportCustomer_Click(object sender, EventArgs e)
        {
            TabCustomer.CssClass = "Initial";
            TabItems.CssClass = "Initial";
            TabImportCustomer.CssClass = "Clicked";
            MultiViewMaster.ActiveViewIndex = 1;
        }
        protected void TabItems_Click(object sender, EventArgs e)
        {
            TabCustomer.CssClass = "Initial";
            TabImportCustomer.CssClass = "Initial";
            TabItems.CssClass = "Clicked";
            MultiViewMaster.ActiveViewIndex = 2;
            this.txtItemspageIndex.Text = string.Empty;
            // Binding data customer.
            if (DataAccess.Items.Items.Instance.getItems().Rows.Count > 0)
            {
                ReloadItems();
            }
            else
            {
                NavigationPanelItems.Visible = false;
                this.dgvItems.DataSource = DataAccess.Items.Items.Instance.getItems();
                this.dgvItems.DataBind();
            }

            this.txtTempItemCode.Text = string.Empty;
            this.txtFullItemCode.Text = string.Empty;
            this.txtItemName.Text = string.Empty;

            PageSetFocus(this.txtTempItemCode);
        }

        protected void btnReceiveData_Click(object sender, EventArgs e)
        {
            //Receive Data From.
            dtReceive = null;
            //นำเข้าข้อมูล
            try
            {
                if (!string.IsNullOrEmpty(this.txthidConfirm.Value))
                {
                    if (this.txthidConfirm.Value == "Yes")
                    {
                        #region Confirm

                        // event occures
                        if (lblResultSync.ForeColor == Color.Red)
                        {
                            Page.ScriptJqueryMessage("HandHeld ไม่ถูกเชื่อมต่อ กรุณาตรวจสอบ", JMessageType.Warning);
                            return;
                        }
                        else
                        {
                            rapi = new OpenNETCF.Desktop.Communication.RAPI();
                            if (rapi.DevicePresent)
                            {
                                if (!rapi.Connected)
                                {
                                    // Fix path : Create folder Data Base SDF in C:
                                    //string _create_path = @"C:\Data Base SDF\";
                                    // Config path : Create folder Data Base SDF in C:
                                    string _create_path = string.IsNullOrEmpty(ConfigurationManager.AppSettings["CreatePath"].ToString()) ? @"C:\Data Base SDF\" : ConfigurationManager.AppSettings["CreatePath"].ToString();
                                    // Fix path : Copy files.
                                    //string path = @"C:\Data Base SDF\TJG.sdf";
                                    // Config path : Copy files.
                                    string path = string.IsNullOrEmpty(ConfigurationManager.AppSettings["DBPath"].ToString()) ? @"C:\Data Base SDF\TJG.sdf" : ConfigurationManager.AppSettings["DBPath"].ToString();
                                    string pathPDA = "\\My Documents\\TJG.sdf";
                                    //Check path in local.
                                    if (!Directory.Exists(_create_path))
                                        Directory.CreateDirectory(_create_path);
                                    //Class copy files from device to PC.
                                    CopyFiles.Files.ClsCopyFile.CopyFromDevice(path, pathPDA);
                                    rapi.Connect();
                                    rapi.Disconnect();
                                }
                                rapi.Dispose();

                                // Validation expire software.
                                if (Executing.Instance.CheckExpireSoftware().Trim() == "TRUE")
                                {
                                    this.btnActivateLicence.Visible = true;
                                }
                                else if (Executing.Instance.CheckExpireSoftware().Trim() == "LIFETIME")
                                {
                                    this.btnActivateLicence.Visible = false;
                                }
                                else if (Executing.Instance.CheckExpireSoftware().Trim() == "EXPIRE")
                                {
                                    ScriptManager.RegisterStartupScript( 
                                                    this,
                                                    GetType(),
                                                    "key",
                                                    "alert('โปรแกรมหมดอายุการใช้งาน กรุณาลงทะเบียน Licences key ที่โปรแกรมบน PDA -> เมนู Activate Licences');",
                                                    true
                                                    );

                                    Response.Redirect("ExpireLicencseKeys.aspx?activate=EXPIRE");
                                }

                                //Query data in sqlCe.
                                if (this.Tab1.CssClass.ToString() == "Clicked")
                                {
                                    // Select Tab1.
                                    // Data 1.1.2
                                    // Binding data customer.
                                    gIDelivery = Executing.Instance.getGIDelivery();
                                    if (gIDelivery.Rows.Count > 0)
                                    {
                                        DoLoading("1", gIDelivery);
                                    }
                                    else
                                    {
                                        // DataBind
                                        this.dgvGIRefDelivery.DataSource = gIDelivery;
                                        this.dgvGIRefDelivery.DataBind();
                                        NavigationPanelCustomer.Visible = false;
                                    }

                                    Page.ScriptJqueryMessage("โหลดข้อมูล สำเร็จ", JMessageType.Accept);
                                }
                                else if (this.Tab2.CssClass.ToString() == "Clicked")
                                {
                                    // Select Tab2.
                                    // Data 1.2
                                    distributionRet = Executing.Instance.getDistributionRet();
                                    if (distributionRet.Rows.Count > 0)
                                    {
                                        DoLoading("2", distributionRet);
                                    }
                                    else
                                    {
                                        // DataBind
                                        this.dgvDistributionRet.DataSource = distributionRet;
                                        this.dgvDistributionRet.DataBind();
                                        NavigationPanelCustomer.Visible = false;
                                    }

                                    Page.ScriptJqueryMessage("โหลดข้อมูล สำเร็จ", JMessageType.Accept);
                                }
                                else if (this.Tab3.CssClass.ToString() == "Clicked")
                                {
                                    // Select Tab3.
                                    // Data 1.4.1
                                    gRFromProduction = Executing.Instance.getGRFromProduction();
                                    if (gRFromProduction.Rows.Count > 0)
                                    {
                                        DoLoading("3", gRFromProduction);
                                    }
                                    else
                                    {
                                        // DataBind
                                        this.dgvGRFromProduction.DataSource = gRFromProduction;
                                        this.dgvGRFromProduction.DataBind();
                                        NavigationPanelCustomer.Visible = false;
                                    }

                                    Page.ScriptJqueryMessage("โหลดข้อมูล สำเร็จ", JMessageType.Accept);
                                }
                                else if (this.Tab4.CssClass.ToString() == "Clicked")
                                {
                                    // Select Tab4.
                                    // Data 1.5.3
                                    gIRefIOEmpty = Executing.Instance.getGIRefIOEmpty();
                                    if (gIRefIOEmpty.Rows.Count > 0)
                                    {
                                        DoLoading("4", gIRefIOEmpty);
                                    }
                                    else
                                    {
                                        // DataBind
                                        this.dgvGIRefIOEmpty.DataSource = gIRefIOEmpty;
                                        this.dgvGIRefIOEmpty.DataBind();
                                        NavigationPanelCustomer.Visible = false;
                                    }

                                    Page.ScriptJqueryMessage("โหลดข้อมูล สำเร็จ", JMessageType.Accept);
                                }
                            }
                            else
                            {
                                this.loading.Style.Add("display", "none");
                                Page.ScriptJqueryMessage("HandHeld ไม่ถูกเชื่อมต่อ กรุณาตรวจสอบ", JMessageType.Warning);
                                return;
                            }
                        }

                        loading.Attributes.Add("style", "display:none");
                        this.dtRead = new DataTable();
                        //loading.Style.Add("display", "none");

                        #endregion
                    }
                    else
                    {
                        #region Not confirm

                        // do nothing
                        loading.Attributes.Add("style", "display:none");
                        return;

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.ToString().IndexOf("used by another process") > -1)
                {
                    rapi.Dispose();
                    loading.Attributes.Add("style", "display:none");
                    string _error_msg = "มีการใช้งานฐานข้อมูล กรุณาปิดโปรแกรมบน HandHeld " + ex.Message.ToString().Trim();
                    Page.ScriptJqueryMessage(_error_msg, JMessageType.Error);
                    //Executing.Instance.Insert_Log("Web_LoadData", "used by another process", "Class_CopyFromDevice", "btnReceiveData_Click");
                    using(Execution _execute = new Execution())
                    {
                        _execute.Insert_Log("Web_LoadData", "database process used by another process", "Mamgements.aspx", "btnReceiveData_Click");
                    }
                }
                else
                {
                    rapi.Dispose();
                    loading.Attributes.Add("style", "display:none");
                    string _error_msg = "โปรแกรมบน HandHeld อาจถูกเปิดอยู่ กรุณาปิดโปรแกรมบน HandHeld หรือ ไม่มีไฟล์ฐานข้อมูล";
                    Page.ScriptJqueryMessage(_error_msg, JMessageType.Error);
                    //Executing.Instance.Insert_Log("Web_LoadData", "used by another process", "Class_CopyFromDevice", "btnReceiveData_Click");
                    using (Execution _execute = new Execution())
                    {
                        _execute.Insert_Log("Web_LoadData", "database process used by another process", "Mamgements.aspx", "btnReceiveData_Click");
                    }
                }
            }
        }
        protected void btnLoadDatabase_Click(object sender, EventArgs e)
        {
            try
            {
                // event occures
                if (lblResultSync.ForeColor == Color.Red)
                {
                    Page.ScriptJqueryMessage("HandHeld ไม่ถูกเชื่อมต่อ กรุณาตรวจสอบ", JMessageType.Warning);
                    return;
                }
                else
                {
                    rapi = new OpenNETCF.Desktop.Communication.RAPI();
                    if (rapi.DevicePresent)
                    {
                        if (!rapi.Connected)
                        {
                            // Fix path : Create folder Data Base SDF in C:
                            //string _create_path = @"C:\Data Base SDF\";
                            // Config path : Create folder Data Base SDF in C:
                            string _create_path = string.IsNullOrEmpty(ConfigurationManager.AppSettings["CreatePath"].ToString()) ? @"C:\Data Base SDF\" : ConfigurationManager.AppSettings["CreatePath"].ToString();
                            // Fix path : Copy files.
                            //string path = @"C:\Data Base SDF\TJG.sdf";
                            // Config path : Copy files.
                            string path = string.IsNullOrEmpty(ConfigurationManager.AppSettings["DBPath"].ToString()) ? @"C:\Data Base SDF\TJG.sdf" : ConfigurationManager.AppSettings["DBPath"].ToString();
                            string pathPDA = "\\My Documents\\TJG.sdf";
                            //Check path in local.
                            if (!Directory.Exists(_create_path))
                                Directory.CreateDirectory(_create_path);
                            //Class copy files from device to PC.
                            CopyFiles.Files.ClsCopyFile.CopyFromDevice(path, pathPDA);
                            rapi.Connect();
                            rapi.Disconnect();
                        }
                        rapi.Dispose();

                        Page.ScriptJqueryMessage("โหลดข้อมูลจาก PDA สำเร็จ", JMessageType.Accept);
                    }
                    else
                    {
                        Page.ScriptJqueryMessage("HandHeld ไม่ถูกเชื่อมต่อ กรุณาตรวจสอบ", JMessageType.Warning);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.ToString().IndexOf("used by another process") > -1)
                {
                    rapi.Dispose();
                    loading.Attributes.Add("style", "display:none");
                    string _error_msg = "มีการใช้งานฐานข้อมูล กรุณาปิดโปรแกรมบน HandHeld " + ex.Message.ToString().Trim();
                    Page.ScriptJqueryMessage(_error_msg, JMessageType.Error);
                    //Executing.Instance.Insert_Log("Web_LoadData", "used by another process", "Class_CopyFromDevice", "btnLoadDatabase_Click");
                    using(Execution _execute = new Execution())
                    {
                        _execute.Insert_Log("Web_LoadData", "database process used by another process", "Mamgements.aspx", "btnLoadDatabase_Click");
                    }
                }
                else
                {
                    rapi.Dispose();
                    loading.Attributes.Add("style", "display:none");
                    string _error_msg = "โปรแกรมบน HandHeld อาจถูกเปิดอยู่ กรุณาปิดโปรแกรมบน HandHeld หรือ ไม่มีไฟล์ฐานข้อมูล";
                    Page.ScriptJqueryMessage(_error_msg, JMessageType.Error);
                    //Executing.Instance.Insert_Log("Web_LoadData", "used by another process", "Class_CopyFromDevice", "btnLoadDatabase_Click");
                    using(Execution _execute = new Execution())
                    {
                        _execute.Insert_Log("Web_LoadData", "database process used by another process", "Mamgements.aspx", "btnLoadDatabase_Click");
                    }
                }
            }
        }
        protected void btnAddMaster_Click(object sender, EventArgs e)
        {
            light.Style.Add("display", "block");
            fade.Style.Add("display", "block");

            TabCustomer.CssClass = "Clicked";
            TabImportCustomer.CssClass = "Initial";
            TabItems.CssClass = "Initial";
            MultiViewMaster.ActiveViewIndex = 0;

            // Control Customer.
            this.txtCustomerCode.Text = string.Empty;
            this.txtCustomerCode.Enabled = true;
            this.txtCustomerName.Text = string.Empty;
            this.chkIsActive.Checked = false;
            this.txtCuspageIndex.Text = string.Empty;
            // Binding data customer.
            DataTable dtAddCustomer = Customer.Instance.getCustomer();
            if (dtAddCustomer.Rows.Count > 0)
            {
                Reload();
            }
            else
            {
                this.dgvCustomer.DataSource = dtAddCustomer;
                this.dgvCustomer.DataBind();
                NavigationPanelCustomer.Visible = false;
            }

            // Import Customer Controls.
            if (!this.dtRead.Columns.Contains("cust_id"))
                this.dtRead.Columns.Add("cust_id");
            else if (!this.dtRead.Columns.Contains("cust_name"))
                this.dtRead.Columns.Add("cust_name");

            // Control Items.
            this.txtTempItemCode.Text = string.Empty;
            this.txtFullItemCode.Text = string.Empty;
            this.txtItemName.Text = string.Empty;

            PageSetFocus(txtCustomerCode);
        }
        protected void btnSaveCustomer_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtCustomerCode.Text.Trim()))
            {
                Page.ScriptJqueryMessage("Please enter customer code", JMessageType.Warning);
                PageSetFocus(txtCustomerCode);
                return;
            }
            else if (string.IsNullOrEmpty(this.txtCustomerName.Text.Trim()))
            {
                Page.ScriptJqueryMessage("Please enter customer name", JMessageType.Warning);
                PageSetFocus(txtCustomerName);
                return;
            }
            else
            {
                if (this.txtAction.Enabled)
                {
                    #region INSERT

                    if (Customer.Instance.SaveCustomer(Guid.NewGuid().ToString().ToUpper().Trim(), this.txtCustomerCode.Text.Trim(),
                            this.txtCustomerName.Text.Trim(), this.chkIsActive.Checked == true ? "YES" : "NO") == "0")
                    {
                        this.txtAction.Text = string.Empty;
                        this.txtCustomerCode.Enabled = true;
                        this.txtAction.Enabled = true;

                        // Clear control customer.
                        this.txtCustomerCode.Text = string.Empty;
                        this.txtCustomerName.Text = string.Empty;
                        this.chkIsActive.Checked = false;
                        Page.ScriptJqueryMessage("Save successfully", JMessageType.Accept);
                        Reload();
                    }
                    else
                    {
                        Page.ScriptJqueryMessage(Customer.Instance.SaveCustomer(Guid.NewGuid().ToString().ToUpper().Trim(), this.txtCustomerCode.Text.Trim(),
                            this.txtCustomerName.Text.Trim(), this.chkIsActive.Checked == true ? "YES" : "NO"), JMessageType.Warning);
                        return;
                    }

                    #endregion
                }
                else if (!this.txtAction.Enabled)
                {
                    #region EDIT

                    if (Customer.Instance.EditCustomer(this.txtAction.Text.Trim(), this.txtCustomerCode.Text.Trim(),
                            this.txtCustomerName.Text.Trim(), this.chkIsActive.Checked == true ? "YES" : "NO") == "0")
                    {
                        this.txtAction.Text = string.Empty;
                        this.txtCustomerCode.Enabled = true;
                        this.txtAction.Enabled = true;

                        // Clear control customer.
                        this.txtCustomerCode.Text = string.Empty;
                        this.txtCustomerName.Text = string.Empty;
                        this.chkIsActive.Checked = false;
                        Page.ScriptJqueryMessage("Edit successfully", JMessageType.Accept);
                        Reload();
                    }
                    else
                    {
                        Page.ScriptJqueryMessage(Customer.Instance.EditCustomer(this.txtAction.Text.Trim(), this.txtCustomerCode.Text.Trim(),
                            this.txtCustomerName.Text.Trim(), this.chkIsActive.Checked == true ? "YES" : "NO"), JMessageType.Warning);
                        return;
                    }

                    #endregion
                }
            }
        }
        protected void btnClearCustomer_Click(object sender, EventArgs e)
        {
            this.txtCustomerCode.Text = string.Empty;
            this.txtCustomerCode.Enabled = true;
            this.txtCustomerName.Text = string.Empty;
            this.chkIsActive.Checked = false;

            this.txtAction.Text = string.Empty;
            this.txtAction.Enabled = true;

            PageSetFocus(this.txtCustomerCode);
        }
        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (upFile.HasFile)
            {
                #region Have File

                try
                {
                    #region Load file

                    string fileName = this.upFile.PostedFile.FileName;
                    //string TempfileLocation = Server.MapPath(this.upFile.PostedFile.FileName); // @"C:\UploadFiles\";
                    string getFullPath = Server.MapPath(this.upFile.PostedFile.FileName); // @"C:\UploadFiles\";
                    //string getFullPath = System.IO.Path.Combine(TempfileLocation, fileName);

                    this.upFile.SaveAs(getFullPath);
                    if (!string.IsNullOrEmpty(getFullPath))
                    {
                        if (Customer.Instance.getImportCustomer())
                        {
                            #region Have Customer

                            // Check column in datatable.
                            if (!this.dtRead.Columns.Contains("cust_id"))
                                this.dtRead.Columns.Add("cust_id");
                            else if (!this.dtRead.Columns.Contains("cust_name"))
                                this.dtRead.Columns.Add("cust_name");

                            using (StreamReader streamReader = new StreamReader(getFullPath, Encoding.GetEncoding("windows-874")))
                            {
                                string text = streamReader.ReadToEnd().Replace("\n", string.Empty);
                                string[] array = text.Split(new char[]
					                                {
						                                Convert.ToChar("\r")
					                                });

                                for (int i = 0; i < Enumerable.Count<string>(array); i++)
                                {
                                    if (i >= 0)
                                    {
                                        if (!string.IsNullOrEmpty(array[i]))
                                        {
                                            string[] array2 = array[i].Split(new char[]
								{
									Convert.ToChar('\t')
								});

                                            if (array2.Length == 3)
                                            {
                                                Page.ScriptJqueryMessage("Select file not type for customer", JMessageType.Warning);
                                                return;
                                            }

                                            this.dtRead.Rows.Add(new object[0]);
                                            int num = this.dtRead.Rows.Count - 1;

                                            // Insert data to datatable.
                                            this.dtRead.Rows[num]["cust_id"] = array2[0];
                                            this.dtRead.Rows[num]["cust_name"] = array2[1];
                                        }
                                    }
                                }
                                streamReader.Dispose();
                            }

                            if (this.dtRead.Rows.Count > 0)
                            {
                                if (!Customer.Instance.DeleteCustomerAll())
                                {
                                    Page.ScriptJqueryMessage("Can't import customer", JMessageType.Warning);
                                    return;
                                }
                                else
                                {
                                    List<entCustomer> _list = new List<entCustomer>();
                                    for (int i = 0; i < this.dtRead.Rows.Count; i++)
                                    {
                                        _list.Add(new entCustomer
                                        {
                                            // Insert data to list.
                                            customer_code = this.dtRead.Rows[i]["cust_id"].ToString().Trim(),
                                            customer_name = this.dtRead.Rows[i]["cust_name"].ToString().Trim(),
                                            is_active = "YES"
                                        });
                                    }

                                    // Save
                                    if (Customer.Instance.SaveImportCustomer(_list) == "0")
                                    {
                                        this.loading.Style.Add("display", "none");
                                        this.dtRead = new DataTable();
                                        this.dtRead.Columns.Add("cust_id");
                                        this.dtRead.Columns.Add("cust_name");

                                        // Display message
                                        Page.ScriptJqueryMessage("Import Successfully", JMessageType.Accept);
                                    }
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            #region Not have customer

                            // Check column in datatable.
                            if (!this.dtRead.Columns.Contains("cust_id"))
                                this.dtRead.Columns.Add("cust_id");
                            else if (!this.dtRead.Columns.Contains("cust_name"))
                                this.dtRead.Columns.Add("cust_name");

                            using (StreamReader streamReader = new StreamReader(getFullPath, Encoding.GetEncoding("windows-874")))
                            {
                                string text = streamReader.ReadToEnd().Replace("\n", string.Empty);
                                string[] array = text.Split(new char[]
					                                {
						                                Convert.ToChar("\r")
					                                });

                                for (int i = 0; i < Enumerable.Count<string>(array); i++)
                                {
                                    if (i >= 0)
                                    {
                                        if (!string.IsNullOrEmpty(array[i]))
                                        {
                                            string[] array2 = array[i].Split(new char[]
								{
									Convert.ToChar('\t')
								});

                                            if (array2.Length == 3)
                                            {
                                                Page.ScriptJqueryMessage("Select file not type for customer", JMessageType.Warning);
                                                return;
                                            }

                                            this.dtRead.Rows.Add(new object[0]);
                                            int num = this.dtRead.Rows.Count - 1;

                                            // Insert data to datatable.
                                            this.dtRead.Rows[num]["cust_id"] = array2[0];
                                            this.dtRead.Rows[num]["cust_name"] = array2[1];
                                        }
                                    }
                                }
                                streamReader.Dispose();
                            }

                            if (this.dtRead.Rows.Count > 0)
                            {
                                List<entCustomer> _list = new List<entCustomer>();
                                for (int i = 0; i < this.dtRead.Rows.Count; i++)
                                {
                                    _list.Add(new entCustomer
                                    {
                                        // Insert data to list.
                                        customer_code = this.dtRead.Rows[i]["cust_id"].ToString().Trim(),
                                        customer_name = this.dtRead.Rows[i]["cust_name"].ToString().Trim(),
                                        is_active = "YES"
                                    });
                                }

                                // Save
                                if (Customer.Instance.SaveImportCustomer(_list) == "0")
                                {
                                    this.loading.Style.Add("display", "none");
                                    this.dtRead = new DataTable();
                                    this.dtRead.Columns.Add("cust_id");
                                    this.dtRead.Columns.Add("cust_name");

                                    // Display message
                                    Page.ScriptJqueryMessage("Import Successfully", JMessageType.Accept);
                                }
                            }

                            #endregion
                        }
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    // Display messager error.
                    Page.ScriptJqueryMessage(ex.Message.ToString(), JMessageType.Error);
                }

                #endregion
            }
            else
            {
                #region Not file

                this.loading.Style.Add("display", "none");
                this.dtRead = new DataTable();
                this.dtRead.Columns.Add("cust_id");
                this.dtRead.Columns.Add("cust_name");

                #endregion
            }
        }

        protected void Firstpage_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Tab1.CssClass.ToString() == "Clicked")
                {
                    DostartPaging(1, "1");
                }
                else if (this.Tab2.CssClass.ToString() == "Clicked")
                {
                    DostartPaging(1, "2");
                }
                else if (this.Tab3.CssClass.ToString() == "Clicked")
                {
                    DostartPaging(1, "3");
                }
                else if (this.Tab4.CssClass.ToString() == "Clicked")
                {
                    DostartPaging(1, "4");
                }
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("First page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void PreviousButton_Click(object sender, EventArgs e)
        {
            if (this.Tab1.CssClass.ToString() == "Clicked")
            {
                if (!string.IsNullOrEmpty(CurrentPageLabel.Text))
                {
                    currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                    currentPage--;
                    DoLoading("1", gIDelivery);
                }
            }
            else if (this.Tab2.CssClass.ToString() == "Clicked")
            {
                if (!string.IsNullOrEmpty(CurrentPageLabel.Text))
                {
                    currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                    currentPage--;
                    DoLoading("2", distributionRet);
                }
            }
            else if (this.Tab3.CssClass.ToString() == "Clicked")
            {
                if (!string.IsNullOrEmpty(CurrentPageLabel.Text))
                {
                    currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                    currentPage--;
                    DoLoading("3", gRFromProduction);
                }
            }
            else if (this.Tab4.CssClass.ToString() == "Clicked")
            {
                if (!string.IsNullOrEmpty(CurrentPageLabel.Text))
                {
                    currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                    currentPage--;
                    DoLoading("4", gIRefIOEmpty);
                }
            }
        }
        protected void NextButton_Click(object sender, EventArgs e)
        {
            if (this.Tab1.CssClass.ToString() == "Clicked")
            {
                if (!string.IsNullOrEmpty(CurrentPageLabel.Text))
                {
                    currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                    currentPage++;
                    DoLoading("1", gIDelivery);
                }
            }
            else if (this.Tab2.CssClass.ToString() == "Clicked")
            {
                if (!string.IsNullOrEmpty(CurrentPageLabel.Text))
                {
                    currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                    currentPage++;
                    DoLoading("2", distributionRet);
                }
            }
            else if (this.Tab3.CssClass.ToString() == "Clicked")
            {
                if (!string.IsNullOrEmpty(CurrentPageLabel.Text))
                {
                    currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                    currentPage++;
                    DoLoading("3", gRFromProduction);
                }
            }
            else if (this.Tab4.CssClass.ToString() == "Clicked")
            {
                if (!string.IsNullOrEmpty(CurrentPageLabel.Text))
                {
                    currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                    currentPage++;
                    DoLoading("4", gIRefIOEmpty);
                }
            }
        }
        protected void Endpage_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Tab1.CssClass.ToString() == "Clicked")
                {
                    if (!string.IsNullOrEmpty(this.TotalPagesLabel.Text.Trim()))
                    {
                        int lastPage = Convert.ToInt32(this.TotalPagesLabel.Text.Trim());
                        if (lastPage > 0)
                        {
                            DoendPaging(lastPage, "1");
                        }
                        else if (lastPage <= 0)
                        {
                            return;
                        }
                    }
                }
                else if (this.Tab2.CssClass.ToString() == "Clicked")
                {
                    if (!string.IsNullOrEmpty(this.TotalPagesLabel.Text.Trim()))
                    {
                        int lastPage = Convert.ToInt32(this.TotalPagesLabel.Text.Trim());
                        if (lastPage > 0)
                        {
                            DoendPaging(lastPage, "2");
                        }
                        else if (lastPage <= 0)
                        {
                            return;
                        }
                    }
                }
                else if (this.Tab3.CssClass.ToString() == "Clicked")
                {
                    if (!string.IsNullOrEmpty(this.TotalPagesLabel.Text.Trim()))
                    {
                        int lastPage = Convert.ToInt32(this.TotalPagesLabel.Text.Trim());
                        if (lastPage > 0)
                        {
                            DoendPaging(lastPage, "3");
                        }
                        else if (lastPage <= 0)
                        {
                            return;
                        }
                    }
                }
                else if (this.Tab4.CssClass.ToString() == "Clicked")
                {
                    if (!string.IsNullOrEmpty(this.TotalPagesLabel.Text.Trim()))
                    {
                        int lastPage = Convert.ToInt32(this.TotalPagesLabel.Text.Trim());
                        if (lastPage > 0)
                        {
                            DoendPaging(lastPage, "4");
                        }
                        else if (lastPage <= 0)
                        {
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("End page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void btnGO_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Tab1.CssClass.ToString() == "Clicked")
                {
                    if (!string.IsNullOrEmpty(this.txtpageIndex.Text.Trim()))
                        DogotoPage(Convert.ToInt32(this.txtpageIndex.Text.Trim()), "1");
                    else
                        PageSetFocus(this.txtpageIndex);
                }
                else if (this.Tab2.CssClass.ToString() == "Clicked")
                {
                    if (!string.IsNullOrEmpty(this.txtpageIndex.Text.Trim()))
                        DogotoPage(Convert.ToInt32(this.txtpageIndex.Text.Trim()), "2");
                    else
                        PageSetFocus(this.txtpageIndex);
                }
                else if (this.Tab3.CssClass.ToString() == "Clicked")
                {
                    if (!string.IsNullOrEmpty(this.txtpageIndex.Text.Trim()))
                        DogotoPage(Convert.ToInt32(this.txtpageIndex.Text.Trim()), "3");
                    else
                        PageSetFocus(this.txtpageIndex);
                }
                else if (this.Tab4.CssClass.ToString() == "Clicked")
                {
                    if (!string.IsNullOrEmpty(this.txtpageIndex.Text.Trim()))
                        DogotoPage(Convert.ToInt32(this.txtpageIndex.Text.Trim()), "4");
                    else
                        PageSetFocus(this.txtpageIndex);
                }
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("Go to page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void CusFirstpage_Click(object sender, EventArgs e)
        {
            try
            {
                startPaging(1);
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("First page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void CusPreviousButton_Click(object sender, EventArgs e)
        {
            Cus_currentPage = Convert.ToInt32(CusCurrentPageLabel.Text);
            Cus_currentPage--;
            Reload();
        }
        protected void CusNextButton_Click(object sender, EventArgs e)
        {
            Cus_currentPage = Convert.ToInt32(CusCurrentPageLabel.Text);
            Cus_currentPage++;
            Reload();
        }
        protected void CusEndpage_Click(object sender, EventArgs e)
        {
            try
            {
                int lastPage = Convert.ToInt32(this.CusTotalPagesLabel.Text.Trim());
                if (lastPage > 0)
                {
                    endPaging(lastPage);
                }
                else if (lastPage <= 0)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("End page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void btnCusGO_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(this.txtCuspageIndex.Text.Trim()))
                    gotoPage(Convert.ToInt32(this.txtCuspageIndex.Text.Trim()));
                else
                    PageSetFocus(this.txtCuspageIndex);
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("Go to page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void ItemsFirstpage_Click(object sender, EventArgs e)
        {
            try
            {
                startPagingItems(1);
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("Item first page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void ItemsPreviousButton_Click(object sender, EventArgs e)
        {
            Items_currentPage = Convert.ToInt32(ItemsCurrentPageLabel.Text);
            Items_currentPage--;
            ReloadItems();
        }
        protected void ItemsNextButton_Click(object sender, EventArgs e)
        {
            Items_currentPage = Convert.ToInt32(ItemsCurrentPageLabel.Text);
            Items_currentPage++;
            ReloadItems();
        }
        protected void ItemsEndpage_Click(object sender, EventArgs e)
        {
            try
            {
                int lastPage = Convert.ToInt32(this.ItemsTotalPagesLabel.Text.Trim());
                if (lastPage > 0)
                    endPagingItems(lastPage);
                else if (lastPage <= 0)
                    return;
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("Items End page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void btnItemsGO_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(this.txtItemspageIndex.Text.Trim()))
                    gotoPageItems(Convert.ToInt32(this.txtItemspageIndex.Text.Trim()));
                else
                    PageSetFocus(this.txtItemspageIndex);
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("Items Go to page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }

        protected void btnClearPathFile_Click(object sender, EventArgs e)
        {
            this.upFile.Dispose();
        }
        protected void btnSaveItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtTempItemCode.Text.Trim()))
            {
                Page.ScriptJqueryMessage("Please input temp item code", JMessageType.Warning);
                PageSetFocus(this.txtTempItemCode);
                return;
            }
            else if (string.IsNullOrEmpty(this.txtFullItemCode.Text.Trim()))
            {
                Page.ScriptJqueryMessage("Please input full item code", JMessageType.Warning);
                PageSetFocus(this.txtFullItemCode);
                return;
            }
            else if (string.IsNullOrEmpty(this.txtItemName.Text.Trim()))
            {
                Page.ScriptJqueryMessage("Please input item name", JMessageType.Warning);
                PageSetFocus(this.txtItemName);
                return;
            }
            else
            {
                if (this.txtItemAction.Enabled)
                {
                    #region INSERT

                    if (DataAccess.Items.Items.Instance.SaveItems(Guid.NewGuid().ToString().ToUpper().Trim(), this.txtTempItemCode.Text.Trim(),
                        this.txtFullItemCode.Text.Trim(), this.txtItemName.Text.Trim(), this.chkItemIsActive.Checked == true ? "YES" : "NO") == "0")
                    {
                        this.txtTempItemCode.Text = string.Empty;
                        this.txtFullItemCode.Text = string.Empty;
                        this.txtItemName.Text = string.Empty;
                        this.chkItemIsActive.Checked = false;
                        Page.ScriptJqueryMessage("Save successfully", JMessageType.Accept);
                        ReloadItems();
                    }
                    else
                    {
                        Page.ScriptJqueryMessage(DataAccess.Items.Items.Instance.SaveItems(Guid.NewGuid().ToString().ToUpper().Trim(), this.txtTempItemCode.Text.Trim(),
                        this.txtFullItemCode.Text.Trim(), this.txtItemName.Text.Trim(), this.chkItemIsActive.Checked == true ? "YES" : "NO"), JMessageType.Warning);
                        return;
                    }

                    #endregion
                }
                else if (!this.txtItemAction.Enabled)
                {
                    #region EDIT

                    if (DataAccess.Items.Items.Instance.EditItems(this.txtItemAction.Text.Trim(), this.txtTempItemCode.Text.Trim(),
                        this.txtFullItemCode.Text.Trim(), this.txtItemName.Text.Trim(), this.chkItemIsActive.Checked == true ? "YES" : "NO") == "0")
                    {
                        this.txtTempItemCode.Text = string.Empty;
                        this.txtFullItemCode.Text = string.Empty;
                        this.txtItemName.Text = string.Empty;
                        this.chkItemIsActive.Checked = false;
                        Page.ScriptJqueryMessage("Edit successfully", JMessageType.Accept);
                        ReloadItems();
                    }
                    else
                    {
                        Page.ScriptJqueryMessage(DataAccess.Items.Items.Instance.EditItems(this.txtItemAction.Text.Trim(), this.txtTempItemCode.Text.Trim(),
                        this.txtFullItemCode.Text.Trim(), this.txtItemName.Text.Trim(), this.chkItemIsActive.Checked == true ? "YES" : "NO"), JMessageType.Warning);
                        return;
                    }

                    #endregion
                }
            }
        }
        protected void btnClearItem_Click(object sender, EventArgs e)
        {
            this.txtTempItemCode.Text = string.Empty;
            this.txtFullItemCode.Text = string.Empty;
            this.txtItemName.Text = string.Empty;
            this.chkItemIsActive.Checked = false;

            this.txtItemAction.Text = string.Empty;
            this.txtItemAction.Enabled = true;

            PageSetFocus(this.txtTempItemCode);
        }
        protected void imgcloseted_Click(object sender, ImageClickEventArgs e)
        {
            light.Style.Add("display", "none");
            fade.Style.Add("display", "none");
        }

        // Button Export
        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Tab1.CssClass.ToString() == "Clicked")
                {
                    // Select Tab1.
                    #region Comment Convert Gridview to datatable.

                    /* Convert Gridview to datatable.
                //Create a new DataTable.
                if (dgvGIRefDelivery.HeaderRow != null)
                {
                    DataTable dtTab1 = new DataTable("Tab1");

                    //Add columns to DataTable.
                    foreach (TableCell cell in dgvGIRefDelivery.HeaderRow.Cells)
                    {
                        dtTab1.Columns.Add(cell.Text);
                    }

                    //Loop through the GridView and copy rows.
                    foreach (GridViewRow row in dgvGIRefDelivery.Rows)
                    {
                        dtTab1.Rows.Add();
                        for (int i = 0; i < row.Cells.Count; i++)
                        {
                            dtTab1.Rows[row.RowIndex][i] = row.Cells[i].Text;
                        }
                    }
                    

                    //ExportExcel(dtTab1, ExportFormat.Excel, "Tab1");
                    
                }
                */

                    #endregion

                    if (dgvGIRefDelivery.HeaderRow != null)
                    {
                        ExportDataToExcel(dgvGIRefDelivery, "1");
                    }
                }
                else if (this.Tab2.CssClass.ToString() == "Clicked")
                {
                    // Select Tab2.
                    if (dgvDistributionRet.HeaderRow != null)
                    {
                        ExportDataToExcel(dgvDistributionRet, "2");
                    }
                }
                else if (this.Tab3.CssClass.ToString() == "Clicked")
                {
                    // Select Tab3.
                    if (dgvGRFromProduction.HeaderRow != null)
                    {
                        ExportDataToExcel(dgvGRFromProduction, "3");
                    }
                }
                else if (this.Tab4.CssClass.ToString() == "Clicked")
                {
                    // Select Tab4.
                    if (dgvGIRefIOEmpty.HeaderRow != null)
                    {
                        ExportDataToExcel(dgvGIRefIOEmpty, "4");
                    }
                }
            }
            catch(Exception ex)
            {
                using(Execution _execute = new Execution())
                {
                    _execute.Insert_Log("ExportExcel", ex.Message.ToString(), "Mamgements.aspx", "btnExport_Click");
                }
            }
        }
        protected void btnActivateLicence_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Executing.Instance.getDateExpireSoftware()))
                {
                    if (Executing.Instance.getDateExpireSoftware() != "FALSE" &&
                        Executing.Instance.getDateExpireSoftware() != "LIFETIME" &&
                        Executing.Instance.getDateExpireSoftware() != "EXPIRE")
                    {
                        Response.Redirect("~/ActivateLicense.aspx?activate=ACTIVATES&expireDate=" + Executing.Instance.getDateExpireSoftware());
                    }
                    else if (Executing.Instance.getDateExpireSoftware() == "EXPIRE")
                    {
                        Response.Redirect("ExpireLicencseKeys.aspx?activate=EXPIRE");
                    }
                    else
                    {
                        Response.Redirect("~/ActivateLicense.aspx?activate=ACTIVATES");
                    }
                }
                else
                {
                    Response.Redirect("~/ActivateLicense.aspx?activate=ACTIVATES");
                }
            }
            catch (Exception)
            {
                if (lblResultSync.ForeColor == Color.Red)
                {
                    Page.ScriptJqueryMessage("HandHeld ไม่ถูกเชื่อมต่อ กรุณาตรวจสอบ", JMessageType.Warning);
                    return;
                }
                else
                {
                    Page.ScriptJqueryMessage("กรุณากดปุ่ม Receive Data เพื่อทำการตรวจ Licencse Key", JMessageType.Warning);
                    return;
                }
            }
        }

        protected void DeleteImageButton_Click(object sender, EventArgs e)
        {
            string value_DELETE = (sender as ImageButton).CommandArgument;
            if (!string.IsNullOrEmpty(value_DELETE))
            {
                if (Customer.Instance.DeleteCustomer(value_DELETE) == "0")
                {
                    // Clear control customer.
                    this.txtAction.Text = string.Empty;
                    this.txtCustomerCode.Enabled = true;
                    this.txtAction.Enabled = true;
                    this.txtCustomerCode.Text = string.Empty;
                    this.txtCustomerName.Text = string.Empty;
                    this.chkIsActive.Checked = false;

                    // Binding customer.
                    Reload();
                }
            }
            else
            {
                Page.ScriptJqueryMessage("Not found guid for delete customer", JMessageType.Warning);
                return;
            }
        }
        protected void EditImageButton_Click(object sender, EventArgs e)
        {
            string value_EDIT = (sender as ImageButton).CommandArgument;

            if (!string.IsNullOrEmpty(value_EDIT))
            {
                // EDIT
                DataTable dtCustomer = Customer.Instance.getCustomerDetail(value_EDIT);
                if (dtCustomer.Rows.Count > 0)
                {
                    this.txtCustomerCode.Text = dtCustomer.Rows[0]["customer_code"].ToString().Trim();
                    this.txtCustomerCode.Enabled = false;
                    this.txtAction.Text = dtCustomer.Rows[0]["guid"].ToString().Trim();
                    this.txtAction.Enabled = false;
                    this.txtCustomerName.Text = dtCustomer.Rows[0]["customer_name"].ToString().Trim();
                    this.chkIsActive.Checked = dtCustomer.Rows[0]["is_active"].ToString().Trim() == "NO" ? false : true;
                }
                else
                {
                    Page.ScriptJqueryMessage("Not found data", JMessageType.Warning);
                    return;
                }
            }
            else
            {
                Page.ScriptJqueryMessage("Not found guid for edit customer", JMessageType.Warning);
                return;
            }
        }
        protected void DeleteItemsImageButton_Click(object sender, EventArgs e)
        {
            string value_DELETE = (sender as ImageButton).CommandArgument;
            if (!string.IsNullOrEmpty(value_DELETE))
            {
                if (DataAccess.Items.Items.Instance.DeleteItems(value_DELETE) == "0")
                {
                    this.txtItemAction.Text = string.Empty;
                    this.txtItemAction.Enabled = true;
                    this.txtTempItemCode.Text = string.Empty;
                    this.txtFullItemCode.Text = string.Empty;
                    this.txtItemName.Text = string.Empty;
                    this.chkItemIsActive.Checked = false;

                    // Load items.
                    if (DataAccess.Items.Items.Instance.getItems().Rows.Count >= Items_pageSize)
                    {
                        ReloadItems();
                    }
                    else
                    {
                        if (DataAccess.Items.Items.Instance.getItems().Rows.Count == 0)
                        {
                            this.NavigationPanelItems.Visible = false;
                        }
                        this.dgvItems.DataSource = DataAccess.Items.Items.Instance.getItems();
                        this.dgvItems.DataBind();
                    }
                }
            }
            else
            {
                Page.ScriptJqueryMessage("Not found guid for delete items", JMessageType.Warning);
                return;
            }
        }
        protected void EditItemsImageButton_Click(object sender, EventArgs e)
        {
            string value_EDIT = (sender as ImageButton).CommandArgument;

            if (!string.IsNullOrEmpty(value_EDIT))
            {
                // EDIT
                DataTable dtItems = DataAccess.Items.Items.Instance.getItemsDetail(value_EDIT);
                if (dtItems.Rows.Count > 0)
                {
                    this.txtItemAction.Text = dtItems.Rows[0]["guid"].ToString().Trim();
                    this.txtItemAction.Enabled = false;
                    this.txtTempItemCode.Text = dtItems.Rows[0]["temp_item_code"].ToString().Trim();
                    this.txtFullItemCode.Text = dtItems.Rows[0]["full_item_code"].ToString().Trim();
                    this.txtItemName.Text = dtItems.Rows[0]["item_name"].ToString().Trim();
                    this.chkItemIsActive.Checked = dtItems.Rows[0]["is_active"].ToString().Trim() == "NO" ? false : true;
                }
                else
                {
                    Page.ScriptJqueryMessage("Not found data", JMessageType.Warning);
                    return;
                }
            }
            else
            {
                Page.ScriptJqueryMessage("Not found guid for edit items", JMessageType.Warning);
                return;
            }
        }

        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtCustomerCode.Text.Trim()) & string.IsNullOrEmpty(this.txtCustomerName.Text.Trim()))
                Reload();
            else
                cusSearching(this.txtCustomerCode.Text.Trim(), this.txtCustomerName.Text.Trim());
        }

        protected void UpdateTimer_Tick(object sender, EventArgs e)
        {
            ConnectDevice();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        #endregion
    }
}