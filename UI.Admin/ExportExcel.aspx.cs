﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UI.Admin.Execute;

namespace UI.Admin
{
    public partial class ExportExcel : System.Web.UI.Page
    {
        private DataTable gIDelivery;
        private void ExportData(DataTable dtExport, string _tabName)
        {
            switch (_tabName)
            {
                case "1":
                    var grid = new System.Web.UI.WebControls.GridView();
                    grid.DataSource = dtExport;
                    grid.DataBind();

                    /*
                    foreach (GridViewRow oItem in grid.Rows)
                    {
                        oItem.Cells[0].Attributes.Add("class", "td");
                        oItem.Cells[4].Attributes.Add("class", "td");

                        if (!string.IsNullOrEmpty(oItem.Cells[12].Text))
                        {
                            if (Convert.ToDecimal(oItem.Cells[12].Text) < 0)
                            {
                                oItem.Cells[12].Text = "(" + oItem.Cells[12].Text + ")";
                                oItem.Cells[12].Attributes.Add("class", "fontred");
                            }
                        }
                    }
                    */

                    Response.ClearContent();
                    Response.AddHeader("content-disposition", "kattachment; filename=Tab1.xls");
                    Response.ContentType = "application/excel";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);

                    grid.RenderControl(htw);

                    Response.Write(@"<html><body>" + sw.ToString() + "</body></html>");

                    Response.End();
                    break;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (dgvGIRefDelivery.HeaderRow != null)
            {
                DataTable dtTab1 = new DataTable("Tab1");

                //Add columns to DataTable.
                foreach (TableCell cell in dgvGIRefDelivery.HeaderRow.Cells)
                {
                    dtTab1.Columns.Add(cell.Text);
                }

                //Loop through the GridView and copy rows.
                foreach (GridViewRow row in dgvGIRefDelivery.Rows)
                {
                    dtTab1.Rows.Add();
                    for (int i = 0; i < row.Cells.Count; i++)
                    {
                        dtTab1.Rows[row.RowIndex][i] = row.Cells[i].Text;
                    }
                }

                //ExportExcel(dtTab1, ExportFormat.Excel, "Tab1");
                ExportData(dtTab1, "1");
            }
        }

        protected void ddd_Click(object sender, EventArgs e)
        {
            gIDelivery = Executing.Instance.getGIDelivery();
            // DataBind
            this.dgvGIRefDelivery.DataSource = gIDelivery;
            this.dgvGIRefDelivery.DataBind();
        }
    }
}