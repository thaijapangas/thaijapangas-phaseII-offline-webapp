﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilitys.Events;

namespace UI.Admin
{
    public partial class ViewLogging : System.Web.UI.Page
    {
        #region Member

        DataTable dtReload;
        DataTable dtReloadItem;
        DataTable dtLogging = new DataTable();

        // Page Customer
        int Cus_pageSize = 15;
        //int totalUsers = 0;
        int Cus_totalPages = 0;
        int Cus_currentPage = 1;

        // Page Items
        int Items_pageSize = 15;
        int Items_totalPages = 0;
        int Items_currentPage = 1;

        DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;

        #endregion

        #region Focus TextBox

        void PageSetFocus(Control ctrl)
        {
            ScriptManager.GetCurrent(this.Page).SetFocus(ctrl);
        }

        #endregion

        #region Method

        private void Reload(DataTable dtReload)
        {
            /*
            using (Execute.Execution _execute = new Execute.Execution())
            {
                dtReload = _execute.getLogging();
            }
            */

            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("id");
            dtSubReload.Columns.Add("error_code");
            dtSubReload.Columns.Add("error_message");
            dtSubReload.Columns.Add("page_form");
            dtSubReload.Columns.Add("method_name");
            dtSubReload.Columns.Add("create_date");

            if (!dtReload.IsNullOrNoRows())
            {
                dgvCustomer.DataSource = dtReload;
                dgvCustomer.DataBind();

                //Calculate split page.
                int counter = 0;
                int pageIndex = Cus_currentPage - 1;
                int startIndex = Cus_pageSize * pageIndex;
                int endIndex = startIndex + Cus_pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["id"].ToString().Trim();
                            drReload[1] = itemReload["error_code"].ToString().Trim();
                            drReload[2] = itemReload["error_message"].ToString().Trim();
                            drReload[3] = itemReload["page_form"].ToString().Trim();
                            drReload[4] = itemReload["method_name"].ToString().Trim();
                            drReload[5] = itemReload["create_date"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblCusSearchingCount.Visible = false;
                }
                else if (dtReload.Rows.Count == 0 || dtReload == null)
                {
                    //Not found data.
                    this.lblCusSearchingCount.Visible = true;
                    this.lblCusSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvCustomer.DataSource = dtReload;
                    dgvCustomer.DataBind();

                    CusNextButton.Visible = false;
                    CusEndpage.Visible = false;
                    CusPreviousButton.Visible = false;
                    CusFirstpage.Visible = false;

                    this.lblCuspageIndex.Visible = false;
                    this.txtCuspageIndex.Visible = false;
                    this.btnCusGO.Visible = false;

                    CusCurrentPageLabel.Text = "0";
                    CusTotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                CusNextButton.Visible = true;
                CusEndpage.Visible = true;
                CusPreviousButton.Visible = true;
                CusFirstpage.Visible = true;

                dgvCustomer.DataSource = dtSubReload;

                Cus_totalPages = ((dtReload.Rows.Count - 1) / Cus_pageSize) + 1;

                if (Cus_currentPage > Cus_totalPages)
                {
                    Cus_currentPage = Cus_totalPages;
                    Reload(dtReload);
                    return;
                }

                dgvCustomer.DataBind();
                CusCurrentPageLabel.Text = Cus_currentPage.ToString();
                CusTotalPagesLabel.Text = Cus_totalPages.ToString();

                if (this.CusTotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblCuspageIndex.Visible = false;
                    this.txtCuspageIndex.Visible = false;
                    this.btnCusGO.Visible = false;
                }
                else if (this.CusTotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblCuspageIndex.Visible = true;
                    this.txtCuspageIndex.Visible = true;
                    this.btnCusGO.Visible = true;
                }

                if (Cus_currentPage == Cus_totalPages)
                {
                    CusNextButton.Visible = false;
                    CusEndpage.Visible = false;
                }
                else
                {
                    CusNextButton.Visible = true;
                    CusEndpage.Visible = true;
                }

                if (Cus_currentPage == 1)
                {
                    CusFirstpage.Visible = false;
                    CusPreviousButton.Visible = false;
                }
                else
                {
                    CusFirstpage.Visible = true;
                    CusPreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanelCustomer.Visible = false;
                else
                    NavigationPanelCustomer.Visible = true;
            }
        }
        void startPaging(int startPage)
        {
            this.txtCuspageIndex.Text = string.Empty;
            using(Execute.Execution _execute = new Execute.Execution())
            {
                dtReload = _execute.getLoggingBycriteria(null, null, string.Empty);
            }
            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("id");
            dtSubReload.Columns.Add("error_code");
            dtSubReload.Columns.Add("error_message");
            dtSubReload.Columns.Add("page_form");
            dtSubReload.Columns.Add("method_name");
            dtSubReload.Columns.Add("create_date");

            //Calculate split page.
            int counter = 0;
            int pageIndex = startPage - 1;
            int startIndex = Cus_pageSize * pageIndex;
            int endIndex = startIndex + Cus_pageSize - 1;

            if (!dtReload.IsNullOrNoRows())
            {
                foreach (DataRow itemReload in dtReload.Rows)
                {
                    if (counter >= startIndex)
                    {
                        DataRow drReload = dtSubReload.NewRow();
                        drReload[0] = itemReload["id"].ToString().Trim();
                        drReload[1] = itemReload["error_code"].ToString().Trim();
                        drReload[2] = itemReload["error_message"].ToString().Trim();
                        drReload[3] = itemReload["page_form"].ToString().Trim();
                        drReload[4] = itemReload["method_name"].ToString().Trim();
                        drReload[5] = itemReload["create_date"].ToString().Trim();

                        dtSubReload.Rows.Add(drReload);
                    }

                    if (counter >= endIndex) { break; }
                    counter++;
                }

                this.lblCusSearchingCount.Visible = false;
            }
            else if (dtReload == null || dtReload.Rows.Count == 0)
            {
                //Not found data.
                this.lblCusSearchingCount.Visible = true;
                this.lblCusSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                dgvCustomer.DataSource = dtReload;
                dgvCustomer.DataBind();

                CusNextButton.Visible = false;
                CusEndpage.Visible = false;
                CusPreviousButton.Visible = false;
                CusFirstpage.Visible = false;

                this.lblCuspageIndex.Visible = false;
                this.txtCuspageIndex.Visible = false;
                this.btnCusGO.Visible = false;

                CusCurrentPageLabel.Text = "0";
                CusTotalPagesLabel.Text = "0";
                return;
            }

            //Control page.
            CusNextButton.Visible = true;
            CusEndpage.Visible = true;
            CusPreviousButton.Visible = true;
            CusFirstpage.Visible = true;

            dgvCustomer.DataSource = dtSubReload;

            Cus_totalPages = ((dtReload.Rows.Count - 1) / Cus_pageSize) + 1;

            if (startPage > Cus_totalPages)
            {
                startPage = Cus_totalPages;
                startPaging(startPage);
                return;
            }

            dgvCustomer.DataBind();
            CusCurrentPageLabel.Text = startPage.ToString();
            CusTotalPagesLabel.Text = Cus_totalPages.ToString();

            if (this.CusTotalPagesLabel.Text.Trim() == "1")
            {
                this.lblCuspageIndex.Visible = false;
                this.txtCuspageIndex.Visible = false;
                this.btnCusGO.Visible = false;
            }
            else if (this.CusTotalPagesLabel.Text.Trim() != "1")
            {
                this.lblCuspageIndex.Visible = true;
                this.txtCuspageIndex.Visible = true;
                this.btnCusGO.Visible = true;
            }

            if (startPage == Cus_totalPages)
            {
                CusNextButton.Visible = false;
                CusEndpage.Visible = false;
            }
            else
            {
                CusNextButton.Visible = true;
                CusEndpage.Visible = true;
            }

            if (startPage == 1)
            {
                CusFirstpage.Visible = false;
                CusPreviousButton.Visible = false;
            }
            else
            {
                CusFirstpage.Visible = true;
                CusPreviousButton.Visible = true;
            }

            if (dtReload.Rows.Count <= 0)
                NavigationPanelCustomer.Visible = false;
            else
                NavigationPanelCustomer.Visible = true;
        }
        void endPaging(int lastPage)
        {
            this.txtCuspageIndex.Text = string.Empty;
            using (Execute.Execution _execute = new Execute.Execution())
            {
                dtReload = _execute.getLoggingBycriteria(null, null, string.Empty);
            }
            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("id");
            dtSubReload.Columns.Add("error_code");
            dtSubReload.Columns.Add("error_message");
            dtSubReload.Columns.Add("page_form");
            dtSubReload.Columns.Add("method_name");
            dtSubReload.Columns.Add("create_date");

            //Calculate split page.
            int counter = 0;
            int pageIndex = lastPage - 1;
            int startIndex = Cus_pageSize * pageIndex;
            int endIndex = startIndex + Cus_pageSize - 1;

            if (!dtReload.IsNullOrNoRows())
            {
                foreach (DataRow itemReload in dtReload.Rows)
                {
                    if (counter >= startIndex)
                    {
                        DataRow drReload = dtSubReload.NewRow();
                        drReload[0] = itemReload["id"].ToString().Trim();
                        drReload[1] = itemReload["error_code"].ToString().Trim();
                        drReload[2] = itemReload["error_message"].ToString().Trim();
                        drReload[3] = itemReload["page_form"].ToString().Trim();
                        drReload[4] = itemReload["method_name"].ToString().Trim();
                        drReload[5] = itemReload["create_date"].ToString().Trim();

                        dtSubReload.Rows.Add(drReload);
                    }

                    if (counter >= endIndex) { break; }
                    counter++;
                }

                this.lblCusSearchingCount.Visible = false;
            }
            else if (dtReload == null || dtReload.Rows.Count == 0)
            {
                //Not found data.
                this.lblCusSearchingCount.Visible = true;
                this.lblCusSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                dgvCustomer.DataSource = dtReload;
                dgvCustomer.DataBind();

                CusNextButton.Visible = false;
                CusEndpage.Visible = false;
                CusPreviousButton.Visible = false;
                CusFirstpage.Visible = false;

                this.lblCuspageIndex.Visible = false;
                this.txtCuspageIndex.Visible = false;
                this.btnCusGO.Visible = false;

                CusCurrentPageLabel.Text = "0";
                CusTotalPagesLabel.Text = "0";
                return;
            }

            //Control page.
            CusNextButton.Visible = true;
            CusEndpage.Visible = true;
            CusPreviousButton.Visible = true;
            CusFirstpage.Visible = true;

            dgvCustomer.DataSource = dtSubReload;

            Cus_totalPages = ((dtReload.Rows.Count - 1) / Cus_pageSize) + 1;

            if (lastPage > Cus_totalPages)
            {
                lastPage = Cus_totalPages;
                endPaging(lastPage);
                return;
            }

            dgvCustomer.DataBind();
            CusCurrentPageLabel.Text = lastPage.ToString();
            CusTotalPagesLabel.Text = Cus_totalPages.ToString();

            if (this.CusTotalPagesLabel.Text.Trim() == "1")
            {
                this.lblCuspageIndex.Visible = false;
                this.txtCuspageIndex.Visible = false;
                this.btnCusGO.Visible = false;
            }
            else if (this.CusTotalPagesLabel.Text.Trim() != "1")
            {
                this.lblCuspageIndex.Visible = true;
                this.txtCuspageIndex.Visible = true;
                this.btnCusGO.Visible = true;
            }

            if (lastPage == Cus_totalPages)
            {
                CusNextButton.Visible = false;
                CusEndpage.Visible = false;
            }
            else
            {
                CusNextButton.Visible = true;
                CusEndpage.Visible = true;
            }

            if (lastPage == 1)
            {
                CusFirstpage.Visible = false;
                CusPreviousButton.Visible = false;
            }
            else
            {
                CusFirstpage.Visible = true;
                CusPreviousButton.Visible = true;
            }

            if (dtReload.Rows.Count <= 0)
                NavigationPanelCustomer.Visible = false;
            else
                NavigationPanelCustomer.Visible = true;
        }
        void gotoPage(int index)
        {
            if (index <= 0)
            {
                if (this.CusTotalPagesLabel.Text.Trim() == "0")
                {
                    //Alert.
                    Page.ScriptJqueryMessage("Not found data of page", JMessageType.Warning);
                    this.txtCuspageIndex.Text = string.Empty;
                    return;
                }
                else
                {
                    //Alert.
                    Page.ScriptJqueryMessage("Input page correct format", JMessageType.Warning);
                    this.txtCuspageIndex.Text = string.Empty;
                    return;
                }
            }
            else
            {
                if (this.CusTotalPagesLabel.Text.Trim() == "1")
                {
                    this.txtCuspageIndex.Attributes.Add("onfocus", "selectText();");
                    return;
                }
                else if (index > Convert.ToInt32(this.CusTotalPagesLabel.Text.Trim()))
                {
                    this.txtCuspageIndex.Text = string.Empty;
                    return;
                }
                else
                {
                    #region Go to Page

                    using (Execute.Execution _execute = new Execute.Execution())
                    {
                        dtReload = _execute.getLoggingBycriteria(null, null, string.Empty);
                    }

                    DataTable goPage = new DataTable();

                    //Add colunms
                    goPage.Columns.Add("id");
                    goPage.Columns.Add("error_code");
                    goPage.Columns.Add("error_message");
                    goPage.Columns.Add("page_form");
                    goPage.Columns.Add("method_name");
                    goPage.Columns.Add("create_date");

                    int counter = 0;
                    int pageIndex = index - 1;
                    int startIndex = Cus_pageSize * pageIndex;
                    int endIndex = startIndex + Cus_pageSize - 1;

                    if (!dtReload.IsNullOrNoRows())
                    {
                        foreach (DataRow itemReload in dtReload.Rows)
                        {
                            if (counter >= startIndex)
                            {
                                DataRow drgoPage = goPage.NewRow();
                                drgoPage[0] = itemReload["id"].ToString().Trim();
                                drgoPage[1] = itemReload["customer_code"].ToString().Trim();
                                drgoPage[2] = itemReload["customer_name"].ToString().Trim();
                                drgoPage[3] = itemReload["is_active"].ToString().Trim();
                                drgoPage[4] = itemReload["create_date"].ToString().Trim();
                                goPage.Rows.Add(drgoPage);
                            }

                            if (counter >= endIndex) { break; }
                            counter++;
                        }

                        this.lblCusSearchingCount.Visible = false;
                    }
                    else if (dtReload == null || dtReload.Rows.Count == 0)
                    {
                        //Not found data.
                        this.lblCusSearchingCount.Visible = true;
                        this.lblCusSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                        dgvCustomer.DataSource = goPage;
                        dgvCustomer.DataBind();

                        CusNextButton.Visible = false;
                        CusEndpage.Visible = false;
                        CusPreviousButton.Visible = false;
                        CusFirstpage.Visible = false;

                        this.lblCuspageIndex.Visible = false;
                        this.txtCuspageIndex.Visible = false;
                        this.btnCusGO.Visible = false;

                        CusCurrentPageLabel.Text = "0";
                        CusTotalPagesLabel.Text = "0";
                        return;
                    }

                    //Control page.
                    CusNextButton.Visible = true;
                    CusEndpage.Visible = true;
                    CusPreviousButton.Visible = true;
                    CusFirstpage.Visible = true;

                    dgvCustomer.DataSource = goPage;

                    Cus_totalPages = ((dtReload.Rows.Count - 1) / Cus_pageSize) + 1;

                    if (index > Cus_totalPages)
                    {
                        index = Cus_totalPages;
                        gotoPage(index);
                        return;
                    }

                    dgvCustomer.DataBind();
                    CusCurrentPageLabel.Text = index.ToString();
                    CusTotalPagesLabel.Text = Cus_totalPages.ToString();

                    if (this.CusTotalPagesLabel.Text.Trim() == "1")
                    {
                        this.lblCuspageIndex.Visible = false;
                        this.txtCuspageIndex.Visible = false;
                        this.btnCusGO.Visible = false;
                    }
                    else if (this.CusTotalPagesLabel.Text.Trim() != "1")
                    {
                        this.lblCuspageIndex.Visible = true;
                        this.txtCuspageIndex.Visible = true;
                        this.btnCusGO.Visible = true;
                    }

                    if (index == Cus_totalPages)
                    {
                        CusNextButton.Visible = false;
                        CusEndpage.Visible = false;
                    }
                    else
                    {
                        CusNextButton.Visible = true;
                        CusEndpage.Visible = true;
                    }

                    if (index == 1)
                    {
                        CusFirstpage.Visible = false;
                        CusPreviousButton.Visible = false;
                    }
                    else
                    {
                        CusFirstpage.Visible = true;
                        CusPreviousButton.Visible = true;
                    }

                    if (dtReload.Rows.Count <= 0)
                        NavigationPanelCustomer.Visible = false;
                    else
                        NavigationPanelCustomer.Visible = true;

                    #endregion
                }
            }
        }

        private void ReloadItems(DataTable dtReloadItem)
        {
            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("id");
            dtSubReload.Columns.Add("error_code");
            dtSubReload.Columns.Add("error_message");
            dtSubReload.Columns.Add("page_form");
            dtSubReload.Columns.Add("method_name");
            dtSubReload.Columns.Add("create_date");

            if (!dtReloadItem.IsNullOrNoRows())
            {
                dgvItems.DataSource = dtReloadItem;
                dgvItems.DataBind();

                //Calculate split page.
                int counter = 0;
                int pageIndex = Items_currentPage - 1;
                int startIndex = Items_pageSize * pageIndex;
                int endIndex = startIndex + Items_pageSize - 1;

                if (dtReloadItem.Rows.Count > 0 || dtReloadItem != null)
                {
                    foreach (DataRow itemReload in dtReloadItem.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["id"].ToString().Trim();
                            drReload[1] = itemReload["error_code"].ToString().Trim();
                            drReload[2] = itemReload["error_message"].ToString().Trim();
                            drReload[3] = itemReload["page_form"].ToString().Trim();
                            drReload[4] = itemReload["method_name"].ToString().Trim();
                            drReload[5] = itemReload["create_date"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblItemsSearchingCount.Visible = false;
                }
                else if (dtReloadItem.Rows.Count == 0 || dtReloadItem == null)
                {
                    //Not found data.
                    this.lblItemsSearchingCount.Visible = true;
                    this.lblItemsSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvItems.DataSource = dtReloadItem;
                    dgvItems.DataBind();

                    ItemsNextButton.Visible = false;
                    ItemsEndpage.Visible = false;
                    ItemsPreviousButton.Visible = false;
                    ItemsFirstpage.Visible = false;

                    this.lblItemspageIndex.Visible = false;
                    this.txtItemspageIndex.Visible = false;
                    this.btnItemsGO.Visible = false;

                    ItemsCurrentPageLabel.Text = "0";
                    ItemsTotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                ItemsNextButton.Visible = true;
                ItemsEndpage.Visible = true;
                ItemsPreviousButton.Visible = true;
                ItemsFirstpage.Visible = true;

                dgvItems.DataSource = dtSubReload;

                Items_totalPages = ((dtReloadItem.Rows.Count - 1) / Items_pageSize) + 1;

                if (Items_currentPage > Items_totalPages)
                {
                    Items_currentPage = Items_totalPages;
                    ReloadItems(dtReloadItem);
                    return;
                }

                dgvItems.DataBind();
                ItemsCurrentPageLabel.Text = Items_currentPage.ToString();
                ItemsTotalPagesLabel.Text = Items_totalPages.ToString();

                if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblItemspageIndex.Visible = false;
                    this.txtItemspageIndex.Visible = false;
                    this.btnItemsGO.Visible = false;
                }
                else if (this.ItemsTotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblItemspageIndex.Visible = true;
                    this.txtItemspageIndex.Visible = true;
                    this.btnItemsGO.Visible = true;
                }

                if (Items_currentPage == Items_totalPages)
                {
                    ItemsNextButton.Visible = false;
                    ItemsEndpage.Visible = false;
                }
                else
                {
                    ItemsNextButton.Visible = true;
                    ItemsEndpage.Visible = true;
                }

                if (Items_currentPage == 1)
                {
                    ItemsFirstpage.Visible = false;
                    ItemsPreviousButton.Visible = false;
                }
                else
                {
                    ItemsFirstpage.Visible = true;
                    ItemsPreviousButton.Visible = true;
                }

                if (dtReloadItem.Rows.Count <= 0)
                    NavigationPanelItems.Visible = false;
                else
                    NavigationPanelItems.Visible = true;
            }
        }
        void startPagingItems(int startPage)
        {
            this.txtItemspageIndex.Text = string.Empty;
            using(Execute.Execution _execute = new Execute.Execution())
            {
                dtReloadItem = _execute.getPDALoggingBycriteria(null, null, string.Empty);
            }

            //dtReloadItem = DataAccess.Items.Items.Instance.getItems();

            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("id");
            dtSubReload.Columns.Add("error_code");
            dtSubReload.Columns.Add("error_message");
            dtSubReload.Columns.Add("page_form");
            dtSubReload.Columns.Add("method_name");
            dtSubReload.Columns.Add("create_date");

            //Calculate split page.
            int counter = 0;
            int pageIndex = startPage - 1;
            int startIndex = Items_pageSize * pageIndex;
            int endIndex = startIndex + Items_pageSize - 1;

            if (!dtReloadItem.IsNullOrNoRows())
            {
                foreach (DataRow itemReload in dtReloadItem.Rows)
                {
                    if (counter >= startIndex)
                    {
                        DataRow drReload = dtSubReload.NewRow();
                        drReload[0] = itemReload["id"].ToString().Trim();
                        drReload[1] = itemReload["error_code"].ToString().Trim();
                        drReload[2] = itemReload["error_message"].ToString().Trim();
                        drReload[3] = itemReload["page_form"].ToString().Trim();
                        drReload[4] = itemReload["method_name"].ToString().Trim();
                        drReload[5] = itemReload["create_date"].ToString().Trim();

                        dtSubReload.Rows.Add(drReload);
                    }

                    if (counter >= endIndex) { break; }
                    counter++;
                }

                this.lblItemsSearchingCount.Visible = false;
            }
            else if (dtReload == null || dtReload.Rows.Count == 0)
            {
                //Not found data.
                this.lblItemsSearchingCount.Visible = true;
                this.lblItemsSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                dgvItems.DataSource = dtReloadItem;
                dgvItems.DataBind();

                ItemsNextButton.Visible = false;
                ItemsEndpage.Visible = false;
                ItemsPreviousButton.Visible = false;
                ItemsFirstpage.Visible = false;

                this.lblItemspageIndex.Visible = false;
                this.txtItemspageIndex.Visible = false;
                this.btnItemsGO.Visible = false;

                ItemsCurrentPageLabel.Text = "0";
                ItemsTotalPagesLabel.Text = "0";
                return;
            }

            //Control page.
            ItemsNextButton.Visible = true;
            ItemsEndpage.Visible = true;
            ItemsPreviousButton.Visible = true;
            ItemsFirstpage.Visible = true;

            dgvItems.DataSource = dtSubReload;

            Items_totalPages = ((dtReloadItem.Rows.Count - 1) / Items_pageSize) + 1;

            if (startPage > Items_totalPages)
            {
                startPage = Items_totalPages;
                startPagingItems(startPage);
                return;
            }

            dgvItems.DataBind();
            ItemsCurrentPageLabel.Text = startPage.ToString();
            ItemsTotalPagesLabel.Text = Items_totalPages.ToString();

            if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
            {
                this.lblItemspageIndex.Visible = false;
                this.txtItemspageIndex.Visible = false;
                this.btnItemsGO.Visible = false;
            }
            else if (this.ItemsTotalPagesLabel.Text.Trim() != "1")
            {
                this.lblItemspageIndex.Visible = true;
                this.txtItemspageIndex.Visible = true;
                this.btnItemsGO.Visible = true;
            }

            if (startPage == Items_totalPages)
            {
                ItemsNextButton.Visible = false;
                ItemsEndpage.Visible = false;
            }
            else
            {
                ItemsNextButton.Visible = true;
                ItemsEndpage.Visible = true;
            }

            if (startPage == 1)
            {
                ItemsFirstpage.Visible = false;
                ItemsPreviousButton.Visible = false;
            }
            else
            {
                ItemsFirstpage.Visible = true;
                ItemsPreviousButton.Visible = true;
            }

            if (dtReloadItem.Rows.Count <= 0)
                NavigationPanelItems.Visible = false;
            else
                NavigationPanelItems.Visible = true;
        }
        void endPagingItems(int lastPage)
        {
            this.txtItemspageIndex.Text = string.Empty;
            using(Execute.Execution _execute = new Execute.Execution())
            {
                dtReloadItem = _execute.getPDALoggingBycriteria(null, null, string.Empty);
            }

            //dtReloadItem = DataAccess.Items.Items.Instance.getItems();
            DataTable dtSubReload = new DataTable();

            //Add colunms
            dtSubReload.Columns.Add("id");
            dtSubReload.Columns.Add("error_code");
            dtSubReload.Columns.Add("error_message");
            dtSubReload.Columns.Add("page_form");
            dtSubReload.Columns.Add("method_name");
            dtSubReload.Columns.Add("create_date");

            //Calculate split page.
            int counter = 0;
            int pageIndex = lastPage - 1;
            int startIndex = Items_pageSize * pageIndex;
            int endIndex = startIndex + Items_pageSize - 1;

            if (!dtReloadItem.IsNullOrNoRows())
            {
                foreach (DataRow itemReload in dtReloadItem.Rows)
                {
                    if (counter >= startIndex)
                    {
                        DataRow drReload = dtSubReload.NewRow();
                        drReload[0] = itemReload["id"].ToString().Trim();
                        drReload[1] = itemReload["error_code"].ToString().Trim();
                        drReload[2] = itemReload["error_message"].ToString().Trim();
                        drReload[3] = itemReload["page_form"].ToString().Trim();
                        drReload[4] = itemReload["method_name"].ToString().Trim();
                        drReload[5] = itemReload["create_date"].ToString().Trim();

                        dtSubReload.Rows.Add(drReload);
                    }

                    if (counter >= endIndex) { break; }
                    counter++;
                }

                this.lblItemsSearchingCount.Visible = false;
            }
            else if (dtReloadItem.IsNullOrNoRows())
            {
                //Not found data.
                this.lblItemsSearchingCount.Visible = true;
                this.lblItemsSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                dgvItems.DataSource = dtReloadItem;
                dgvItems.DataBind();

                ItemsNextButton.Visible = false;
                ItemsEndpage.Visible = false;
                ItemsPreviousButton.Visible = false;
                ItemsFirstpage.Visible = false;

                this.lblItemspageIndex.Visible = false;
                this.txtItemspageIndex.Visible = false;
                this.btnItemsGO.Visible = false;

                ItemsCurrentPageLabel.Text = "0";
                ItemsTotalPagesLabel.Text = "0";
                return;
            }

            //Control page.
            ItemsNextButton.Visible = true;
            ItemsEndpage.Visible = true;
            ItemsPreviousButton.Visible = true;
            ItemsFirstpage.Visible = true;

            dgvItems.DataSource = dtSubReload;

            Items_totalPages = ((dtReloadItem.Rows.Count - 1) / Items_pageSize) + 1;

            if (lastPage > Items_totalPages)
            {
                lastPage = Items_totalPages;
                endPagingItems(lastPage);
                return;
            }

            dgvItems.DataBind();
            ItemsCurrentPageLabel.Text = lastPage.ToString();
            ItemsTotalPagesLabel.Text = Items_totalPages.ToString();

            if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
            {
                this.lblItemspageIndex.Visible = false;
                this.txtItemspageIndex.Visible = false;
                this.btnItemsGO.Visible = false;
            }
            else if (this.ItemsTotalPagesLabel.Text.Trim() != "1")
            {
                this.lblItemspageIndex.Visible = true;
                this.txtItemspageIndex.Visible = true;
                this.btnCusGO.Visible = true;
            }

            if (lastPage == Items_totalPages)
            {
                ItemsNextButton.Visible = false;
                ItemsEndpage.Visible = false;
            }
            else
            {
                ItemsNextButton.Visible = true;
                ItemsEndpage.Visible = true;
            }

            if (lastPage == 1)
            {
                ItemsFirstpage.Visible = false;
                ItemsPreviousButton.Visible = false;
            }
            else
            {
                ItemsFirstpage.Visible = true;
                ItemsPreviousButton.Visible = true;
            }

            if (dtReloadItem.Rows.Count <= 0)
                NavigationPanelItems.Visible = false;
            else
                NavigationPanelItems.Visible = true;
        }
        void gotoPageItems(int index)
        {
            if (index <= 0)
            {
                if (this.ItemsTotalPagesLabel.Text.Trim() == "0")
                {
                    //Alert.
                    Page.ScriptJqueryMessage("Not found data of page", JMessageType.Warning);
                    this.txtItemspageIndex.Text = string.Empty;
                    return;
                }
                else
                {
                    //Alert.
                    Page.ScriptJqueryMessage("Input page correct format", JMessageType.Warning);
                    this.txtItemspageIndex.Text = string.Empty;
                    return;
                }
            }
            else
            {
                if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
                {
                    this.txtItemspageIndex.Attributes.Add("onfocus", "selectText();");
                    PageSetFocus(this.txtItemspageIndex);
                    return;
                }
                else if (index > Convert.ToInt32(this.ItemsTotalPagesLabel.Text.Trim()))
                {
                    this.txtCuspageIndex.Text = string.Empty;
                    PageSetFocus(this.txtItemspageIndex);
                    return;
                }
                else
                {
                    #region Go to Page

                    using(Execute.Execution _execute = new Execute.Execution())
                    {
                        dtReloadItem = _execute.getPDALoggingBycriteria(null, null, string.Empty);
                    }
                    DataTable goPage = new DataTable();

                    //Add colunms
                    goPage.Columns.Add("id");
                    goPage.Columns.Add("error_code");
                    goPage.Columns.Add("error_message");
                    goPage.Columns.Add("page_form");
                    goPage.Columns.Add("method_name");
                    goPage.Columns.Add("create_date");

                    int counter = 0;
                    int pageIndex = index - 1;
                    int startIndex = Items_pageSize * pageIndex;
                    int endIndex = startIndex + Items_pageSize - 1;

                    if (!dtReloadItem.IsNullOrNoRows())
                    {
                        foreach (DataRow itemReload in dtReloadItem.Rows)
                        {
                            if (counter >= startIndex)
                            {
                                DataRow drgoPage = goPage.NewRow();
                                drgoPage[0] = itemReload["id"].ToString().Trim();
                                drgoPage[1] = itemReload["error_code"].ToString().Trim();
                                drgoPage[2] = itemReload["error_message"].ToString().Trim();
                                drgoPage[3] = itemReload["page_form"].ToString().Trim();
                                drgoPage[4] = itemReload["method_name"].ToString().Trim();
                                drgoPage[5] = itemReload["create_date"].ToString().Trim();
                                goPage.Rows.Add(drgoPage);
                            }

                            if (counter >= endIndex) { break; }
                            counter++;
                        }

                        this.lblItemsSearchingCount.Visible = false;
                    }
                    else if (dtReloadItem == null || dtReloadItem.Rows.Count == 0)
                    {
                        //Not found data.
                        this.lblItemsSearchingCount.Visible = true;
                        this.lblItemsSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                        dgvItems.DataSource = goPage;
                        dgvItems.DataBind();

                        ItemsNextButton.Visible = false;
                        ItemsEndpage.Visible = false;
                        ItemsPreviousButton.Visible = false;
                        ItemsFirstpage.Visible = false;

                        this.lblItemspageIndex.Visible = false;
                        this.txtItemspageIndex.Visible = false;
                        this.btnItemsGO.Visible = false;

                        ItemsCurrentPageLabel.Text = "0";
                        ItemsTotalPagesLabel.Text = "0";
                        return;
                    }

                    //Control page.
                    ItemsNextButton.Visible = true;
                    ItemsEndpage.Visible = true;
                    ItemsPreviousButton.Visible = true;
                    ItemsFirstpage.Visible = true;

                    dgvItems.DataSource = goPage;

                    Items_totalPages = ((dtReloadItem.Rows.Count - 1) / Items_pageSize) + 1;

                    if (index > Items_totalPages)
                    {
                        index = Items_totalPages;
                        gotoPageItems(index);
                        return;
                    }

                    dgvItems.DataBind();
                    ItemsCurrentPageLabel.Text = index.ToString();
                    ItemsTotalPagesLabel.Text = Items_totalPages.ToString();

                    if (this.ItemsTotalPagesLabel.Text.Trim() == "1")
                    {
                        this.lblItemspageIndex.Visible = false;
                        this.txtItemspageIndex.Visible = false;
                        this.btnItemsGO.Visible = false;
                    }
                    else if (this.ItemsTotalPagesLabel.Text.Trim() != "1")
                    {
                        this.lblItemspageIndex.Visible = true;
                        this.txtItemspageIndex.Visible = true;
                        this.btnItemsGO.Visible = true;
                    }

                    if (index == Items_totalPages)
                    {
                        ItemsNextButton.Visible = false;
                        ItemsEndpage.Visible = false;
                    }
                    else
                    {
                        ItemsNextButton.Visible = true;
                        ItemsEndpage.Visible = true;
                    }

                    if (index == 1)
                    {
                        ItemsFirstpage.Visible = false;
                        ItemsPreviousButton.Visible = false;
                    }
                    else
                    {
                        ItemsFirstpage.Visible = true;
                        ItemsPreviousButton.Visible = true;
                    }

                    if (dtReloadItem.Rows.Count <= 0)
                        NavigationPanelItems.Visible = false;
                    else
                        NavigationPanelItems.Visible = true;

                    #endregion
                }
            }
        }

        private bool ValidateSearch(DateTime? start, DateTime? end)
        {
            if (start.HasValue && end.HasValue)
            {
                if (start.Value.Date > end.Value)
                {
                    Page.ScriptJqueryMessage("วันที่(ตั้งแต่)ต้องน้อยกว่าหรือเท่ากับวันที่(ถึง) รูปแบบการค้นหาวันที่ไม่ถูกต้อง", JMessageType.Error);
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region Event

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["secAdmin"] == null)
                {
                    Response.Redirect("~/Login.aspx");
                }

                TabCustomer.CssClass = "Clicked";
                MultiViewMaster.ActiveViewIndex = 0;

                using(Execute.Execution exe = new Execute.Execution())
                {
                    dtLogging = exe.getLoggingBycriteria(null, null, string.Empty);
                    if (dtLogging.Rows.Count > 0)
                    {
                        Reload(dtLogging);
                    }
                    else
                    {
                        this.dgvCustomer.DataSource = dtLogging;
                        this.dgvCustomer.DataBind();
                        NavigationPanelCustomer.Visible = false;
                    }
                }
            }
        }

        // Tab Web Logging.
        protected void TabCustomer_Click(object sender, EventArgs e)
        {
            TabCustomer.CssClass = "Clicked";
            TabItems.CssClass = "Initial";
            MultiViewMaster.ActiveViewIndex = 0;

            this.txtStartDate.Text = string.Empty;
            this.txtEndDate.Text = string.Empty;
            this.txtErrorCode.Text = string.Empty;

            // Binding data customer.
            using (Execute.Execution exe = new Execute.Execution())
            {
                DataTable dtLogging = exe.getLoggingBycriteria(null, null, string.Empty);
                if (dtLogging.Rows.Count > 0)
                {
                    Reload(dtLogging);
                }
                else
                {
                    this.dgvCustomer.DataSource = dtLogging;
                    this.dgvCustomer.DataBind();
                    NavigationPanelCustomer.Visible = false;
                }
            }
        }
        protected void CusFirstpage_Click(object sender, EventArgs e)
        {
            try
            {
                startPaging(1);
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("First page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void CusPreviousButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(CusCurrentPageLabel.Text))
            {
                Cus_currentPage = Convert.ToInt32(CusCurrentPageLabel.Text);
                Cus_currentPage--;
                using (Execute.Execution exe = new Execute.Execution())
                {
                    Reload(exe.getLoggingBycriteria(null, null, string.Empty));
                }
            }
        }
        protected void CusNextButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(CusCurrentPageLabel.Text))
            {
                Cus_currentPage = Convert.ToInt32(CusCurrentPageLabel.Text);
                Cus_currentPage++;
                using (Execute.Execution exe = new Execute.Execution())
                {
                    Reload(exe.getLoggingBycriteria(null, null, string.Empty));
                }
            }
        }
        protected void CusEndpage_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(CusTotalPagesLabel.Text))
                {
                    int lastPage = Convert.ToInt32(this.CusTotalPagesLabel.Text.Trim());
                    if (lastPage > 0)
                    {
                        endPaging(lastPage);
                    }
                    else if (lastPage <= 0)
                    {
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("End page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void btnCusGO_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(this.txtCuspageIndex.Text.Trim()))
                    gotoPage(Convert.ToInt32(this.txtCuspageIndex.Text.Trim()));
                else
                    PageSetFocus(this.txtCuspageIndex);
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("Go to page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        // Image click
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            DateTime? start;
            DateTime? end;

            if (string.IsNullOrEmpty(txtStartDate.Text.Trim()))
                start = null;
            else
                start = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", usDtfi); //Convert.ToDateTime(txtStartDate.Text, usDtfi);

            if (string.IsNullOrEmpty(txtEndDate.Text.Trim()))
                end = null;
            else
                end = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", usDtfi); //Convert.ToDateTime(txtStartDate.Text, usDtfi);//Convert.ToDateTime(txtEndDate.Text);

            if (!ValidateSearch(start, end))
                return;

            using (Execute.Execution _execute = new Execute.Execution())
            {
                DataTable dtSearch = _execute.getLoggingBycriteria(string.IsNullOrEmpty(txtStartDate.Text) ? string.Empty : txtStartDate.Text.Trim(), 
                    string.IsNullOrEmpty(txtEndDate.Text) ? string.Empty : txtEndDate.Text.Trim(), this.txtErrorCode.Text.Trim());
                if (!dtSearch.IsNullOrNoRows())
                {
                    Reload(dtSearch);
                }
                else
                {
                    this.dgvCustomer.DataSource = dtLogging;
                    this.dgvCustomer.DataBind();
                    NavigationPanelCustomer.Visible = false;
                }
            }
        }
        protected void imgClearDate_Click(object sender, ImageClickEventArgs e)
        {
            this.txtStartDate.Text = string.Empty;
            this.txtEndDate.Text = string.Empty;

            this.txtErrorCode.Text = string.Empty;
        }
        protected void btnClearErrorCode_Click(object sender, EventArgs e)
        {
            this.txtErrorCode.Text = string.Empty;
        }

        // Tab PDA Logging.
        protected void TabItems_Click(object sender, EventArgs e)
        {
            TabCustomer.CssClass = "Initial";
            TabItems.CssClass = "Clicked";
            MultiViewMaster.ActiveViewIndex = 1;
            this.txtItemspageIndex.Text = string.Empty;

            this.txtStartDate.Text = string.Empty;
            this.txtEndDate.Text = string.Empty;
            this.txtErrorCode.Text = string.Empty;

            // Binding data customer.
            using (Execute.Execution exe = new Execute.Execution())
            {
                DataTable dtPDALogging = exe.getPDALoggingBycriteria(null, null, string.Empty);
                if (dtPDALogging.Rows.Count > 0)
                {
                    ReloadItems(dtPDALogging);
                }
                else
                {
                    NavigationPanelItems.Visible = false;
                    this.dgvItems.DataSource = DataAccess.Items.Items.Instance.getItems();
                    this.dgvItems.DataBind();
                }
            }
        }
        protected void ItemsFirstpage_Click(object sender, EventArgs e)
        {
            try
            {
                startPagingItems(1);
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("Item first page : " + ex.Message.ToString(), JMessageType.Error);
                using (Execute.Execution _execute = new Execute.Execution())
                {
                    _execute.Insert_Log("Web_FirstPage", ex.Message.ToString(), "ViewLogging.aspx", "ItemsFirstpage_Click");
                }
            }
        }
        protected void ItemsPreviousButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ItemsCurrentPageLabel.Text))
            {
                Items_currentPage = Convert.ToInt32(ItemsCurrentPageLabel.Text);
                Items_currentPage--;
                using (Execute.Execution exe = new Execute.Execution())
                {
                    ReloadItems(exe.getPDALoggingBycriteria(null, null, string.Empty));
                }
            }
        }
        protected void ItemsNextButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ItemsCurrentPageLabel.Text))
            {
                Items_currentPage = Convert.ToInt32(ItemsCurrentPageLabel.Text);
                Items_currentPage++;
                using (Execute.Execution exe = new Execute.Execution())
                {
                    ReloadItems(exe.getPDALoggingBycriteria(null, null, string.Empty));
                }
            }
        }
        protected void ItemsEndpage_Click(object sender, EventArgs e)
        {
            try
            {
                if(!string.IsNullOrEmpty(ItemsTotalPagesLabel.Text.Trim()))
                {
                    int lastPage = Convert.ToInt32(this.ItemsTotalPagesLabel.Text.Trim());
                    if (lastPage > 0)
                        endPagingItems(lastPage);
                    else if (lastPage <= 0)
                        return;
                }
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("Items End page : " + ex.Message.ToString(), JMessageType.Error);
                using (Execute.Execution _execute = new Execute.Execution())
                {
                    _execute.Insert_Log("Web_EndPage", ex.Message.ToString(), "ViewLogging.aspx", "ItemsEndpage_Click");
                }
            }
        }
        protected void btnItemsGO_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(this.txtItemspageIndex.Text.Trim()))
                    gotoPageItems(Convert.ToInt32(this.txtItemspageIndex.Text.Trim()));
                else
                    PageSetFocus(this.txtItemspageIndex);
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("Items Go to page : " + ex.Message.ToString(), JMessageType.Error);
                using (Execute.Execution _execute = new Execute.Execution())
                {
                    _execute.Insert_Log("Web_GoTo", ex.Message.ToString(), "ViewLogging.aspx", "btnItemsGO_Click");
                }
            }
        }
        // Image click
        protected void imgPDASearch_Click(object sender, ImageClickEventArgs e)
        {
            DateTime? start;
            DateTime? end;

            if (string.IsNullOrEmpty(txtPDAStartDate.Text.Trim()))
                start = null;
            else
                start = DateTime.ParseExact(txtPDAStartDate.Text, "dd/MM/yyyy", usDtfi); // Convert.ToDateTime(txtPDAStartDate.Text);

            if (string.IsNullOrEmpty(txtPDAEndDate.Text.Trim()))
                end = null;
            else
                end = DateTime.ParseExact(txtPDAEndDate.Text, "dd/MM/yyyy", usDtfi); // Convert.ToDateTime(txtPDAEndDate.Text);

            if (!ValidateSearch(start, end))
                return;

            using (Execute.Execution _execute = new Execute.Execution())
            {
                DataTable dtSearch = _execute.getPDALoggingBycriteria(string.IsNullOrEmpty(txtPDAStartDate.Text) ? string.Empty : txtPDAStartDate.Text.Trim(), 
                    string.IsNullOrEmpty(txtPDAEndDate.Text) ? string.Empty : txtPDAEndDate.Text.Trim(), this.txtPDAErrorCode.Text.Trim());
                if (!dtSearch.IsNullOrNoRows())
                {
                    ReloadItems(dtSearch);
                }
                else
                {
                    this.dgvItems.DataSource = dtSearch;
                    this.dgvItems.DataBind();
                    NavigationPanelItems.Visible = false;
                }
            }
        }
        protected void imgPDAClearDate_Click(object sender, ImageClickEventArgs e)
        {
            this.txtPDAStartDate.Text = string.Empty;
            this.txtPDAEndDate.Text = string.Empty;

            this.txtPDAErrorCode.Text = string.Empty;
        }

        #endregion
    }
}