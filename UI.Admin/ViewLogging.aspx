﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewLogging.aspx.cs" Inherits="UI.Admin.ViewLogging" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Thai Japan Gas</title>
    <link rel="stylesheet" href="_css/popup.css" />
    <link rel="stylesheet" href="_css/btnbootstrap.css" />
    <link rel="stylesheet" href="_css/button.css" />
    <link rel="stylesheet" type="text/css" href="../_css/main.css" />
    <link rel="stylesheet" type="text/css" href="../_css/styles.css" />
    <link rel="stylesheet" href="_css/element-view.css" />
    <script src="../_js/script.js" type="text/javascript"></script>
    <script src="_js/jquery1.7.1.js" type="text/javascript"></script>
    <script src="_js/jquery-latest.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="_js/jquery.min.js"></script>

    <style type="text/css">
        .Initial {
            display: block;
            padding: 4px 18px 4px 18px;
            float: left;
            background: url("../_images/InitialImage.png") no-repeat right top;
            /*background-color:#089bfc;*/
            color: Black;
            font-weight: bold;
        }

            .Initial:hover {
                color: White;
                background: url("../_images/SelectedButton.png") no-repeat right top;
                /*background-color:#006699;*/
            }

        .Clicked {
            float: left;
            display: block;
            background: url("../_images/SelectedButton.png") no-repeat right top;
            /*background-color:#006699;*/
            padding: 4px 18px 4px 18px;
            color: Black;
            font-weight: bold;
            color: White;
        }

        #loading-image {
            border-width: 0px;
            position: fixed;
            padding: 50px;
            left: 40%;
            top: 30%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </cc1:ToolkitScriptManager>
        <%--<asp:ScriptManager ID="ScriptManager" runat="server" />--%>
        <div class="header">
            <div class="fullpage">
                <div id='cssmenu'>
                    <table>
                        <tr>
                            <td style="height: 20px;"></td>
                        </tr>
                    </table>
                    <div class="login" id="bt_login">
                        <a href="Login.aspx" style="text-decoration: none; color: black;">
                            <asp:Label ID="lblLogout" runat="server" Text="Log out" ForeColor="White" Font-Bold="true" Width="55px"></asp:Label></a>
                    </div>
                </div>
            </div>
        </div>
        <table id="tbMasterData" style="width: 100%;">
            <tr style="background-color: #006699;">
                <td>
                    <asp:Label ID="lblMasterData" runat="server" Text="View log error" ForeColor="White"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 0px;"></td>
            </tr>
            <tr>
                <td>
                    <asp:Button Text="1. Web Logging" BorderStyle="None" ID="TabCustomer" CssClass="Initial" runat="server" OnClick="TabCustomer_Click" />
                    <asp:Button Text="2. PDA Logging" BorderStyle="None" ID="TabItems" CssClass="Initial" runat="server" OnClick="TabItems_Click" />
                    <asp:MultiView ID="MultiViewMaster" runat="server">
                        <asp:View ID="ViewCustomer" runat="server">
                            <asp:UpdatePanel ID="unpCustomer" runat="server">
                                <ContentTemplate>
                                    <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                        <tr>
                                            <td class="auto-style1">
                                                <asp:Label ID="lblSearchDate" runat="server" Text="ค้นหาวันที่"></asp:Label>
                                            </td>
                                            <td class="auto-style4">
                                                <asp:TextBox ID="txtStartDate" runat="server" Enabled="false" BackColor="White"></asp:TextBox>
                                                <asp:ImageButton ID="imgPopup" ImageUrl="~/_images/calendar.png" ImageAlign="Bottom" runat="server" />
                                                <cc1:CalendarExtender ID="Calendar1" PopupButtonID="imgPopup" runat="server" TargetControlID="txtStartDate"
                                                    Format="dd/MM/yyyy">
                                                </cc1:CalendarExtender>
                                                &nbsp;
                                        <asp:Label ID="lblTo" runat="server" Text="ถึง"></asp:Label>&nbsp;&nbsp;&nbsp;
                                        <asp:TextBox ID="txtEndDate" runat="server" Enabled="false" BackColor="White"></asp:TextBox>
                                                <asp:ImageButton ID="imgEndDate" ImageUrl="~/_images/calendar.png" ImageAlign="Bottom" runat="server" />
                                                <cc1:CalendarExtender ID="Calendar2" PopupButtonID="imgEndDate" runat="server" TargetControlID="txtEndDate"
                                                    Format="dd/MM/yyyy">
                                                </cc1:CalendarExtender>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:ImageButton ID="imgSearch" runat="server" ImageUrl="~/_images/search.png" Height="22px" Width="22px" OnClick="imgSearch_Click" />&nbsp;&nbsp;&nbsp;
                                        <asp:ImageButton ID="imgClearDate" runat="server" ImageUrl="~/_images/clear.png" Height="22px" Width="22px" OnClick="imgClearDate_Click" />

                                            </td>
                                            <td rowspan="2">
                                                <asp:Button ID="btnSaveCustomer" runat="server" CssClass="button button-action" Text="Save" Visible="false" />&nbsp;&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style1">
                                                <asp:Label ID="lblErorCode" runat="server" Text="Error Code"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                    
                                            </td>
                                            <td class="auto-style4">
                                                <asp:TextBox ID="txtErrorCode" runat="server" Width="400px"></asp:TextBox>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnClearErrorCode" runat="server" CssClass="button button-action" Text="Clear" Visible="false" OnClick="btnClearErrorCode_Click" />&nbsp;&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style1"></td>
                                            <td class="auto-style4"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:Panel ID="pnperson" runat="server" GroupingText="Logging List" Width="98%" Style="text-align: left;">
                                                    <asp:GridView ID="dgvCustomer" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <HeaderStyle BackColor="#006699" Height="30px"></HeaderStyle>
                                                        <Columns>
                                                            <asp:BoundField DataField="id" HeaderText="ID" Visible="False">
                                                                <HeaderStyle ForeColor="White" />
                                                                <ItemStyle Width="0px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="error_code" HeaderText="Error Code">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="80px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="error_message" HeaderText="Error Message">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="300px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="page_form" HeaderText="Page Form">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="50px" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="method_name" HeaderText="Method Name">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="create_date" HeaderText="Create Date">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <br />
                                                    <asp:Panel ID="NavigationPanelCustomer" Visible="true" runat="server">
                                                        <table border="0" cellpadding="3" cellspacing="3">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblCusSearchingCount" runat="server" Visible="false"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 100">Page
                                                            <asp:Label ID="CusCurrentPageLabel" runat="server" />
                                                                    of
                                                            <asp:Label ID="CusTotalPagesLabel" runat="server" />
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="CusFirstpage" Text="<< Start" runat="server" Style="text-decoration: none; color: #089bfc;" OnClick="CusFirstpage_Click" />
                                                                </td>
                                                                <td style="width: 60px">
                                                                    <asp:LinkButton ID="CusPreviousButton" Text="< Prev" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="CusPreviousButton_Click" />
                                                                </td>
                                                                <td style="width: 60px">
                                                                    <asp:LinkButton ID="CusNextButton" Text="Next >" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="CusNextButton_Click" />
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="CusEndpage" Text="End >>" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="CusEndpage_Click" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblCuspageIndex" runat="server" Text="Page"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCuspageIndex" runat="server" Width="35px" MaxLength="4" Height="17px" onkeypress="return isNumberKey(event);"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="btnCusGO" runat="server" Text="Go" Width="40px" Style="text-align: center;" CssClass="btnOpen btnOpen-5" Height="22px" OnClick="btnCusGO_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:View>
                        <asp:View ID="ViewItems" runat="server">
                            <asp:UpdatePanel ID="unpErrorPDA" runat="server">
                                <ContentTemplate>
                                    <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                        <tr>
                                            <td class="auto-style1">
                                                <asp:Label ID="lblPDASearchDate" runat="server" Text="ค้นหาวันที่"></asp:Label>
                                            </td>
                                            <td class="auto-style4">
                                                <asp:TextBox ID="txtPDAStartDate" runat="server" Enabled="false" BackColor="White"></asp:TextBox>
                                                <asp:ImageButton ID="imgPDAPopup" ImageUrl="~/_images/calendar.png" ImageAlign="Bottom" runat="server" />
                                                <cc1:CalendarExtender ID="PDACalendar1" PopupButtonID="imgPDAPopup" runat="server" TargetControlID="txtPDAStartDate"
                                                    Format="dd/MM/yyyy">
                                                </cc1:CalendarExtender>
                                                &nbsp;
                                        <asp:Label ID="lblPDATo" runat="server" Text="ถึง"></asp:Label>&nbsp;&nbsp;&nbsp;
                                        <asp:TextBox ID="txtPDAEndDate" runat="server" Enabled="false" BackColor="White"></asp:TextBox>
                                                <asp:ImageButton ID="imgPDAEndDate" ImageUrl="~/_images/calendar.png" ImageAlign="Bottom" runat="server" />
                                                <cc1:CalendarExtender ID="PDACalendar2" PopupButtonID="imgPDAEndDate" runat="server" TargetControlID="txtPDAEndDate"
                                                    Format="dd/MM/yyyy">
                                                </cc1:CalendarExtender>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:ImageButton ID="imgPDASearch" runat="server" ImageUrl="~/_images/search.png" Height="22px" Width="22px" OnClick="imgPDASearch_Click" />&nbsp;&nbsp;&nbsp;
                                        <asp:ImageButton ID="imgPDAClearDate" runat="server" ImageUrl="~/_images/clear.png" Height="22px" Width="22px" OnClick="imgPDAClearDate_Click" />

                                            </td>
                                            <td rowspan="2">
                                                <asp:Button ID="btnPDASaveCustomer" runat="server" CssClass="button button-action" Text="Save" Visible="false" />&nbsp;&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style1">
                                                <asp:Label ID="lblPDAErorCode" runat="server" Text="Error Code"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                    
                                            </td>
                                            <td class="auto-style4">
                                                <asp:TextBox ID="txtPDAErrorCode" runat="server" Width="400px"></asp:TextBox>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnPDAClearErrorCode" runat="server" CssClass="button button-action" Text="Clear" Visible="false" />&nbsp;&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style1"></td>
                                            <td class="auto-style4"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:Panel ID="pnPDAperson" runat="server" GroupingText="Logging List" Width="98%" Style="text-align: left;">
                                                    <asp:GridView ID="dgvItems" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <HeaderStyle BackColor="#006699" Height="30px"></HeaderStyle>
                                                        <Columns>
                                                            <asp:BoundField DataField="id" HeaderText="ID" Visible="False">
                                                                <HeaderStyle ForeColor="White" />
                                                                <ItemStyle Width="0px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="error_code" HeaderText="Error Code">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="90px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="error_message" HeaderText="Error Message">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="450px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="page_form" HeaderText="Page Form">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="50px" HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="method_name" HeaderText="Method Name">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="create_date" HeaderText="Create Date">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="90px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <br />
                                                    <asp:Panel ID="NavigationPanelItems" Visible="true" runat="server">
                                                        <table border="0" cellpadding="3" cellspacing="3">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblItemsSearchingCount" runat="server" Visible="false"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 100">Page
                                                            <asp:Label ID="ItemsCurrentPageLabel" runat="server" />
                                                                    of
                                                            <asp:Label ID="ItemsTotalPagesLabel" runat="server" />
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="ItemsFirstpage" Text="<< Start" runat="server" Style="text-decoration: none; color: #089bfc;" OnClick="ItemsFirstpage_Click" />
                                                                </td>
                                                                <td style="width: 60px">
                                                                    <asp:LinkButton ID="ItemsPreviousButton" Text="< Prev" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="ItemsPreviousButton_Click" />
                                                                </td>
                                                                <td style="width: 60px">
                                                                    <asp:LinkButton ID="ItemsNextButton" Text="Next >" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="ItemsNextButton_Click" />
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="ItemsEndpage" Text="End >>" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="ItemsEndpage_Click" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblItemspageIndex" runat="server" Text="Page"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtItemspageIndex" runat="server" Width="35px" MaxLength="4" Height="17px" onkeypress="return isNumberKey(event);"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="btnItemsGO" runat="server" Text="Go" Width="40px" Style="text-align: center;" CssClass="btnOpen btnOpen-5" Height="22px" OnClick="btnItemsGO_Click" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
