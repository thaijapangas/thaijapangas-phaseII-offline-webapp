﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Data;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using UI.Admin.CallingService;
using UI.Admin.Execute;
using System.Configuration;

namespace UI.Admin.Connections
{
    public partial class Connection
    {
        #region Property

        protected CallService dbManager;

        #endregion

        #region Method

        public Connection()
        {
            //string connectionString = @"Data Source=C:\Data Base SDF\TJG.sdf;Password=pass;Persist Security Info=True";
            string connectionString_temp = @"Data Source=C:\Data Base SDF\TJG.sdf;Default Lock Timeout = 30000;Max Database Size = 2560;Password=pass;Persist Security Info=True";
            string connectionString = string.IsNullOrEmpty(ConfigurationManager.AppSettings["conn"].ToString()) ? connectionString_temp : ConfigurationManager.AppSettings["conn"].ToString().Trim();

            dbManager = new CallService(connectionString);
            try
            {
                dbManager.Open();
            }
            catch (SqlCeException sqlex)
            {
                //Executing.Instance.Insert_Log("Web_CONNSQLCEException", sqlex.Message.ToString(), "Class_Connection", "Method_Connection()");
                using(Execution _execute = new Execution())
                {
                    _execute.Insert_Log("Web_CONNSQLCEException", sqlex.Message.ToString(), "Class_Connection.cs", "Method_Connection()");
                }
            }
            catch (Exception ex)
            {
                //Executing.Instance.Insert_Log("Web_CONNException", ex.Message.ToString(), "Class_Connection", "Method_Connection()");
                using (Execution _execute = new Execution())
                {
                    _execute.Insert_Log("Web_CONNException", ex.Message.ToString(), "Class_Connection.cs", "Method_Connection()");
                }
            }
        }

        #endregion
    }
}