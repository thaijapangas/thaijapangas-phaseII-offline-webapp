﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExportExcel.aspx.cs" Inherits="UI.Admin.ExportExcel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <table>
        <tr>
            <td>
                <asp:GridView ID="dgvGIRefDelivery" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <HeaderStyle BackColor="#006699" Height="30px"></HeaderStyle>
                                                        <Columns>
                                                            <asp:BoundField DataField="create_date" HeaderText="Create Date">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="do_no" HeaderText="Invoice / DO">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="serial_number" HeaderText="S/N">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ddd" runat="server" Text="Load" OnClick="ddd_Click" />
                <asp:Button ID="btnExport" runat="server" Text="Export Excel Test" OnClick="btnExport_Click" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
