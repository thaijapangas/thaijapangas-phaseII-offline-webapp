﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UI.Admin.Execute;

namespace UI.Admin
{
    public partial class ActivateLicense : System.Web.UI.Page
    {
        #region Focus TextBox

        void PageSetFocus(Control ctrl)
        {
            ScriptManager.GetCurrent(this.Page).SetFocus(ctrl);
        }

        #endregion

        #region Member

        DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;

        #endregion

        #region Event

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["secLogin"] == null)
                {
                    Response.Redirect("~/Login.aspx");
                    PageSetFocus(txtActivateKeys);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["expireDate"]))
                {
                    this.lblExpire.Text = "Licences key สามารถใช้งานได้ถึงวันที่ " + Request.QueryString["expireDate"].ToString().Trim() + " กรุณาลงทะเบียน Licences key ที่โปรแกรมบน PDA -> เมนู 'Activate Licences'";
                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtActivateKeys.Text.Trim()))
            {
                Page.ScriptJqueryMessage("กรุณากรอกหมายเลขลงทะเบียนโปรแกรม", JMessageType.Warning);
                PageSetFocus(txtActivateKeys);
                return;
            }
            else if (!string.IsNullOrEmpty(this.txtActivateKeys.Text.Trim()))
            {
                try
                {
                    string _crrent_date = Convert.ToDateTime(Executing.Instance.GetDateServer()).ToString("yyyyMMdd", usDtfi);
                    string _licence_key = Convert.ToString((Convert.ToInt64(this.txtActivateKeys.Text.Trim()) - 39335472));
                    if (!string.IsNullOrEmpty(_licence_key))
                    {
                        if (Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()) == "TRUE")
                        {
                            Page.ScriptJqueryMessage("หมายเลขลงทะเบียนถูกต้อง กรุณาเปิดโปรแกรมใหม่อีกครั้ง", JMessageType.Warning);
                            Response.Redirect("~/Login.aspx");
                        }
                        else if (Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()).IndexOf("|") >= 0)
                        {
                            string[] array = Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()).Split('|');
                            Page.ScriptJqueryMessage("หมายเลขลงทะเบียนถูกต้อง จะมีอายุการใช้งานได้ถึงวันที่ " + array[1].ToString().Trim() + " กรุณาเปิดโปรแกรมใหม่อีกครั้ง", JMessageType.Accept);
                            Response.Redirect("~/Login.aspx");
                        }
                        else if (Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()) == "TRUE_EXPIRED")
                        {
                            ScriptManager.RegisterStartupScript(
                                                    this,
                                                    GetType(),
                                                    "key",
                                                    "confirm('โปรแกรมหมดอายุการใช้งาน กรุณาติดต่อผู้ดูแลระบบเพื่อทำการลงทะเบียนโปรแกรม?');",
                                                    true
                                                    );

                            Page.ScriptJqueryMessage("หมายเลขทะเบียนนี้หมดอายุแล้ว คุณต้องการที่จะใส่หมายเลขทะเบียนอีกครั้งหรือไม่?", JMessageType.Error);
                            PageSetFocus(txtActivateKeys);
                            return;

                            /*
                            if (MessageActivate.DialogQuestion("หมายเลขทะเบียนนี้หมดอายุแล้ว คุณต้องการที่จะใส่หมายเลขทะเบียนอีกครั้งหรือไม่?")
                                            == System.Windows.Forms.DialogResult.Yes)
                            {
                                this.txtActivateKeys.Focus();
                                this.txtActivateKeys.SelectAll();
                                return;
                            }
                            else
                            {
                                if (this.Action_form.Trim() == "ACTIVATES")
                                {
                                    this.Close();
                                }
                                else
                                {
                                    Application.Exit();
                                }
                            }
                            */
                        }
                        else if (Executing.Instance.ValidationLicence(this.txtActivateKeys.Text.Trim()) == "FALSE")
                        {
                            Page.ScriptJqueryMessage("หมายเลขลงทะเบียนไม่ถูกต้อง คุณต้องการที่จะใส่หมายเลขทะเบียนอีกครั้งหรือไม่?", JMessageType.Error);
                            PageSetFocus(txtActivateKeys);
                            return;

                            /*
                            if (MessageActivate.DialogQuestion("หมายเลขลงทะเบียนไม่ถูกต้อง คุณต้องการที่จะใส่หมายเลขทะเบียนอีกครั้งหรือไม่?")
                                            == System.Windows.Forms.DialogResult.Yes)
                            {
                                this.txtActivateKeys.Focus();
                                this.txtActivateKeys.SelectAll();
                                return;
                            }
                            else
                            {
                                if (this.Action_form.Trim() == "ACTIVATES")
                                {
                                    this.Close();
                                }
                                else
                                {
                                    Application.Exit();
                                }
                            }
                            */
                        }
                    }
                }
                catch (Exception)
                {
                    Page.ScriptJqueryMessage("หมายเลขลงทะเบียนไม่ถูกต้อง คุณต้องการที่จะใส่หมายเลขทะเบียนอีกครั้งหรือไม่?", JMessageType.Error);
                    PageSetFocus(txtActivateKeys);
                    return;

                    /*
                    if (MessageActivate.DialogQuestion("หมายเลขลงทะเบียนไม่ถูกต้อง คุณต้องการที่จะใส่หมายเลขทะเบียนอีกครั้งหรือไม่?")
                                    == System.Windows.Forms.DialogResult.Yes)
                    {
                        this.txtActivateKeys.Focus();
                        this.txtActivateKeys.SelectAll();
                        return;
                    }
                    else
                    {
                        if (this.Action_form.Trim() == "ACTIVATES")
                        {
                            this.Close();
                        }
                        else
                        {
                            Application.Exit();
                        }
                    }
                    */
                }
            }
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["activate"]))
            {
                if (Request.QueryString["activate"].ToString().Trim() == "ACTIVATES")
                {
                    Response.Redirect("~/Mamgements.aspx");
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }
        }

        #endregion
    }
}